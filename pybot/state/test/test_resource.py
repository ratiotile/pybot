from pybot.state.resource import Bank, Reserve
import cybw

def test_Reserve():
    Resource = Bank()
    Resource.update(1, 100, 0, (4,10))
    with Reserve(Resource, cybw.UnitTypes.Terran_Supply_Depot):
        assert Resource.mineralsAvailable() == 0
    assert Resource.mineralsAvailable() == 100

