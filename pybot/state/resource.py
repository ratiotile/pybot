import logging
import cybw
from collections import deque, namedtuple, defaultdict

logger = logging.getLogger(__name__)

ResourceData = namedtuple(
    'ResourceData',  # Used for ResourceRate calculations
        ("minerals",  # change in minerals, negative for consumption
         "gas",  # change in gas, positive for production
         "supply",  # change in supply
         "frame",  # time when resource change happened
         "unit"  # worker for gathering, production unit otherwise
         )
)


class ResourceRate:
    def __init__(self, interval):
        self.maxinterval = interval  # moving average frames
        self.interval = interval
        # {UnitType : deque(m, g, s, frame, unit)}
        self.resource_data = defaultdict(deque)
        # rates, per second
        self.mineral_in = 0
        self.gas_in = 0
        self.supply_in = 0
        # expenditure, per second, as negative
        self.mineral_ex = 0
        self.gas_ex = 0
        self.supply_ex = 0

    def discardOldData(self, currentframe):
        cutoff_frame = currentframe - self.maxinterval
        for dataq in self.resource_data.values():
            while(len(dataq) > 0 and
                  dataq[0].frame < cutoff_frame):
                dataq.popleft()

    def getData(self, unittypes):
        return {k: self.resource_data[k] for k in unittypes}

    def getMinEx(self, unittype):
        return sum(d.minerals for d in self.resource_data[unittype])

    def update(self, currentframe):
        if currentframe == 0: return
        if self.maxinterval > currentframe:
            self.interval = min(self.maxinterval, currentframe)
        self.discardOldData(currentframe)
        self.updateRates()

    def updateRates(self):
        min_in = 0
        gas_in = 0
        sup_in = 0
        min_ex = 0
        gas_ex = 0
        sup_ex = 0
        for utype, dq in self.resource_data.items():
            for d in dq:
                if d.minerals > 0:  min_in += d.minerals
                else:               min_ex += d.minerals
                if d.gas > 0:       gas_in += d.gas
                else:               gas_ex += d.gas
                if d.supply > 0:    sup_in += d.supply
                else:               sup_ex += d.supply
        self.mineral_in = min_in / self.interval * 23.81  # per second
        self.gas_in = gas_in / self.interval * 23.81
        self.supply_in = sup_in / self.interval * 23.81
        self.mineral_ex = min_ex / self.interval * 23.81  # per second
        self.gas_ex = gas_ex / self.interval * 23.81
        self.supply_ex = sup_ex / self.interval * 23.81

    def addData(self, mineral, gas, supply, unit):
        self.resource_data[unit.getType()].append(ResourceData(
            mineral, gas, supply, cybw.Broodwar.getFrameCount(), unit
        ))


class Bank:

    """ to manage resources available """

    def __init__(self):
        self.mineral = 0
        self.gas = 0
        self.supplyUsed = 0
        self.supplyTotal = 0

        self.reserved_mineral = 0
        self.reserved_gas = 0
        self.reserved_supply = 0

        self.rate = ResourceRate(interval=300)

    def update(self, frame, mineral=None, gas=None, supply=None):
        if mineral is not None:
            self.mineral = mineral
        if gas is not None:
            self.gas = gas
        if supply is not None:
            self.supplyUsed, self.supplyTotal = supply
        self.rate.update(frame)
        self.debugDraw()

    def mineralsAvailable(self):
        return self.mineral - self.reserved_mineral

    def gasAvailable(self):
        return self.gas - self.reserved_gas

    def supplyAvailable(self):
        return self.supplyTotal - self.supplyUsed - self.reserved_supply

    def mineralIncome(self):
        return self.rate.mineral_in

    def gasIncome(self):
        return self.rate.gas_in

    def supplyIncome(self):
        return self.rate.supply_in

    def mineralExpense(self, factory=None):
        if factory is None:
            return self.rate.mineral_ex
        else:
            assert isinstance(factory, cybw.UnitType)
            return self.rate.getMinEx(factory)

    def haveResourcesFor(self, utype):
        if(self.mineralsAvailable() >= utype.mineralPrice() and
           self.gasAvailable() >= utype.gasPrice()):
            if utype.supplyProvided() > 0:
                return True
            elif self.supplyAvailable() >= utype.supplyRequired():
                return True
        return False

    def reserve(self, mineral=0, gas=0, supply=0):
        self.reserved_mineral += mineral
        self.reserved_gas += gas
        self.reserved_supply += supply

    def reserveType(self, actype):
        self.reserved_mineral += actype.mineralPrice()
        self.reserved_gas += actype.gasPrice()
        if isinstance(actype, cybw.UnitType):
            self.reserved_supply += actype.supplyRequired()

    def release(self, mineral=0, gas=0, supply=0):
        self.reserved_mineral -= mineral
        self.reserved_gas -= gas
        self.reserved_supply -= supply

    def releaseType(self, actype):
        self.reserved_mineral -= actype.mineralPrice()
        self.reserved_gas -= actype.gasPrice()
        if isinstance(actype, cybw.UnitType):
            self.reserved_supply -= actype.supplyRequired()

    def debugDraw(self):
        cybw.Broodwar.drawTextScreen(cybw.Position(450, 15),
                                     str(self.mineralsAvailable()))


class Reserve:

    """ context manager for reserving and releasing resources.
    usage: 'with Reserve(m, g, s):' """

    def __init__(self, bank, actype):
        self.mineral = actype.mineralPrice()
        self.gas = actype.gasPrice()
        self.supply = 0
        if isinstance(actype, cybw.UnitType):
            self.supply = actype.supplyRequired()
        self.bank = bank

    def __enter__(self):
        self.bank.reserve(self.mineral, self.gas, self.supply)

    def __exit__(self, type, value, tb):
        self.bank.release(self.mineral, self.gas, self.supply)
