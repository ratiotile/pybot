class Region:
    """ Larger map divisions, separated by chokes """
    pass

class Sector:
    """ Smaller map divisions, based on builtin BWAPI Regions """
    pass

class Border:
    """ divides Regions """
    pass

class Choke:
    """ a kind of Border that is narrow """
    pass
