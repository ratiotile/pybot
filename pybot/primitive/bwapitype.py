from pybot.interface import DatabaseInterface


class BaseType:

    """ Base class for UnitType, UpgradeType, TechType """

    def __init__(self, row):
        """ expects a sqlite3 row, initialize type data """
        self._data = {}
        for name in row.keys():
            val = row[name]
            self._data[name] = val

    def _finalizeProperties(self):
        """ override this to customize properties """
        pass

    @classmethod
    def _setProperties(cls, keys):
        for name in keys:
            cls._addProperty(name)

    @classmethod
    def _addProperty(cls, propName):
        def getter(self):
            try:
                return self._data[propName]
            except KeyError:
                import ipdb; ipdb.set_trace()
        setattr(cls, propName, property(getter))


class Race(BaseType):

    def __init__(self, row):
        super(Race, self).__init__(row)

    def __repr__(self):
        return "<Race[{}]>".format(self.name)

    def _finalizeProperties(self):
        """ get UnitTypes for refiner, supply, worker, transport, depot """
        self._data["worker"] = UnitTypes.byID(self.worker)
        self._data["depot"] = UnitTypes.byID(self.depot)
        self._data["refinery"] = UnitTypes.byID(self.refinery)
        self._data["transport"] = UnitTypes.byID(self.transport)
        self._data["supplyUnit"] = UnitTypes.byID(self.supplyUnit)


class UnitType(BaseType):

    def __init__(self, row):
        super(UnitType, self).__init__(row)

    def __repr__(self):
        return "<UnitType[{}]>".format(self.name)

    def _finalizeProperties(self):
        self._data["race"] = Races.byID(self.race)
        self._data["requiredTech"] = TechTypes.byID(self.requiredTech)
        whatBuilds = (UnitTypes.byID(self.whatBuildsUnit),
                      self.whatBuildsNum)
        self._data["whatBuilds"] = whatBuilds
        self._addProperty('whatBuilds')
        self._data["whatBuildsUnit"] = UnitTypes.byID(self.whatBuildsUnit)


class UpgradeType(BaseType):

    def __init__(self, row):
        super(UpgradeType, self).__init__(row)

    def __repr__(self):
        return "<UpgradeType[{}]>".format(self.name)

    def _finalizeProperties(self):
        """ get UnitTypes for refiner, supply, worker, transport, depot """
        self._data["whatUpgrades"] = UnitTypes.byID(self.whatUpgrades)
        self._data["race"] = Races.byID(self.race)

    def whatsRequired(self, level):
        return self._data["whatsRequiredForLevel"].get(level, UnitTypes.none)


class TechType(BaseType):

    def __init__(self, row):
        super(TechType, self).__init__(row)

    def __repr__(self):
        return "<TechType[{}]>".format(self.name)

    def _finalizeProperties(self):
        """ get UnitType for whatResearches """
        self._data["whatResearches"] = UnitTypes.byID(self.whatResearches)
        self._data["race"] = Races.byID(self.race)


class BaseTypes:

    """ Parent of UnitTypes, UpgradeTypes, TechTypes """
    @classmethod
    def _init(cls, data, Type):
        """ data is sqlite3.Row, Type is class ex. UnitType """
        cls.id_mapping = {}  # don't modify!
        # create properties from header
        for row in data:
            keys = row.keys()
            Type._setProperties(keys)
            break  # we only need 1 row
        # now create read-only UnitTypes
        for row in data:
            t = Type(row)
            cls.id_mapping[t.id] = t
            setattr(cls, row['name'], t)

    @classmethod
    def _finalize(cls):
        """ calls each object's _finalizeProperties method """
        for o in cls.id_mapping.values():
            o._finalizeProperties()

    @classmethod
    def byID(cls, id):
        """ get a Type by its id """
        try:
            return cls.id_mapping[id]
        except KeyError:
            print("KeyError in {}, id {} not found.".format(
                cls.__name__, id))
            return cls.none

    @classmethod
    def getAll(cls):
        """ returns iterable over all member Type objects """
        return cls.id_mapping.values()


class UnitTypes(BaseTypes):

    @classmethod
    def _init(cls, data):
        super(cls, cls)._init(data, UnitType)


class TechTypes(BaseTypes):

    @classmethod
    def _init(cls, data):
        super(cls, cls)._init(data, TechType)


class UpgradeTypes(BaseTypes):

    @classmethod
    def _init(cls, data):
        super(cls, cls)._init(data, UpgradeType)

    @classmethod
    def _finalize(cls, upg_reqs):
        """ calls each object's _finalizeProperties method,
        then handles the level requirements """
        for o in cls.id_mapping.values():
            o._finalizeProperties()
            o._data["whatsRequiredForLevel"] = {}
        # handle req unit for level
        for row in upg_reqs:
            id = row["upgradeID"]
            level = row["level"]
            uid = row["requiredUnitID"]
            upg = cls.id_mapping[id]
            unit = UnitTypes.byID(uid)
            upg._data["whatsRequiredForLevel"][level] = unit


class Races(BaseTypes):

    @classmethod
    def _init(cls, data):
        super(cls, cls)._init(data, Race)


def loadUnitPrereqs():
    data = DatabaseInterface.getTypeDataFromTable(
        DatabaseInterface.UNIT_REQUIREMENTS_TABLE)
    # give all units requiredUnits map
    for unit in UnitTypes.getAll():
        unit._data["requiredUnits"] = {}
        unit._addProperty("requiredUnits")

    for row in data:
        unit_id = row['unit_id']
        req_unit_id = row['req_unit_id']
        req_num = row['req_num']
        req_unit = UnitTypes.byID(req_unit_id)
        ut = UnitTypes.byID(unit_id)
        ut._data["requiredUnits"][req_unit] = req_num
    # insert nulls
    for unit in UnitTypes.getAll():
        if len(unit.requiredUnits) == 0:
            unit._data["requiredUnits"][UnitTypes.none] = 0


def init():
    """ initializes all data types by loading the data from database """
    unit_data = DatabaseInterface.getTypeDataFromTable(
                    DatabaseInterface.UNITTYPE_TABLE)
    UnitTypes._init(unit_data)
    loadUnitPrereqs()
    tech_data = DatabaseInterface.getTypeDataFromTable(
                    DatabaseInterface.TECHTYPE_TABLE)
    TechTypes._init(tech_data)
    upgrade_data = DatabaseInterface.getTypeDataFromTable(
                        DatabaseInterface.UPGRADETYPE_TABLE)
    UpgradeTypes._init(upgrade_data)
    race_data = DatabaseInterface.getTypeDataFromTable(
                    DatabaseInterface.RACE_TABLE)
    Races._init(race_data)
    UnitTypes._finalize()
    TechTypes._finalize()
    upg_level_reqs = DatabaseInterface.getTypeDataFromTable(
                        DatabaseInterface.UPG_REQ_TABLE)
    UpgradeTypes._finalize(upg_level_reqs)
    Races._finalize()

init()