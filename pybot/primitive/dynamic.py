""" Replacements for BWAPI/cybw Dynamic types.

Dynamic types in BWAPI are tied to in-game data and are not safe to be
used in other processes """
import inspect


class Player:
    pass

def getPublicMethodNames(obj):
    """ used to get cybw extension type method names """
    def is_public_method(obj):
        if inspect.isroutine(obj) and not obj.__name__.startswith("_"):
            return True
        return False

    return [x[0] for x in inspect.getmembers(
        obj, predicate=is_public_method)]
