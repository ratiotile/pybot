from collections import deque

class MetaBuildQueue(type):

    """ needed to get len() to work on singleton class """

    def __len__(self):
        return self.size()

class BuildQueue(object, metaclass=MetaBuildQueue):
    items = deque()

    @classmethod
    def push(cls, builditem):
        cls.items.append(builditem)

    @classmethod
    def pop(cls, builditem):
        return cls.items.popleft()

    @classmethod
    def size(cls):
        return len(cls.items)


