""" Handles resource gathering operations """
import logging, functools
import cybw
from cybw import Broodwar
from pybot.engine.harvestnode import HarvestNode

logger = logging.getLogger(__name__)


class Harvester:

    """ Harvest Manager. Singleton which controls all resource extraction, and
    manages each HarvestNode.
    """

    def __init__(self, unitmanager, taskmaster, mapinfo, bank, unitmapper):
        self.harvest_nodes = []
        self.unitmanager = unitmanager
        self.map = mapinfo
        unitmanager.ee_unitready.register(self._handleUnitReadyEvent)
        unitmanager.ee_unitselected.register(self._handleUnitSelectedEvent)
        self.workers = set()  # unassigned workers
        self.taskmaster = taskmaster
        self.bank = bank
        self.unitmapper = unitmapper

    def _handleUnitReadyEvent(self, event):
        unit = event.unit
        if unit.getType().isWorker():
            self._handleWorkerReadyEvent(event)
        elif unit.getType().isResourceDepot():
            self._handleDepotReadyEvent(event)
        elif unit.getType().isRefinery():
            self._handleRefineryReady(unit)

    def _handleWorkerReadyEvent(self, event):
        logger.info("Harvester got event worker ready: {}".format(event))
        if self.unitmanager.acquireUnit(event.unit):
            logger.info("Successfully acquired {}".format(event.unit))
            self.addWorker(event.unit)

    def _handleDepotReadyEvent(self, event):
        logger.info("Harvester got event depot ready: {}".format(event))
        self.onDepotCreate(event.unit)

    def _handleRefineryReady(self, refinery):
        # find which node it belongs to
        logger.debug("handle refinery ready {}".format(refinery))
        node = None
        for hn in self.harvest_nodes:
            if hn.hasResource(refinery):
                node = hn
                break
        if node is not None:
            if self.unitmanager.acquireUnit(refinery):
                node.onGeyserMorph(refinery)

    def _handleUnitSelectedEvent(self, event):
        if not event.unit.getType().isWorker():
            return
        found = False
        for worker in self.workers.copy():
            if worker == event.unit:
                logger.info("UnitSelected: {} found in free workers.".format(
                    event.unit))
                found = True
                self.workers.discard(worker)
                break
        if not found:  # pass onto nodes, they should remove unit
            for node in self.harvest_nodes:
                found = node.handleUnitSelected(event.unit)
                if found:
                    break

        if found:  # tell UM to put into free pool
            self.unitmanager.unitReleased(event.unit)

    def getNearestGeysers(self, tpos):
        # get set of geysers from all nodes
        geysers = []
        for node in self.harvest_nodes:
            for res in node.resources:
                if res.getType() == cybw.UnitTypes.Resource_Vespene_Geyser:
                    geysers.append(res)
        def dist(geyser):
            return geyser.getTilePosition().getApproxDistance(tpos)
        geysers.sort(key=dist)
        # sort by distance
        return geysers

    def getWorkerNear(self, pos):
        # first find closest node to pull from
        if not isinstance(pos, cybw.Position):
            pos = cybw.Position(pos)
        closestnode = None
        mindist = None
        for node in self.harvest_nodes:
            if len(node) > 0:
                dist = pos.getApproxDistance(node.depot.getPosition())
                if mindist is None or dist < mindist:
                    mindist = dist
                    closestnode = node
        if closestnode is None:
            logger.warn("Harvester.pullNearestWorker: no node found!")
            return None
        # pull the worker
        return closestnode.pullNearestWorker(pos)


    def onDepotCreate(self, newdepot):
        """ create new node for the depot if it is at unclaimed resources. We
        cannot count on terrain to be analyzed so early, so just look for nearby
        resources
        """
        if not newdepot or not newdepot.getType().isResourceDepot():
            logger.error("Did not receive a proper Depot!")

        # check to see if it is already added
        for node in self.harvest_nodes:
            if node.depot == newdepot:
                logger.warn("Depot already in existing node.")
                return

        # see if there are any resources not already included in other nodes
        unclaimed_resources = set()
        search_radius = 8*32  # 8 tiles, getPosition() is approx center of unit
        units = self.unitmapper.getUnitsInRadius(
            search_radius, newdepot.getPosition())
        for unit in units:
            if unit.getType().isResourceContainer():
                for node in self.harvest_nodes:
                    if unit in node.resources:
                        continue
                unclaimed_resources.add(unit)
        if len(unclaimed_resources) == 0:
            return
        # passes the tests, can make a new HarvestNode
        newnode = HarvestNode(newdepot, unclaimed_resources,
            taskmaster=self.taskmaster, mapinfo=self.map, bank=self.bank)
        self.harvest_nodes.append(newnode)
        logger.info("added new {} with {} resources".format(
            newnode, len(unclaimed_resources)))

    def update(self):
        if len(self.workers) > 0:
            self._assignWorkers()
        for node in self.harvest_nodes:
            node.update()

    def addWorker(self, worker):
        self.workers.add(worker)
        logger.info("added {} to Harvester".format(worker))

    def numWorkersToSaturate(self):
        """ returns number of additional workers until saturated """
        level = 0
        for node in self.harvest_nodes:
            level += node.numWorkersToSaturate()
        return level

    def workerCapacity(self):
        """ returns total number of workers to saturate all nodes """
        capacity = 0
        for node in self.harvest_nodes:
            capacity += node.max_workers
        return capacity

    def _assignWorker(self, worker):
        """send worker to node that would benefit most."""
        def bestNode(wkrpos, nnode):
            """ key based on distance to node, and how full a node is. Use
            partial to fill wkrpos before passing into sort. """
            dist = wkrpos.getApproxDistance(nnode.depot.getPosition())
            nodemax = nnode.max_workers
            nodecur = len(nnode)
            # 2 = less than 1/2 full, 1: 1/2 to nearly full, 0 for full
            if nodecur >= nodemax:
                return 99999
            else:
                fullness = 1
                if nodecur*2 < nodemax:
                    fullness = 2
                return dist / fullness
        # check if we can assign worker
        nodes = []
        for node in self.harvest_nodes:
            if not node.isFull():
                nodes.append(node)
        if len(nodes) == 0:
            logger.info("Harvester failed to assign {}. Nodes full.".format(
                worker))
            return False
        key = functools.partial(bestNode, worker.getPosition())
        nodes = sorted(nodes, key=key)
        nodes[0].addWorker(worker)
        logger.info("Harvester assigned {} to {}".format(worker, nodes[0]))
        return True

    def _assignWorkers(self):
        workers_failed = set()
        for worker in self.workers:
            # keep worker if assignment failed
            if self._assignWorker(worker) is False:
                workers_failed.add(worker)
        self.workers = workers_failed
