import logging
import cybw
from pybot.engine.task import TrainTask
from pybot.interface.prototype import TaskOwner

logger = logging.getLogger(__name__)


class Trainer(TaskOwner):

    """ Controls all unit-producing structures, while they are stationary and
    on the ground. """

    def __init__(self, unitmanager, taskmaster, bank):
        self.unitmanager = unitmanager
        self.units = set()
        self.unitmanager.ee_unitready.register(self._handleUnitReadyEvent)
        self.taskmaster = taskmaster
        self.bank = bank
        TaskOwner.__init__(self)

    def _handleUnitReadyEvent(self, event):
        unit = event.unit
        if unit.getType().isBuilding() and unit.getType().canProduce():
            if self.unitmanager.acquireUnit(unit):
                self.units.add(unit)
                logger.info("Trainer acquired {}".format(unit))
            else:
                logger.info("Trainer failed to acquire {}".format(unit))

    def getBestFactory(self, utype):
        mintime = None
        best = None
        for unit in self.units:
            if unit.canTrain(utype):
                if unit.getType() == cybw.UnitTypes.Zerg_Hatchery:
                    raise NotImplemented("Train doesn't handle hatcheries")

                if not unit.isTraining():
                    logger.info("best factory: {} to train {}".format(unit,
                                                                    utype))
                    return unit
                else:  # is currently training
                    time = unit.getRemainingTrainTime()
                    assert time != 0
                    if mintime is None or time > mintime:
                        mintime = time
                        best = unit
        assert best is not None  # should not call if there is no factory
        return best

    def numTraining(self, utype):
        total = 0
        for task in self.tasks:
            if isinstance(task, TrainTask) and task.btype == utype:
                total += 1
        return total

    def train(self, item):
        # takes BuildItem
        utype = item.action
        fact = self.getBestFactory(utype)
        task = TrainTask(utype, factory=fact, bank=self.bank,
                         unitmanager=self.unitmanager,
                         resourcerate=self.bank.rate)
        self.tasks.add(task)
        self.taskmaster.createtask(task)
        logger.info("train {}".format(utype))
        return True

    def canTrain(self, utype):
        for unit in self.units:
            if unit.canTrain(utype) and not unit.isTraining():
                return True
        return False

    def queuedResources(self):
        mineral = 0
        gas = 0
        supply = 0
        for task in self.tasks:
            if not task.reserved:
                mineral += task.btype.mineralPrice()
                gas += task.btype.gasPrice()
                supply += task.btype.supplyRequired()
        return (mineral, gas, supply)

    def numIdle(self, utype):
        total = 0
        for unit in self.units:
            if unit.getType() == utype and not unit.isTraining():
                total += 1
        return total

    def update(self):
        TaskOwner.update(self)

    def debugDraw(self, x, y, line):
        rate = self.bank.rate
        cybw.Broodwar.drawTextScreen(cybw.Position(x, y),
            "Train: ({:3.1f},{:3.1f},{:3.1f})".format(
                rate.mineral_ex, rate.gas_ex, rate.supply_ex))
        y += line
        for task in self.tasks:
            cybw.Broodwar.drawTextScreen(cybw.Position(x, y), "{}".format(
                task))
            y += line