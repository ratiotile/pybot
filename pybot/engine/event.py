import functools
import weakref
import logging

logger = logging.getLogger(__name__)

class UnitEvent:

    """ defined data package """

    def __init__(self, unit):
        self.unit = unit

    def __str__(self):
        return str(self.unit)


class EventEmitter:

    """ object for sending events, only the sending class has one """

    def __init__(self, eventcls):
        self.callbacks = set()
        self.eventcls = eventcls

    def emit(self, *args, **kwargs):
        """ push event to all its listeners via registered callbacks """

        event = self.eventcls(*args, **kwargs)
        for callback_ref in self.callbacks.copy():
            callback, arg = callback_ref
            callback = callback()
            arg = arg()
            if arg is not None and callback is not None:
                callback(arg, event)

    def eventFactory(self, *args, **kwargs):
        return self.eventcls(*args, **kwargs)

    def register(self,  callback):
        """ saves a tuple containing weakrefs to the caller and function.
        We must split the bound callback method in order to make weak refs to
        them that will stick, since we cannot make weak refs to bound methods.
        Returns a bound unregister function that requires no arguments. """

        callback_ref = (weakref.ref(callback.__func__),
                        weakref.ref(callback.__self__))
        self.callbacks.add(callback_ref)
        unreg = functools.partial(self.unregister,
            weakref.ref(callback.__self__), weakref.ref(callback.__func__))
        return unreg

    def unregister(self, listener, func):
        """ takes a separated caller, function weakref pair since bound methods
        cannot be represented by a weak ref """

        callback_ref = (func, listener)
        logger.debug("{} unregister {}.{}".format(self, listener, func))
        self.callbacks.discard(callback_ref)
        return self.register


class SymmetricEvent:

    """ Works by giving references to both emitter and receiver. The emitter
    will call emit, and the receiver can register/unregister.
    Its perfectly safe, just don't do stupid things.
    """

    def __init__(self, eventcls):
        self.listener_func = weakref.WeakKeyDictionary()
        self.eventcls = eventcls

    def emit(self, *args, **kwargs):
        """ push event to all its listeners via registered callbacks """

        event = self.eventcls(*args, **kwargs)
        for obj, func in list(self.listener_func.items()):
            func(obj, event)

    def register(self,  callback):
        """ saves weak ref to callback's self, so it shouldn't keep the object
        alive if we delete all other references to it. """

        obj = callback.__self__
        func = callback.__func__
        self.listener_func[obj] = func

    def unregister(self, callback):
        self.listener_func.pop(callback.__self__, None)

class EventListener:
    def __init__(self):
        self.unregisters = set()

    def add(self, unreg):
        self.unregisters.add(unreg)

    def cleanup(self):
        for unregister in self.unregisters:
            unregister()
