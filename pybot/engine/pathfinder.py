import heapq
from cybw import Broodwar, WalkPosition, TilePosition
import math



def defaultHValue(tpos, target):
    """ returns Heuristic A* value
    :param tpos: current tile
    :type tpos: TilePosition
    :param target: goal tile
    :type target: TilePosition
    :return: a number
    :rtype: int
    """
    dx = math.fabs(tpos.x - target.x)
    dy = math.fabs(tpos.y - target.y)
    return math.fabs(dx - dy) * 10 + min(dx, dy) * 14

def defaultGValue(tpos, prev, gtotal):
    """
    :param tpos: current tile
    :type tpos: TilePosition
    :param prev: previous position
    :type prev:  TilePosition
    :param gtotal: global g value
    :type gtotal: int
    :return: G value, step cost
    :rtype: int
    """
    gtotal += 10
    if tpos.x != prev.x and tpos.y != prev.y:
        gtotal += 4
    return gtotal

class Pathfinder:
    def __init__(self, mapinfo):
        self.map = mapinfo

    def defaultTileTest(self, tpos):
        # return Broodwar.isWalkable(WalkPosition(tpos))
        assert isinstance(tpos, TilePosition)
        if not self.map.isTileWalkable(tpos): return False
        # if not self.map.buildability_tracker.isWalkable(tpos): return False
        return True

    def createTilePath(self, start, goal,
          tiletest=None, gFunc=defaultGValue, hFunc=defaultHValue,
          max_gvalue=0, diagonal=False):
        if tiletest is None: tiletest = self.defaultTileTest
        print("pathfind {} {}".format(start, goal))
        path = []
        openheap = []
        gmap = {}  # TilePosition to int
        parent = {}  # TilePositions
        closed = set()
        heapq.heappush(openheap, (0, start))
        gmap[start] = 0
        parent[start] = start
        found = None
        while len(openheap) > 0:
            fval, p = heapq.heappop(openheap)
            gval = gmap[p]
            if p == goal or (max_gvalue != 0 and gval >= max_gvalue):
                found = p
                break
            closed.add(p)
            minx = max(p.x - 1, 0)
            maxx = min(p.x + 1, Broodwar.mapWidth()-1)
            miny = max(p.y - 1, 0)
            maxy = min(p.y + 1, Broodwar.mapHeight()-1)

            for x in range(minx, maxx + 1):
                for y in range(miny, maxy + 1):
                    if x != p.x and y != p.y and not diagonal:
                        continue
                    t = TilePosition(x, y)
                    if t in closed:
                        continue
                    if not tiletest(t):
                        continue
                    g = gFunc(t, p, gval)
                    f = g + hFunc(t, goal)
                    if t not in gmap or g < gmap[t]:
                        # print("visiting {} {}".format(t, g))
                        gmap[t] = g
                        heapq.heappush(openheap,(f, t))
                        parent[t] = p
        if found is not None:
            p = found
            while p != parent[p]:
                path.append(p)
                p = parent[p]
            path.append(start)
            # TODO: path object and isComplete
            #import ipdb; ipdb.set_trace()
        else:
            print("openheap empty")
        return path
