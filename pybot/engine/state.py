from collections import defaultdict
import cybw
from pybot.interface.unit import UnitManager


class UnitState:
    def __init__(self, unitmanager, builder):
        self.um = unitmanager
        self.um.ee_unitfinished.register(self._handleUnitFinishedEvent)
        self.um.ee_unitlost.register(self._handleUnitLostEvent)
        self.typecount = defaultdict(int)
        self.builder = builder

    def count(self, utype, planned=False):
        # returns number of completed units of utype
        assert isinstance(utype, cybw.UnitType)
        total = self.typecount[utype]
        assert total >= 0
        if planned:
            total += self.builder.getPlanned(utype)
        return total

    def havePrereqs(self, utype):
        reqs = utype.requiredUnits()
        for req, num in reqs.items():
            if self.count(req) < num:
                return False
        return True

    def _handleUnitFinishedEvent(self, event):
        self.typecount[event.unit.getType()] += 1

    def _handleUnitLostEvent(self, event):
        self.typecount[event.unit.getType()] -= 1
        assert self.typecount[event.unit.getType()] >= 0

    def debugDraw(self):
        x = 400
        y = 15
        line = 10
        cybw.Broodwar.drawTextScreen(cybw.Position(x, y), "UnitState:")
        y += line
        for utype, count in self.typecount.items():
            cybw.Broodwar.drawTextScreen(cybw.Position(x, y), "{} {}".format(
                count, utype.getName()))
            y += line
