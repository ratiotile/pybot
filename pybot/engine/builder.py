""" singleton that creates build tasks from items in the buildqueue
when resources permit """
import cybw
from cybw import TilePosition, Broodwar
from collections import deque
from cybw import Position
from pybot.engine.task import BuildTask
from pybot.engine.train import Trainer
from pybot.interface.prototype import InstanceCounter, TaskOwner

import logging

logger = logging.getLogger(__name__)


def getBuildingTileSize(building, addon=False):
    if not addon or not building.canBuildAddon():
        return building.tileSize()
    return TilePosition(building.tileWidth() + 2, building.tileHeight())


def anyBuildingsOnTile(tile):
    """ Returns True if there are any buildings blocking the specified buildtile.
    :param tile: tile to check
    :type tile: TilePosition
    :return: True if there is a blocking building, False otherwise
    :rtype: bool
    """
    units = Broodwar.getUnitsOnTile(tile)
    for unit in units:
        if unit.getType().isBuilding() and not unit.isLifted():
            return True
    return False


class BuildingPlacer:

    def __init__(self, mapinfo, unitmanager, harvester):
        self.map = mapinfo
        # Reserved positions for buildings so that we can free them later
        self.bpos_type = {}  # tilePosition to building type
        self.bpos_reserved = {}  # building tilepos to tuple tiles
        self.um = unitmanager
        self.unregUnitCreatedEvent = self.um.ee_unitcreated.register(
            self._handleUnitCreatedEvent)
        self.harvester = harvester  # for placing refineries

    def _handleUnitCreatedEvent(self, event):
        """ If unit corresponds to reserved building, clear it.
        :param event: Unit that was created
        :type event: UnitEvent
        :return: void
        :rtype: None
        """
        unit = event.unit
        utype = unit.getType()
        if not utype.isBuilding():
            return
        bpos = unit.getTilePosition()
        if bpos not in self.bpos_reserved or bpos not in self.bpos_type:
            return
        assert self.bpos_type[bpos] == utype

        tiles = self.bpos_reserved[bpos]
        logger.debug("Try to free building {} tiles at {}".format(
            unit.getType(), unit.getTilePosition()))
        self.freeBuildTiles(unit.getTilePosition(), unit.getType(), False)


    def geyserOk(self, geyser):
        # check that geyser has not been reserved.
        if geyser.getTilePosition()  in self.bpos_reserved:
            return False
        return True

    def getRefineryPlacement(self, tpos):
        # get geyser in closest ResourceNode
        geysers = self.harvester.getNearestGeysers(tpos)
        for geyser in geysers:
            # find the closest one that will do.
            if self.geyserOk(geyser):
                return geyser.getTilePosition()

    def getBuildingLocationNear(self, btype, tpos, inregion=True, builddist=0,
                                nonblocking=True):
        """ returns a building location near target TilePosition.

        Args:
            btype: UnitType of the building to place.
            tpos: TilePosition of the target to spiral out from.
            inregion: limit search to same region as tpos.
            builddist: distance to leave between buildings.
            nonblocking: will not block path out of region.

        Returns: TilePosition to top left of building spot.
        """
        assert self.map.is_analyzed
        if not tpos.isValid():
            tpos = self.map.getStartLocation()

        x, y = tpos.getXY()
        length = 1
        j = 0
        dx = 0
        dy = 1
        first = True  # control spiral rate

        while length < self.map.tileWidth():
            if 0 <= x < self.map.tileWidth() and 0 <= y < self.map.tileHeight():
                tp = TilePosition(x, y)
                canbuild = self.canBuildHereWithSpace(tp, btype, builddist,
                                                      nonblocking)
                if canbuild:
                    return tp
            # try next spot
            x += dx
            y += dy
            # length of this leg of spiral
            j += 1
            if j == length:
                j = 0  # reset step counter
                if not first:  # spiral out. Keep going
                    length += 1
                # first=True for each other turn so we spiral out at the right rate
                first = not first
                # turn counter clockwise 90 degrees
                if dx == 0:
                    dx = dy
                    dy = 0
                else:
                    dy = -dx
                    dx = 0
    # fail
        return cybw.TilePositions.none

    def canBuildHere(self, buildpos, building, nonblocking=True):
        """ Is it legal to build UnitType building here?

        Args:
            buildpos: TilePosition where we want to build.
            building: UnitType building to build.
            nonblocking: Whether to check if this will block paths out of region.

        Returns: True if we can build here and meet conditions.
        """
        assert self.map.is_analyzed
        TL = TilePosition(buildpos)
        BR = TilePosition(buildpos.x + building.tileWidth(),
                          buildpos.y + building.tileHeight())
        # check bounds
        if(TL.x < 0 or TL.y < 0 or
           BR.x >= self.map.tileWidth() or BR.y >= self.map.tileHeight()):
            return False
        # use BWAPI builtin check (proxy in order to be testable)
        if not Broodwar.canBuildHere(TL, building):
            return False
        # check buildMap
        for x in range(TL.x, BR.x):
            for y in range(TL.y, BR.y):
                if not self.map.isBuildable(TilePosition(x, y)):
                    return False
        if nonblocking and self.isBlocking(TL, BR):
            return False
        return True

    def canBuildHereWithSpace(self, buildpos, building, spacing,
                              nonblocking=True):
        """ is it legal to build here accounting for spacing and addon?

        Args:
            buildpos: TilePosition where we want to build.
            building: UnitType building to build.
            spacing: how many tiles to leave empty around building.
            nonblocking: check if this will block movement.

        Returns: True if we can build here, meeting the conditions.
        """
        if spacing == 0:
            return self.canBuildHere(buildpos, building, nonblocking)
        # leave space for addon
        width = building.tileWidth()
        height = building.tileHeight()
        if building.canBuildAddon():
            width += 2
        # define rectangle of building area
        startx = buildpos.x - spacing
        starty = buildpos.y - spacing
        endx = buildpos.x + width + spacing
        endy = buildpos.y + height + spacing
        # if rectangle doesn't fit on the map we can't build here
        if(startx < 0 or starty < 0 or
           endx > self.map.tileWidth() or endy > self.map.tileHeight()):
            return False
        # if we can't build here, or space is reserved, or in resource box
        for x in range(startx, endx):
            for y in range(starty, endy):
                if not self.map.isBuildable(TilePosition(x, y)):
                    return False
        return True

    def _getPerimeter(self, TL, BR):
        perimeter = []
        for x in range(TL.x - 1, BR.x):  # from top left rightwards
            perimeter.append(TilePosition(x, TL.y - 1))
        for y in range(TL.y - 1, BR.y):  # top right downwards
            perimeter.append(TilePosition(BR.x, y))
        for x in range(BR.x, TL.x - 1, -1):  # bottom right leftwards
            perimeter.append(TilePosition(x, BR.y))
        for y in range(BR.y, TL.y - 1, -1):  # bottom left upwards
            perimeter.append(TilePosition(TL.x - 1, y))
        return perimeter

    def _perimeterBlocked(self, perimeter):
        perimeterblocked = []  # trace around the perimeter, True if impassable
        # go clockwise from TL
        for tile in perimeter:
            perimeterblocked.append(self.isImpassable(tile))
        return perimeterblocked

    def isBlocking(self, TL, BR):
        """ will a building between TL and BR block movement paths?

        Checks that the building will not connect two impassable regions, or
        buildings.

        Args:
            TL: TilePosition of Top Left of the building
            BR: TilePosition of Bottom Right of the building

        Returns: True if movement would be blocked, False otherwise.
        """
        perimeter = self._getPerimeter(TL, BR)
        perimeterblocked = self._perimeterBlocked(perimeter)
        # check for more than 1 continuous impassable area
        lastvalue = perimeterblocked[-1]
        blockedareas = 0
        for blocked in perimeterblocked:
            if not lastvalue and blocked:
                blockedareas += 1
            lastvalue = blocked
        if blockedareas > 1:
            return True
        return False

    def reserveBuildTiles(self, tilepos, building, addon=False):
        """ reserve tiles for building

        Args:
            tilepos: TilePosition top left of building area.
            building: UnitType.
            addon: if True will reserve space for addon if building can make one.
        """
        logger.info("reserved tiles for {} at {}".format(building, tilepos))
        BR = tilepos + getBuildingTileSize(building, addon)
        tiles = self.setBuildingTiles(tilepos, BR, tilefunc=self.reserveTile)
        self.bpos_reserved[tilepos] = tuple(tiles)
        self.bpos_type[tilepos] = building


    def freeBuildTiles(self, tilepos, building, addon=False):
        BR = tilepos + getBuildingTileSize(building, addon)
        tiles = self.setBuildingTiles(tilepos, BR, tilefunc=self.freeTile)
        self.bpos_reserved.pop(tilepos)
        del self.bpos_type[tilepos]
        logger.info("freed tiles for {} at {}".format(building, tilepos))

    def reserveTile(self, tile):
        self.map.buildability_tracker.reserve(tile)
        #self.reserved_positions[tile] = building

    def freeTile(self, tile):
        self.map.buildability_tracker.free(tile)
        #del self.reserved_positions[tile]

    def setBuildingTiles(self, TL, BR, tilefunc):
        tiles = []
        for x in range(TL.x, BR.x):
            for y in range(TL.y, BR.y):
                tile = TilePosition(x, y)
                tilefunc(tile)
                tiles.append(tile)
        return tiles

    def isImpassable(self, tilepos):
        """
        :param tilepos: position to check.
        :type tilepos: `cybw.TilePosition`
        :return: True if is impassable, False otherwise
        :rtype: bool
        """
        assert self.map.is_analyzed
        if(not tilepos.isValid() or not self.map.isBuildable(tilepos) or
           not self.map.isTileWalkable(tilepos) or
           anyBuildingsOnTile(tilepos)):
            return True
        return False



    def update(self):
        self.debug_draw()

    def debug_draw(self):
        for building, tiles in self.bpos_reserved.items():
            for tile in tiles:
                x, y = tile.getXY()
                x = cybw.BtoP(x)
                y = cybw.BtoP(y)
                Broodwar.drawBoxMap(Position(x, y), Position(x+31, y+31),
                                    cybw.Colors.Green)


class BuildItem(InstanceCounter):
    def __init__(self, action, location=None):
        super().__init__()
        self.type = None
        self._setType(action, (cybw.UnitType, cybw.TechType, cybw.UpgradeType))
        self.action = action
        self.maker = None
        self.location = location
        if location is None:
            self.location = cybw.Broodwar.self().getStartLocation()

    def __str__(self):
        return "BuildItem {}: {}".format(self.id, self.action)

    def _setType(self, action, typelist):
        for atype in typelist:
            if isinstance(action, atype):
                self.type = atype
                return
        raise TypeError("action is unknown type: {}".format(action))

    def isUnit(self):
        return self.type == cybw.UnitType

    def isUpgrade(self):
        return self.type == cybw.UpgradeType

    def isTech(self):
        return self.type == cybw.TechType


class Builder(TaskOwner):
    """ Responsible for setting up and managing build tasks.
    """

    def __init__(self, build_placer, taskmaster, bank, unitmanager,
                 harvester):
        self.placer = build_placer
        self.taskmaster = taskmaster
        TaskOwner.__init__(self)
        self.queue = deque()
        self.bank = bank
        self.building_spots = {}  # for structures
        self.um = unitmanager
        self.harv = harvester
        self.trainer = Trainer(unitmanager=self.um, taskmaster=self.taskmaster,
                               bank=self.bank)

    def cancel(self, building):
        pass

    def canMake(self, action):
        """ call BWAPI builtins to check if we can make it right now

        :param action: type to check that we have ability to make
        :type action: UnitType, UpgradeType, TechType
        :return:
        :rtype:
        """
        if isinstance(action, cybw.UnitType):
            return Broodwar.canMake(action)
        if isinstance(action, cybw.TechType):
            return Broodwar.canResearch(action)
        if isinstance(action, cybw.UpgradeType):
            return Broodwar.canUpgrade(action)
        raise TypeError("action {} is not UnitType, UpgradeType, "
                        "or TechType".format(action))

    def getPlanned(self, utype):
        """
        :param utype: type to get number in production and queued
        :type utype: UnitType
        :return: total number in progress and in queue
        :rtype: int
        """
        assert isinstance(utype, cybw.UnitType)
        total = self.getQueued(utype)

        if utype.isBuilding():  # check own tasks, building tasks
            total += self.numBuilding(utype)

        elif(utype == cybw.UnitTypes.Protoss_Archon or
             utype == cybw.UnitTypes.Protoss_Dark_Archon):
            raise NotImplemented("Need special handling for building archons!")

        else:
            total += self.trainer.numTraining(utype)

        return total

    def getQueued(self, utype):
        total = 0
        for item in self.queue:
            if item.isUnit() and item.action == utype:
                total += 1
        return total

    def push(self, item):
        """ Adds item to end of build queue.

        Build Items are processing in FIFO order. Tasks are spun off once the
        requirements of each item are met.
        :param item: an Action, which may describe UnitType, Tech, or Upgrades.
        :type item: Action
        """
        if not isinstance(item, BuildItem):  # convert type and use defaults
            if(isinstance(item, cybw.UnitType) or
               isinstance(item, cybw.TechType) or
               isinstance(item, cybw.UpgradeType)):
                item = BuildItem(item)
            else:
                raise TypeError("item must be BuildItem or BWAPI type")

        self.queue.append(item)
        logger.info("pushed {} into queue."
                    " {} minerals available".format(item.action,
                                            self.bank.mineralsAvailable()))

    def queuedResources(self):
        # return resources in queue, where they have not been reserved yet
        mineral, gas, supply = self.trainer.queuedResources()
        for item in self.queue:
            mineral += item.action.mineralPrice()
            gas += item.action.gasPrice()
            if item.isUnit():
                supply += item.action.supplyRequired()
        for task in self.tasks:
            if not task.reserved:
                mineral += task.btype.mineralPrice()
                gas += task.btype.gasPrice()

        return (mineral, gas, supply)

    def numBuilding(self, btype):
        """ Get number of building type under construction

        :param btype: type of building
        :type btype: UnitType
        :return: number
        :rtype: int
        """
        total = 0
        for task in self.tasks:
            if isinstance(task, BuildTask) and task.btype == btype:
                total += 1
        return total

    def initiateBuild(self, item, btype, worker):
        self.bank.reserveType(btype)
        buildtask = BuildTask(btype, self.building_spots[item], worker,
                              self.bank, self.um)
        self.taskmaster.createtask(buildtask)
        self.tasks.add(buildtask)

    def supply(self, queued=True):
        #_if queued, also return supply not yet started
        in_progress = 0
        if queued is True:
            for item in self.queue:
                if item.isUnit():
                    in_progress += item.action.supplyProvided()
        for task in self.tasks:
            if isinstance(task, BuildTask):
                in_progress += task.btype.supplyProvided()
        if cybw.Broodwar.self().getRace() == cybw.Races.Zerg:
            in_progress += self.trainer.numTraining(
                cybw.UnitTypes.Zerg_Overlord)
        return in_progress

    def update(self):
        """ Check if front item in queue is ready, and spins off task
        """
        TaskOwner.update(self)
        self.updateQueue()
        self.trainer.update()
        self.debugDraw()

    def updateQueue(self):
        if len(self.queue) > 0:
            top = self.queue[0]
            enough_resources = self.bank.haveResourcesFor(top.action)
            if not enough_resources:
                return
            if top.isUnit():
                ut = top.action
                if ut.isBuilding():
                    r = self._updateBuilding(top, ut)
                else:  # can be handled by trainer
                    r = self._updateTrain(top)
                if r is True:
                    logger.debug("pop {} from queue".format(top.action))
                    # the reservation will be claimed by task
                    self.queue.popleft()


    def _findBuildSpot(self, item):
        btype = item.action
        if btype.isRefinery():
            spot = self.placer.getRefineryPlacement(item.location)
            assert spot.isValid()
        else:
            spot = self.placer.getBuildingLocationNear(
                btype, item.location, True, 0, True)
            assert spot.isValid()
            self.placer.reserveBuildTiles(spot, btype)

        self.building_spots[item] = spot
        logger.info("got a build spot for {}".format(btype))
        return spot

    def _updateBuilding(self, item, btype):
        """ check if we can start the task for this building

        Assume that we have enough resources for this item. Need to find a
        worker to build it, and a build spot.
        :param item: the front item of build queue
        :type item: BuildItem
        :param btype: the type of the building
        :type btype: UnitType
        :return:
        :rtype:
        """
        if item not in self.building_spots:  # find a spot
            spot = self._findBuildSpot(item)
        # try to pick up a worker
        worker = None
        for unit in self.um.free_units:
            if unit.getType().isWorker():
                worker = unit
                break
        if worker is not None:
            self.um.acquireUnit(worker)
        else:  # try to get one from Harvester
            worker = self.harv.getWorkerNear(self.building_spots[item])
        if worker is None:
            return False
        else:
            self.um.acquireUnit(worker)
        # we have everything we need now, spin up a task!
        logger.info("creating build task for {}".format(btype))
        self.initiateBuild(item, btype, worker)
        return True

    def _updateTrain(self, item):
        # initiate train task
        return self.trainer.train(item)

    def debugDraw(self):
        # draw on top left corner of screen
        x = 5
        y = 5
        line = 10
        Broodwar.drawTextScreen(Position(x, y), "Build status:")
        y += line
        for item in self.queue:
            Broodwar.drawTextScreen(Position(x, y), "{}".format(
                item.action.getName()))
            y += line
        for task in self.tasks:
            Broodwar.drawTextScreen(Position(x, y), "{}".format(
                task))
            y += line
        self.trainer.debugDraw(x, y, line)
