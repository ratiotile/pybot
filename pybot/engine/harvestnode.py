import logging
import functools
from collections import defaultdict
from pybot.engine.task import WorkerMineralTask, WorkerRefineryTask
from pybot.interface.prototype import InstanceCounter
from pybot.state import player as PS
import cybw
from cybw import Broodwar

logger = logging.getLogger(__name__)


class NodeIncome:
    """ Component of HarvestNode that calculates resource income
    """
    def __init__(self, workers, bank):
        self.workers = workers
        self.bank = bank
        self.mineral = 0
        self.gas = 0

    def update(self):
        self.mineral = 0
        self.gas = 0
        wkr_data = self.bank.rate.getData(
            [Broodwar.self().getRace().getWorker()])
        for dataq in wkr_data.values():
            for d in dataq:
                if d.unit not in self.workers:
                    continue
                self.mineral += d.minerals
        self.mineral = self.mineral * 23.81 / self.bank.rate.interval

class HarvestNode(InstanceCounter):

    """ once instance per mining base. Manages workers and attempts assign
    workers to nearer patches first. Keeps workers working their
    assigned minerals, to improve efficiency """

    def __init__(self, depot, resources, taskmaster, mapinfo, bank):
        """
        :param depot: ResourceDepot, the collection center of this node
        :type depot: Unit
        :param resources: Set of resources gathered by this node
        :type resources:  Set(Unit)
        :param taskmaster: Master Task scheduler
        :type taskmaster: TaskMaster
        :param mapinfo: map information handler
        :type mapinfo: MapInfo
        """
        InstanceCounter.__init__(self)
        self.depot = depot
        self.resources = resources  # a set of resources
        self.refineries = set()
        # resource unit to set of workers
        self.resource_workers = defaultdict(set)
        self.max_workers = 0
        self.workers = set()
        self.taskmaster = taskmaster
        self.worker_task = {}  # worker to its task
        self.map = mapinfo
        self.bank = bank
        self.income = NodeIncome(self.workers, self.bank)

        self.map.buildability_tracker.addResourceArea(resources, depot)

        for res in resources:
            self.resource_workers[res] = set()
        self._calcMaxWorkers()

    def __len__(self):
        """ returns number of workers """
        return len(self.workers)

    def __str__(self):
        return "HarvestNode:{}".format(self.id)

    def _calcMaxWorkers(self):
        """ 3 for each gas, 2 for each mineral. """
        val = 0
        for resource in self.resources:
            if resource.getType().isMineralField():
                val += 2
            elif(resource.getType().isRefinery() and
                 resource.getPlayer() == PS.ownplayer):
                val += 3
        self.max_workers = val
        logger.info("{} set max_workers to {}".format(self, self.max_workers))



    def addWorker(self, worker):
        """ Unit worker - to work mining minerals or gas """
        self.workers.add(worker)
        self.clearWorkerAssignments(worker)
        refinery = self.findOpenRefinery()
        if refinery is not None:
            self.assignWorkerRefinery(worker, refinery)
        else:
            mineral = self.findOpenMineral()
            if mineral is not None:
                self.assignWorkerMineral(worker, mineral)
            else:
                logger.error("Could not find any assignment for {}".format(
                    worker
                ))
                return False
        return True

    def assignWorkerMineral(self, worker, bestmineral):
        self.resource_workers[bestmineral].add(worker)

        task = WorkerMineralTask(worker, self.depot, bestmineral, self.bank)
        self.taskmaster.createtask(task)
        self.worker_task[worker] = task  # save task to remove later.
        return True

    def assignWorkerRefinery(self, worker, refinery):
        """ Send worker to work in the refinery
        Assumes that the refinery has open spots.
        :param worker: worker unit to be sent to mine gas
        :type worker: Unit
        :param refinery: refinery unit to be worked
        :type refinery: Unit
        :return: None
        :rtype: void
        """
        self.resource_workers[refinery].add(worker)
        task = WorkerRefineryTask(worker, self.depot, refinery, self.bank)
        self.taskmaster.createtask(task)
        self.worker_task[worker] = task

    def clearWorkerAssignments(self, worker):
        if worker in self.worker_task.keys():
            logger.info("HarvestNode: killing task")
            task = self.worker_task[worker]
            self.taskmaster.killtask(task)
            task.terminate()
            del self.worker_task[worker]
        # clear the worker from any assigned resource
        for wkrset in self.resource_workers.values():
            wkrset.discard(worker)

    def findOpenMineral(self):
        """ find nearest mineral which is not full, but prefer to
            assign 1 worker to each mineral before doubling up. """
        minerals = []
        for res in self.resources:
            if (res.getType().isMineralField() and
                        len(self.resource_workers[res]) < 2):
                minerals.append(res)
        if len(minerals) == 0:
            logger.warning("{} could not find open mineral".format(self,))
            return None

        def keymineral(this, mineral):
            """ return a score for a mineral based on distance and workers """
            distance = this.depot.getDistance(mineral)
            # avoid divide-by zero
            numwkr = 1 + len(this.resource_workers[mineral])
            # print("distance:{}, num:{}".format(distance, numwkr))
            return distance * numwkr

        boundkey = functools.partial(keymineral, self)
        sortedminerals = sorted(minerals, key=boundkey)
        bestmineral = sortedminerals[0]
        return bestmineral

    def findOpenRefinery(self):
        for ref in self.refineries:
            if len(self.resource_workers[ref]) <= 3:
                return ref
        return None

    def findNearestWorker(self, pos):
        closest = None
        if len(self.workers) == 0: return None

        mindist = None
        carrymod = 160
        gasmod = 320
        # try for mineral gathering worker not carrying resources
        for worker in self.workers:
            dist = pos.getApproxDistance(worker.getPosition())
            if worker.isGatheringGas(): dist += gasmod
            if worker.isCarryingGas() or worker.isCarryingMinerals():
                dist += carrymod
            if mindist is None or dist < mindist:
                mindist = dist
                closest = worker
        return closest

    def handleUnitSelected(self, unit):
        """ called by Harvester. The node will end all tasks and release the
        unit if found.
        """
        for worker in self.workers.copy():
            if worker == unit:
                logger.info("selected {} found!".format(unit))
                self.remove(worker)
                return True
        return False  # didn't find it

    def hasResource(self, resource):
        return resource in self.resources

    def isFull(self):
        return self.max_workers <= len(self)

    def numWorkersToSaturate(self):
        return self.max_workers - len(self)

    def onGeyserMorph(self, geyser):
        """ increase/decrease max workers by 3, depending on if refinery was
        built or destroyed """
        if geyser.getType().isRefinery() and geyser.getPlayer() == PS.ownplayer:
            self.max_workers += 3
            logger.debug("Refinery {} ready, increased maxWorkers by 3".format(
                geyser
            ))
            self.refineryReady(geyser)
        else:
            self.max_workers -= 3
            self.refineryDestroyed(geyser)

    def onMineralDestroyed(self, mineral):
        """ decrease max workers by 2 """
        self.max_workers -= 2

    def pullNearestWorker(self, pos):
        worker = self.findNearestWorker(pos)
        if worker is None:
            logger.warn("HarvestNode could not find a worker!")
            return None
        self.remove(worker)
        return worker

    def refineryDestroyed(self, refinery):
        pass

    def refineryReady(self, refinery):
        """ The HarvestNode will now accept more workers to work the refinery

        Assumes that the refinery is complete and part of this resource node.
        :param refinery: Refinery
        :type refinery: Unit
        :return: None
        :rtype: void
        """
        self.refineries.add(refinery)

    def remove(self, worker):
        self.clearWorkerAssignments(worker)
        self.workers.discard(worker)

    def update(self):
        """ do various checks on each frame. When workers mine minerals, they
        will have a frame of no target, and an order ResetCollision when they
        are about to return to the ResourceDepot. """
        self.income.update()
        self.debugDraw()

    def debugDraw(self):
        for worker in self.workers:
            Broodwar.drawCircleMap(worker.getPosition(), 15, cybw.Colors.Green)
        for mineral, wset in self.resource_workers.items():
            for worker in wset:
                Broodwar.drawLineMap(worker.getPosition(),
                    mineral.getPosition(), cybw.Colors.Teal)
        Broodwar.drawTextMap(self.depot.getPosition() - cybw.Position(0,10),
            "min/s:{0:5.2f}".format(self.income.mineral))
