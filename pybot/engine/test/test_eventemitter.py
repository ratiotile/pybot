from pybot.engine.event import UnitEvent, EventEmitter
import weakref

class FakeClass:
    def __init__(self, id=1):
        self.unit = None
        self.unregister = None
        self.id = id

    def inc(self, event):
        self.unit = event.unit
        print(str(self.id) + event.unit)



ee = EventEmitter(UnitEvent)

test1 = FakeClass()
test1.unregister = ee.register(test1.inc)

test2 = FakeClass(2)
test2.unregister = ee.register(test2.inc)
ee.emit("_1")

ee.emit("_2")

test1 = None

ee.emit("_3")

test2.unregister()
ee.emit("_4")
print(test2.inc.__name__)