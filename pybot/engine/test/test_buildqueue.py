from pybot.engine.buildqueue import BuildQueue

def test_BuildQueue_len():
    assert len(BuildQueue) == 0
    BuildQueue.push("test")
    assert len(BuildQueue) == 1
