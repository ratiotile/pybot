import cybw
from cybw import TilePosition
from pybot.interface.unit import UnitManager, Unit
from pybot.engine.builder import BuildingPlacer
from pybot.interface.territory import MapInfo
from pybot.engine import builder
from pybot.engine.test.test_harvester import harvestnodeFactory, \
    geyserFactory, harvester
import pytest

@pytest.fixture(autouse=True)
def nop_anyBuildingsOnTile(monkeypatch):
    monkeypatch.setattr(builder, "anyBuildingsOnTile",
                        lambda x: False)

@pytest.fixture
def mapInfo():
    mi = MapInfo()
    mi.is_analyzed = True
    # monkey patch these BW dependencies
    mi.isAreaWalkable = lambda *args: True
    mi._isBuildable = lambda *args: True
    return mi

@pytest.fixture
def BP(mapInfo):
    um = UnitManager()
    bp = BuildingPlacer(mapInfo, um, None)
    return bp


class TestBuildPlacer:
    def test_monkeypatch(self):
        assert builder.anyBuildingsOnTile(cybw.TilePosition(1,1)) == False

    def test_map_assumptions(self, mapInfo):
        tile = cybw.TilePosition(10, 10)
        assert mapInfo.isTileWalkable(tile) == True
        assert mapInfo.isBuildable(tile) == True

    def test_isImpassable(self, mapInfo):
        um = UnitManager()
        bp = BuildingPlacer(mapInfo, um, None)
        tile = cybw.TilePosition(10,10)
        mapInfo.buildability_tracker.reserve(tile)
        assert bp.isImpassable(tile) == True
        opentile = cybw.TilePosition(10,20)
        assert bp.isImpassable(opentile) == False

    def test_perimeter(self, BP):
        TL = cybw.TilePosition(10, 10)
        BR = cybw.TilePosition(11, 11)
        perimeter = BP._getPerimeter(TL, BR)
        #print([str(x) for x in perimeter])
        assert perimeter[0] == TilePosition(9, 9)
        assert perimeter[1] == TilePosition(10, 9)
        assert perimeter[2] == TilePosition(11, 9)
        assert perimeter[3] == TilePosition(11, 10)
        assert perimeter[4] == TilePosition(11, 11)
        assert perimeter[5] == TilePosition(10, 11)
        assert perimeter[6] == TilePosition(9, 11)
        assert perimeter[7] == TilePosition(9, 10)

    def test_perimeterBlocked(self, BP):
        TL = cybw.TilePosition(10, 10)
        BR = cybw.TilePosition(11, 11)
        perimeter = BP._getPerimeter(TL, BR)
        pb = BP._perimeterBlocked(perimeter)
        # print(perimeter)
        assert len(pb) == 8
        for tile in pb:
            assert tile == False

    def test_isBlocking_simple(self, BP):
        TL = cybw.TilePosition(10, 10)
        BR = cybw.TilePosition(11, 11)
        assert BP.isBlocking(TL, BR) == False

    def test_isBlocking_wall(self, BP):
        """
            0 0 0
            T B 0
            0 0 0
        """

        TL = cybw.TilePosition(0, 1)
        BR = cybw.TilePosition(1, 2)
        blocker = cybw.TilePosition(1, 1)
        assert BP.isImpassable(TL) == False
        assert BP.isBlocking(TL, BR) == False
        BP.map.buildability_tracker.reserve(blocker)
        assert BP.isImpassable(blocker) == True
        assert BP.isBlocking(TL, BR) == True

    def test_geyserOk(self, BP):
        geyser = Unit()
        setattr(geyser, "getTilePosition", lambda: TilePosition(1, 1))
        assert geyser.getTilePosition() == TilePosition(1, 1)
        assert BP.geyserOk(geyser) == True
        BP.reserveBuildTiles(TilePosition(1, 1),
                             cybw.UnitTypes.Resource_Vespene_Geyser)
        assert BP.geyserOk(geyser) == False

    def test_getRefineryPlacement(self, BP, harvester, mapInfo):
        g1 = geyserFactory(TilePosition(5, 5))
        g2 = geyserFactory(TilePosition(20, 1))
        assert g2.getTilePosition() == TilePosition(20, 1)
        harvester.harvest_nodes.append(
            harvestnodeFactory(g1, mapInfo)
        )
        harvester.harvest_nodes.append(
            harvestnodeFactory(g2, mapInfo)
        )
        BP.harvester = harvester
        assert BP.getRefineryPlacement(TilePosition(4, 2)) == TilePosition(5, 5)
        nearest = BP.harvester.getNearestGeysers(TilePosition(20, 1))
        assert nearest[0] == g2
        print("nearest from harvester: {}".format(nearest[0].getTilePosition()))
        print("farthest from harvester: {}".format(nearest[
            1].getTilePosition()))
        print("g2 {}".format(g2.getTilePosition()))
        nfbp = BP.getRefineryPlacement(TilePosition(20, 1))
        print("nearest from BP: {}".format(nfbp))
        assert nfbp == g2.getTilePosition()

if __name__ == "__main__":
    um = UnitManager()
    mi = mapInfo()
    bp = BuildingPlacer(mi, um)
    tile = cybw.TilePosition(10, 10)

    mi.buildability_tracker.reserve(tile)
    assert mi.isBuildable(tile) == False
    opentile = cybw.TilePosition(10, 20)
    print(mi.buildability_tracker.isBuildable(opentile))