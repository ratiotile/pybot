import cybw
from cybw import TilePosition
from pybot.engine.harvester import Harvester
from pybot.engine.harvestnode import HarvestNode
from pybot.engine.task import TaskMaster
from pybot.interface.territory import MapInfo
import pytest
from pybot.interface.unit import UnitManager, Unit


@pytest.fixture
def mapInfo():
    mi = MapInfo()
    mi.is_analyzed = True
    # monkey patch these BW dependencies
    mi.isAreaWalkable = lambda *args: True
    mi._isBuildable = lambda *args: True
    return mi

@pytest.fixture
def harvester(mapInfo):
    um = UnitManager()
    tm = TaskMaster()
    harv = Harvester(um, tm, mapInfo, None)
    return harv

def unitFactory(tpos, ut):
    unit = Unit()
    setattr(unit, "getTilePosition", lambda: tpos)
    setattr(unit, "getType", lambda: ut)
    return unit

def harvestnodeFactory(geyser, mapInfo):
    tm = TaskMaster()
    return HarvestNode(unitFactory(TilePosition(1, 1),
                                   cybw.UnitTypes.Terran_Command_Center),
                       [geyser], tm, mapInfo, None)

def geyserFactory(tpos):
    return unitFactory(tpos, cybw.UnitTypes.Resource_Vespene_Geyser)

class TestHarvester:
    def test_getNearestGeysers(self, harvester, mapInfo):
        geyser1 = geyserFactory(TilePosition(1, 1))
        geyser2 = geyserFactory(TilePosition(20, 1))
        harvester.harvest_nodes.append(
            harvestnodeFactory(geyser1, mapInfo)
        )
        harvester.harvest_nodes.append(
            harvestnodeFactory(geyser2, mapInfo)
        )
        nearest = harvester.getNearestGeysers(TilePosition(4, 4))
        assert len(nearest) == 2
        assert nearest[0].getTilePosition() == geyser1.getTilePosition()
        assert nearest[1].getTilePosition() == geyser2.getTilePosition()
        nearest = harvester.getNearestGeysers(TilePosition(22, 1))
        assert nearest[0].getTilePosition() == geyser2.getTilePosition()
        assert nearest[1].getTilePosition() == geyser1.getTilePosition()