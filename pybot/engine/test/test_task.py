from greenlet import greenlet
from pybot.engine.task import PriorityTaskManager, TaskPriorityQueue, Task, \
    TaskMaster


class FakeTask(Task):

    def __init__(self, val):
        super(FakeTask, self).__init__()
        self.value = val

    def run(self):
        while True:
            greenlet.getcurrent().parent.switch(self.value)


def test_TaskPriorityQueue_add_pop():
    tpq = TaskPriorityQueue()
    tt = FakeTask("hello")
    tpq.add(tt)
    assert len(tpq) == 1
    popped = tpq.pop()
    assert popped is tt
    assert len(tpq) == 0

def test_TaskPriorityQueue_peek():
    tpq = TaskPriorityQueue()
    tt = FakeTask("hello")
    tpq.add(tt)
    peeked = tpq.peek()
    assert peeked is tt
    assert len(tpq) == 1

def test_TaskPriorityQueue_priority():
    tpq = TaskPriorityQueue()
    t1 = FakeTask(None)
    t2 = FakeTask(None)
    t3 = FakeTask(None)
    tpq.add(t3, 3)
    tpq.add(t1, 1)
    tpq.add(t2, 2)
    assert tpq.peek() == t1
    tpq.remove(t1)
    assert tpq.peek() == t2
    tpq.remove(t2)
    assert tpq.peek() == t3

def test_PriorityTaskmanager():
    tm = PriorityTaskManager()
    t1 = FakeTask(None)
    tm.add(t1, 1)
    assert len(tm) == 1
    tm.remove(t1)
    assert len(tm) == 0

def test_PriorityTaskmanager_run():
    tm = PriorityTaskManager()
    t1 = FakeTask(1)
    tm.add(t1, 0)
    val = tm.run()
    assert val == 1

def test_PriorityTaskmanager_priority():
    tm = PriorityTaskManager()
    t1 = FakeTask(1)
    t2 = FakeTask(2)
    t3 = FakeTask(3)
    tm.add(t1, 1)
    tm.add(t2, 2)
    tm.add(t3, 3)
    val = tm.run()
    assert val == 1
    tm.remove(t1)
    val = tm.run()
    assert val == 2
    tm.remove(t2)
    val = tm.run()
    assert val == 3

class Test_TaskMaster:
    def test_timeout_when_killtask(self):
        """" TaskMaster has issue where it will timeout on the frame when one of
        its tasks is killed """
        tm = TaskMaster()
        t1 = FakeTask(1)
        t2 = FakeTask(2)
        t3 = FakeTask(3)
        tm.createtask(t1)
        tm.createtask(t2)
        tm.createtask(t3)
        assert tm.update() == 'cycle'
        tm.killtask(t1)
        assert tm.update() == 'cycle'  # this one is timeout