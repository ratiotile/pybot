from heapq import heappush, heappop
from collections import deque
from greenlet import greenlet
import time
import logging
import itertools

from enum import Enum
import cybw

from pybot import settings
from pybot.interface.prototype import InstanceCounter
from pybot.state.resource import Reserve
from pybot.interface.utility import center


logger = logging.getLogger(__name__)


class TaskMaster:

    """ Schedules and runs tasks """

    def __init__(self):
        self.tasks = set()  # to check existence
        self.queue = deque()  # Task run order, circular queue
        self.gloop = greenlet(self.taskloop)
        self.begin_time = None  # Time at start of taskloop
        self.timeout = 0.04
        self.ticks = 0  # count how many tasks executed this cycle
        self.first_task = None  # used to break cycle

    def createtask(self, task):
        # needed to make task greenlet yield in proper spot
        task.glet.parent = self.gloop
        self.tasks.add(task)
        self.queue.append(task)

    def killtask(self, task):
        """ immediately terminate task and remove from queue """
        if task in self.tasks:
            logger.info("Taskmaster: kill task {}".format(task))
            self.tasks.remove(task)
            self.queue.remove(task)
            return True
        return False

    def update(self):
        """ will set up for this frame and continue coroutine """
        self.first_task = None  # so taskloop will update it this cycle
        self.ticks = 0
        self.begin_time = time.clock()
        val = self.gloop.switch()
        return val

    def taskloop(self):
        # loop forever over cycles
        while True:
            while len(self.queue) == 0:
                # exit if queue is empty
                greenlet.getcurrent().parent.switch("empty")
            task = self.queue.popleft()  # these two must be atomic
            if not task.isFailed() and not task.isCompleted():
                self.queue.append(task)  # make queue circular
            else:
                task.terminate()
                self.tasks.remove(task)
                continue

            if self.first_task == task or self.ticks >= len(self.queue):
                greenlet.getcurrent().parent.switch("cycle")
            if self.first_task is None:
                self.first_task = task

            # execute task if it has all preconditions satisfied
            task.update()
            self.ticks += 1

            if time.clock() - self.begin_time >= self.timeout:
                logger.warn("timeout in taskloop!")
                greenlet.getcurrent().parent.switch("timeout")


class UnitScheduler:
    """ Schedules units to run their tasks

    Instead of having tasks sitting out by themselves, since a unit can only
    run a single task at a time anyways, divide up the tasks by the unit that
    they control. Each unit gets a slot in which to run its highest priority
    task, and units will keep a priority queue of running tasks.
    """
    def __init__(self):
        self.unitqueue = deque()
        self.timeout = 0.03

    def add(self, unit):
        self.unitqueue.append(unit)

    def remove(self, unit):
        # remove a unit from task manager. may not be needed as units should
        # remove themselves when they have no more tasks? Or simply add all
        # units to task manager anyways and remove them when they die.
        if unit in self.unitqueue:
            self.unitqueue.remove(unit)

    def update(self):
        """ Run update on each unit
        :return:
        :rtype:
        """
        ticks = 0  # count number of units processed, exit when >= length
        begin_time = time.clock()
        while ticks < len(self.unitqueue) > 0:
            unit = self.unitqueue.popleft()
            self.unitqueue.append(unit)
            ticks += 1
            if time.clock() > begin_time + self.timeout:
                print("timed out")
                return


class PriorityTaskManager:
    """ Runs only the highest priority task each cycle.
    """
    def __init__(self):
        self.taskq = TaskPriorityQueue()
        self.task_glet = {}  # task to greenlet

    def __len__(self):
        return len(self.taskq)

    def add(self, task, priority):
        gtask = greenlet(task.run)
        self.task_glet[task] = gtask
        self.taskq.add(task, priority)

    def remove(self, task):
        if task in self.task_glet.keys():
            self.task_glet[task].throw()
            del self.task_glet[task]
            self.taskq.remove(task)
            return True
        return False

    def run(self):
        if len(self.taskq) > 0:
            top = self.taskq.peek()
            val = self.task_glet[top].switch()
            return val
        return None


class TaskPriorityQueue:
    """ A sort-stable priority queue for Tasks
    """
    def __init__(self):
        self.pq = []
        self.entry_finder = {}
        self.REMOVED = object()
        self.counter = itertools.count()

    def __len__(self):
        return len(self.entry_finder)

    def add(self, task, priority=0):
        count = next(self.counter)
        entry = [priority, count, task]
        self.entry_finder[task] = entry
        heappush(self.pq, entry)

    def remove(self, task):
        entry = self.entry_finder.pop(task)
        entry[-1] = self.REMOVED

    def pop(self):
        while self.pq:
            priority, count, task = heappop(self.pq)
            if task is not self.REMOVED:
                del self.entry_finder[task]
                return task
        raise KeyError('Pop from empty priority queue')

    def peek(self):
        while self.pq[0][-1] is self.REMOVED:
            heappop(self.pq)
        return self.pq[0][-1]


class Task:

    class Status(Enum):
        inactive = 1
        active = 2
        completed = 3
        failed = 4

    def __init__(self):
        self.status = Task.Status.inactive
        self.glet = greenlet(self.run)

    def activate(self):
        """ activates if inactive """
        if self.isActive():
            return False
        if self.check():
            self.status = Task.Status.active
            return True
        return False

    def check(self):
        """ ensure preconditions of task are met.

        Called by update()
        """
        return True

    def update(self):
        """ try to activate task, or terminate if cannot complete.

        TaskMaster should call update on tasks. TaskMaster calls cleanup
        """
        if self.isInactive():
            self.activate()
        if self.isCompleted() or self.isFailed():
            return
        if not self.check():
            print("task {} failed check".format(self))
            self.status = Task.Status.failed
            #self.cleanup()
            return
        if self.isActive():
            if not self.glet.dead:
                self.glet.switch()
            #if self.isCompleted() or self.isFailed():
            #    self.cleanup()
            return

    def run(self):
        """ this method is run as a greenlet """
        pass

    def terminate(self):
        if not self.glet.dead:
            self.glet.throw()
        if self.isActive() or self.isCompleted():
            if not self.isCompleted():
                self.status = Task.Status.failed
            self.cleanup()

    def cleanup(self):
        pass

    def isInactive(self):
        return self.status is Task.Status.inactive

    def isActive(self):
        return self.status is Task.Status.active

    def isCompleted(self):
        return self.status is Task.Status.completed

    def isFailed(self):
        return self.status is Task.Status.failed


class WorkerMineralTask(Task):

    """ oversee a worker gather from a single mineral patch """

    def __init__(self, worker, depot, mineral, bank):
        self.worker = worker
        self.depot = depot
        self.mineral = mineral
        self.bank = bank
        self.spam_interval = 6  # number of frames to wait between clicks
        self.spamcount = self.spam_interval
        self.cycle_time = 0  # track this worker's efficiency
        super().__init__()

    def check(self):
        if not self.worker.exists():
            return False
        if not self.depot.exists():
            return False
        if not self.mineral.exists():
            return False
        return True

    def drawCycleTime(self):
        pos = self.worker.getPosition()
        cybw.Broodwar.drawTextMap(pos, str(self.cycle_time))

    def update(self):
        if self.isActive():
            self.spamcount += 1
        super().update()
        if settings.debug_draw.worker_cycle:
            self.drawCycleTime()

    def run(self):
        worker = self.worker
        mineral = self.mineral
        minclickdist = 4
        cycle_frames = 0
        while True:
            while(not worker.isCarryingMinerals() and
                  not worker.getOrder() == cybw.Orders.MiningMinerals):
                cycle_frames += 1
                order = worker.getOrder()
                try:
                    target = worker.getTarget()
                    if(not worker.isGatheringMinerals() or
                      target != mineral):
                        worker.rightClick(mineral)
                        print("try to change target")
                    elif(self.spamcount > self.spam_interval and
                         worker.getDistance(mineral) > minclickdist):
                        worker.rightClick(mineral)
                        self.spamcount = 0
                except cybw.NullPointerException:
                    if order != cybw.Orders.ResetCollision:
                        print("{}, get back to work!".format(worker))
                        worker.rightClick(mineral)
                greenlet.getcurrent().parent.switch()
            self.spamcount = self.spam_interval + 1  # allow immediate spam on depot
            while worker.getOrder() == cybw.Orders.MiningMinerals:
                cycle_frames += 1
                greenlet.getcurrent().parent.switch()
            worker.rightClick(self.depot)
            while worker.isCarryingMinerals():
                cycle_frames += 1
                if self.spamcount > self.spam_interval:
                    # spamming depot actually slows them down!
                    worker.rightClick(self.depot)
                self.spamcount = 0
                greenlet.getcurrent().parent.switch()
            # dropped off minerals, update the bank!
            self.bank.rate.addData(mineral=8, gas=0, supply=0, unit=self.worker)
            cycle_frames += 1
            self.cycle_time = cycle_frames
            cycle_frames = 0
            greenlet.getcurrent().parent.switch()


class WorkerRefineryTask(Task):
    """ just like the WorkerMineralTask except that no spamming is necessary
    """
    def __init__(self, worker, depot, refinery, bank):
        self.worker = worker
        self.depot = depot
        self.refinery = refinery
        self.bank = bank
        self.cycle_time = 0  # track this worker's efficiency
        super().__init__()

    def check(self):
        if not self.worker.exists():
            return False
        if not self.depot.exists():
            return False
        if not self.refinery.getType().isRefinery():
            return False
        return True

    def drawCycleTime(self):
        pos = self.worker.getPosition()
        cybw.Broodwar.drawTextMap(pos, str(self.cycle_time))

    def update(self):
        super().update()
        if settings.debug_draw.worker_cycle:
            self.drawCycleTime()

    def run(self):
        worker = self.worker
        refinery = self.refinery
        cycle_frames = 0
        while True:
            while (not worker.isCarryingGas() and
                       not worker.getOrder() == cybw.Orders.HarvestGas):
                cycle_frames += 1
                order = worker.getOrder()
                try:
                    target = worker.getTarget()
                    if (not worker.isGatheringGas() or
                                target != refinery):
                        worker.rightClick(refinery)
                        print("worker try to change target")
                except cybw.NullPointerException:
                    if order != cybw.Orders.ResetCollision:
                        print("{}, get back to work!".format(worker))
                        worker.rightClick(refinery)
                greenlet.getcurrent().parent.switch()
            while(worker.getOrder() == cybw.Orders.HarvestGas or
                  worker.getOrder() == cybw.Orders.WaitForGas):
                cycle_frames += 1
                greenlet.getcurrent().parent.switch()
            worker.rightClick(self.depot)
            while worker.isCarryingGas():
                cycle_frames += 1
                greenlet.getcurrent().parent.switch()
            # dropped off gas, update the bank!
            self.bank.rate.addData(mineral=0, gas=8, supply=0, unit=self.worker)
            cycle_frames += 1
            self.cycle_time = cycle_frames
            cycle_frames = 0
            greenlet.getcurrent().parent.switch()

class TrainTask(Task, InstanceCounter):
    def __init__(self, utype, factory, bank, unitmanager, resourcerate):
        super().__init__()
        InstanceCounter.__init__(self)
        self.btype = utype
        self.bank = bank
        self.factory = factory
        self.um = unitmanager
        self.ratetracker = resourcerate
        self.unreg_unitready_event = unitmanager.ee_unitfinished.register(
            self._handleUnitReadyEvent)
        self.reserved = False

    def __str__(self):
        return "Train {}-{}".format(self.id, self.btype)

    def _handleUnitReadyEvent(self, event):
        unit = event.unit
        if not self.isActive():
            return
        if unit.getType() != self.btype:
            return
        if self.factory.isTraining():
            #logger.info("{} factory still training.".format(self))
            return  # make sure our factory is done
        # if it is right type and our factory is done
        logger.debug("{} attempt to claim production of {}".format(
            self, unit
        ))
        if self.um.claimProduction(unit):
            logger.info("{} detected unit, is complete!".format(self))
            self.status = Task.Status.completed

    def run(self):
        tt = self.btype
        while not self.bank.haveResourcesFor(self.btype):
            greenlet.getcurrent().parent.switch()
        with Reserve(self.bank, self.btype):
            self.reserved = True
            # wait until we can train the unit and factory is ready
            while(not self.factory.canTrain() or
                  not cybw.Broodwar.canMake(tt) or
                  self.factory.isTraining() or
                  self.factory.isLifted()):
                greenlet.getcurrent().parent.switch()
            # assume we are ready now
            while True:
                r = self.factory.train(tt)
                if not r:
                    logger.error("Failed to train {} in {}".format(tt,
                                                                   self.factory))
                    self.status = Task.Status.failed
                    greenlet.getcurrent().parent.switch()
                else:
                    self.ratetracker.addData(
                        mineral=-tt.mineralPrice(),
                        gas=-tt.gasPrice(),
                        supply=tt.supplyProvided() - tt.supplyRequired(),
                        unit=self.factory)
                    break
        while True:  # wait for completion
            greenlet.getcurrent().parent.switch()
        self.status = Task.Status.completed
        return True

    def cleanup(self):
        self.unreg_unitready_event()


class BuildTask(Task):
    """ Build a structure 
    """
    def __init__(self, btype, buildpos, worker, bank, unitmanager):
        super(BuildTask, self).__init__()
        self.btype = btype
        self.bank = bank
        self.worker = worker
        self.buildtile = buildpos
        self.close_enough = 120
        self.structure = None
        self.um = unitmanager
        # need to register to get new building
        self.unreg_unit_event = unitmanager.ee_unitcreated.register(
            self.handle_unitcreated_event)
        self.reserved = False

    def __str__(self):
        return "Build {}".format(self.btype.getName())

    def handle_unitcreated_event(self, event):
        unit = event.unit
        if(unit.getType() == self.btype and
          unit.getTilePosition() == self.buildtile):
            self.structure = unit

    def check(self):
        """ Make sure to fail if something is messed up."""
        if self.worker is not None and not self.worker.exists():
            # worker is dead
            self.status = Task.Status.failed
            return False
        if self.structure is not None and not self.structure.exists():
            # structure has died
            self.status = Task.Status.failed
            return False
        return True

    def run(self):
        self.worker.stop()
        spamcount = 0
        logger.info("run build task")
        # assume we have resources since Builder reserved it for us
        self.bank.releaseType(self.btype)
        # assume we already have worker and everything
        with Reserve(self.bank, self.btype):
            self.reserved = True
            # move worker to location
            logger.info("Reserved resources, move worker to site.")
            sitecenter = center(self.buildtile, self.buildtile +
                                self.btype.tileSize())
            self.worker.move(cybw.Position(self.buildtile))
            buildpos = (cybw.Position(sitecenter))
            dist_tosite = self.worker.getDistance(buildpos)
            while dist_tosite > self.close_enough:
                dist_tosite = self.worker.getDistance(buildpos)
                spamcount += 1
                if self.worker.isIdle() or spamcount > 3:
                    self.worker.move(buildpos)
                    spamcount = 0
                greenlet.getcurrent().parent.switch()
            # todo: clear site
            # build structure
            logger.info("ordering build.")
            r = self.worker.build(self.btype, self.buildtile)
            if r: logger.info("success.")
            else: logger.info("failed.")
            while self.structure is None:
                if self.worker.isIdle():
                    print("ordering build")
                    self.worker.build(self.btype, self.buildtile)
                greenlet.getcurrent().parent.switch()
        # building has started
        race = cybw.Broodwar.self().getRace()
        if race == cybw.Races.Zerg:
            # worker mutated into the structure
            logger.debug("race zerg, worker mutated")
            self.worker = None
            # Unitmanager already got event onMorph and dealt with worker
        elif race == cybw.Races.Protoss:
            # can let go of probe now
            logger.debug("race Protoss, worker released")
            self.um.unitReleased(self.worker)
            self.worker = None
        # wait for build to finish
        while not self.structure.isCompleted():
            greenlet.getcurrent().parent.switch()
        # Structure is complete!
        self.status = Task.Status.completed
        return True

    def cleanup(self):
        logger.debug("{} cleanup.".format(self))
        self.unreg_unit_event()
        self.structure = None
        if self.worker:
            self.um.unitReleased(self.worker)
        else:
            logger.warn("{} cleanup has None for worker".format(self))
        self.worker = None
