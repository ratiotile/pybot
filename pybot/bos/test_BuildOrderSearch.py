from pybot.primitive.bwapitype import UnitTypes
from pybot.bos.ActionType import Action
from pybot.bos.BuildOrderSearch import DFBB_BOS


def test_DFBB_BOS_sortActionsByLowerBound():
    BOS = DFBB_BOS()
    scv = Action(UnitTypes.Terran_SCV)
    rine = Action(UnitTypes.Terran_Marine)
    rax = Action(UnitTypes.Terran_Barracks)
    actions = [scv, rine, rax]
    actions[0].lower_bound = 10
    actions[1].lower_bound = 20
    actions[2].lower_bound = 14
    BOS.sortActionsByLowerBound(actions)
    assert actions[0] == scv
    assert actions[1] == rax
    assert actions[2] == rine
