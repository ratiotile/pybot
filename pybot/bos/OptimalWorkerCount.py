from pybot.primitive.bwapitype import Races
from .ActionType import Action, ActionTypes
from .SearchState import State
from . import constants
from math import ceil, floor
import ipdb
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

class OptimalWorkerSearch:

    def __init__(self, race):
        self.race = race

        self.worker = ActionTypes.getByType(race.worker)
        self.depot = ActionTypes.getByType(race.depot)
        self.supply = ActionTypes.getByType(race.supplyUnit)
        self.initial_workers = 1
        self.initial_depots = 1
        self.initial_minerals = 0
        self.time = 0
        self.goal_minerals = 1000 * constants.RESOURCE_SCALE
        self.max_time = 5000000  # to avoid infinite loop

    def getSteadyStateTime(self):
        return ((self.goal_minerals) /
            (self.initial_workers * constants.MPWPF))

    def steadyStateSim(self):
        self._initializeState()
        frame = 1
        while self.state.current_frame < 50000:
            rate = self.state.res.getMineralsPerFrame()
            mins_left = self.goal_minerals - self.state.res.minerals
            predicted = self.state.current_frame + ceil(mins_left/rate)
            self.state.fastForward(predicted)
            frame = predicted
            if self.state.res.minerals >= self.goal_minerals:
                break
        return frame

    def _initializeState(self, workers=1, depots=1):
        self.state = State({
            "race": Races.Terran
            })
        self.state.res.minerals = self.initial_minerals
        for i in range(0, workers):
            self.state.addCompletedAction(Action(self.depot, complete=True))
        for i in range(0, depots):
            self.state.addCompletedAction(Action(self.worker, complete=True))

    def simulate(self, worker_train_max):
        """ get goal_minerals while training up to worker_train_max workers.
        Always train a worker if possible. """
        workers_trained = 0
        self._initializeState()
        cur_frame = self.state.current_frame
        while cur_frame < self.max_time:
            logger.debug("simulate frame: {}, real minerals: {}".format(
                cur_frame, floor(self.state.res.minerals /
                    constants.RESOURCE_SCALE)))
            if self.state.res.minerals >= self.goal_minerals:
                cur_frame = self.state.current_frame
                logger.debug("final mineral rate: {}".format(
                    self.state.res.getMineralsPerFrame()))
                break
            if workers_trained < worker_train_max:
                w_act = Action(self.worker, cur_frame)
                logger.debug("add worker action frame {}".format(cur_frame))
                self.state.doAction(w_act)
                cur_frame = self.state.current_frame
                logger.debug("start train on frame {}".format(cur_frame))
                workers_trained += 1
            # todo: fast forward to worker completion
            else:
                # check if there is a worker building
                if self.state.actions.numInProgress() > 0:
                    ff_time = self.state.actions.getNextActionFinishTime()
                else:  # predict when we will get to mineral goal
                    rate = self.state.res.getMineralsPerFrame()
                    mins_left = self.goal_minerals - self.state.res.minerals
                    predicted = cur_frame + ceil(mins_left/rate)
                    assert predicted > 0
                    logger.info("pred: {}, cur: {} work_time:{}".format(
                        predicted, cur_frame, predicted-cur_frame))
                    ff_time = predicted
                self.state.fastForward(ff_time)
                cur_frame = self.state.current_frame
        return cur_frame
