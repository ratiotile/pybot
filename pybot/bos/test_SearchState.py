from .SearchState import ActionState, ActionsInProgress, UnitState, State
from .ActionType import Action, ActionTypes, ActionType
from pybot.primitive.bwapitype import UnitTypes

# SearchState tests


def test_SearchStateGetActionsCompleted():
    s = State()
    a = Action(ActionTypes.Terran_Command_Center)
    a.complete = False
    a2 = Action(ActionTypes.Terran_SCV)
    a2.complete = True
    s.actions.addAction(a)
    s.actions.addCompletedAction(a2)
    c = s.getActionsCompleted(a2.type)
    assert c == 1
    c = s.getActionsCompleted(a.type)
    assert c == 0

def test_SearchState_addAction_initial():
    """ using addAction to set initial state """
    s = State()
    act = Action(ActionTypes.Terran_Command_Center, complete=True)
    s.addCompletedAction(act)
    #import ipdb; ipdb.set_trace()
    assert s.units.getBuilding(0).type is act.type
    assert s.res.getMaxSupply() == 10*2
    assert s.res.getCurSupply() == 0
    assert s.res.getMinerals() == 0
    assert s.res.getMineralsPerFrame() == 0
    scv = Action(ActionTypes.Terran_SCV, complete=True)
    s.addCompletedAction(scv)
    assert s.res.mineral_workers == 1
    assert s.res.getMineralsPerFrame() > 0
    assert s.res.getMaxSupply() == 10*2
    assert s.res.getCurSupply() == 1*2

def test_SearchState_fastForward_1frame():
    """ should be able to advance a single frame """
    s = State()
    act = Action(ActionTypes.Terran_Command_Center, complete=True)
    s.addCompletedAction(act)
    scv = Action(ActionTypes.Terran_SCV, complete=True)
    s.addCompletedAction(scv)
    s.fastForward(1)
    assert s.res.minerals == s.res.getMineralsPerFrame()

# ActionState tests


def test_ActionState_satisfies():
    """ test based on count of actions """
    goal = ActionState()
    a_state = ActionState()
    a1 = Action(ActionTypes.getByType(UnitTypes.Protoss_Probe).getID())
    a_state.addAction(a1)
    a2 = Action(ActionTypes.getByType(UnitTypes.Protoss_Probe).getID())
    goal.addAction(a2)
    assert a_state.satisfies(goal) == True
    # should not satisfy
    goal.addAction(Action(UnitTypes.Protoss_Pylon))
    assert a_state.satisfies(goal) == False
    # test if more than required still satisfies
    a_state.addAction(Action(UnitTypes.Protoss_Pylon))
    a_state.addAction(Action(UnitTypes.Protoss_Pylon))
    assert a_state.satisfies(goal) == True

# ActionsInProgress testing


def test_ActionState_constructFromDict():
    scv = ActionType(UnitTypes.Terran_SCV)
    rax = ActionType(UnitTypes.Terran_Barracks)
    port = ActionType(UnitTypes.Terran_Starport)
    prereqs = {
        scv: 1,
        rax: 2,
        port: 3,
    }
    a_state = ActionState(prereqs)
    assert len(a_state.actions[scv]) == 1
    assert len(a_state.actions[rax]) == 2
    assert len(a_state.actions[port]) == 3


def test_ActionsInProgress_addAction():
    act = Action(ActionTypes.Terran_SCV)
    aip = ActionsInProgress()
    aip.addAction(act)
    assert len(aip.in_progress) == 1
    assert len(aip.num_progress) == 1
    assert aip.numInProgress(ActionTypes.Terran_SCV) == 1


def test_ActionsInProgress_nextActionFinishTime():
    act = Action(ActionTypes.Terran_SCV)
    act.time = 123
    aip = ActionsInProgress()
    aip.addAction(act)
    assert aip.nextActionFinishTime() == 123
    assert aip.nextActionFinishTime(ActionTypes.Terran_SCV) == 123

# UnitState testing

def test_UnitState_canBuildNow():
    us = UnitState(type=ActionTypes.Terran_Barracks)
    assert us.canBuildNow(ActionTypes.Terran_Marine) is True
