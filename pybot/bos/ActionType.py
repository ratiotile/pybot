from pybot.primitive.bwapitype import (
    UnitTypes, UpgradeTypes, TechTypes, Races,
    UnitType, UpgradeType, TechType, Race)
from pybot.bos import constants
import itertools
from collections import defaultdict


class ActionType:

    def __init__(self, t, actionID=None):
        # handle creation from UnitType/Tech/Upgrade
        if(actionID is None and (
           isinstance(t, UnitType) or isinstance(t, TechType) or
           isinstance(t, UpgradeType))):
            actionID = ActionTypes.getByType(t).getID()
        if t is None:  # handle ActionTypes.none
            self.initNone(t, actionID)
            return
        self.initCommon(t, actionID)

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "ActionType<{}>".format(self.name)

    def __hash__(self):
        return self.id

    def initNone(self, t, id):
        self.type = None
        self.race = None
        self.id = id

    def initCommon(self, t, id):
        assert isinstance(id, int)
        assert(isinstance(t, UnitType) or isinstance(t, TechType) or
           isinstance(t, UpgradeType)), "t is {}".format(t)
        self.type = t
        self.race = t.race
        self.id = id
        self.mineral_price = t.mineralPrice * constants.RESOURCE_SCALE
        self.gas_price = t.gasPrice * constants.RESOURCE_SCALE
        self.supply_required = 0
        self.supply_provided = 0
        self.num_produced = 1
        self.name = t.name
        self.metaName = t.name
        self.is_worker = False
        self.is_refinery = False
        self.is_resource_depot = False
        self.is_supply_provider = False
        self.can_produce = False
        self.can_attack = False
        self.is_structure = False
        self.is_addon = False
        self.is_morphed = False
        self.requires_addon = False
        self.reqAddonID = 0
        if isinstance(t, UnitType):
            self.initUnitType(t)
        elif isinstance(t, UpgradeType):
            self.initUpgradeType(t)
        elif isinstance(t, TechType):
            self.initTechType(t)
        self.prerequisites = None
        self.recursivePrerequisites = None
        # set short name
        prefix = self.race.name
        self.shortName = self.name
        if prefix in self.name:
            self.shortName = self.name[len(prefix)+1:]

    def initUnitType(self, t):
        self.supply_required = t.supplyRequired
        self.supply_provided = t.supplyProvided
        self.make_time = t.buildTime
        self.num_produced = 2 if t.isTwoUnitsInOneEgg else 1
        self.is_worker = t.isWorker
        self.is_refinery = t.isRefinery
        self.is_resource_depot = t.isResourceDepot
        self.is_supply_provider = t.supplyProvided and not t.isResourceDepot
        self.can_produce = t.isBuilding and t.canProduce
        self.can_attack = t.canAttack
        #self.whatBuildsUnitType = t.whatBuilds[0]
        self.is_structure = t.isBuilding
        self.is_addon = t.isAddon
        if (t == UnitTypes.Zerg_Lair
                or t == UnitTypes.Zerg_Hive
                or t == UnitTypes.Zerg_Greater_Spire
                or t == UnitTypes.Zerg_Lurker
                or t == UnitTypes.Zerg_Guardian
                or t == UnitTypes.Zerg_Sunken_Colony
                or t == UnitTypes.Zerg_Spore_Colony ):
            self.is_morphed = True

    def initUpgradeType(self, t):
        self.make_time = t.upgradeTime
        #self.whatBuildsUnitType = t.whatUpgrades

    def initTechType(self, t):
        self.make_time = t.researchTime
        #self.whatBuildsUnitType = t.whatResearches

    def mineralPriceScaled(self):
        return self.mineral_price * 100

    def gasPriceScaled(self):
        return self.gas_price * 100

    def getID(self):
        return self.id

    def isUnit(self):
        return isinstance(self.type, UnitType)

    def isTech(self):
        return isinstance(self.type, TechType)

    def isUpgrade(self):
        return isinstance(self.type, UpgradeType)

    def __eq__(self, other):
        return self.id == other.id

    def __lt__(self, other):
        return self.id < other.id

    def setWhatBuildsActionID(self, a, wbib, wbil):
        self.whatBuildsActionID = a
        self.whatBuildsIsBuildingBool = wbib
        self.whatBuildsIsLarvaBool = wbil

    def setRequiredAddon(self, requiresAddon, id):
        self.requires_addon = requiresAddon
        self.reqAddonID = id

    def _dispatcher(self, f_unit, f_tech, f_upg):
        if isinstance(self.type, UnitType):
            return getattr(self.type, f_unit)
        elif isinstance(self.type, TechType):
            return getattr(self.type, f_tech)
        elif isinstance(self.type, UpgradeType):
            return getattr(self.type, f_upg)

    def whatBuilds(self):
        """ return the UnitType that makes this type """
        return self._dispatcher("whatBuildsUnit",
                                "whatResearches",
                                "whatUpgrades")

    def getPrerequisites(self, recursive=False):
        """ return list of (type, num) direct prereqs to build """
        return calculatePrerequisites(self, recursive)

    def canBuild(self, actype):
        assert isinstance(actype, ActionType)
        """ given an ActionType, return if this type can make it """
        if actype.race is not self.race:
            return False
        what_builds = actype.whatBuilds()
        if what_builds is self.type:
            return True
        return False

    @classmethod
    def Init(cls):
        cls.AddActions()
        cls.CalculateWhatBuilds()
        cls.AddPrerequisites()

    @classmethod
    def AddActions(cls):

        # add in order of ascending cost
        unitTypes = UnitTypes.getAll()
        unitTypes.sort(lambda x, y:
            x.mineralPrice()+2*x.gasPrice() < y.mineralPrice+2*y.gasPrice())
        for type in unitTypes:
            if (isinstance(type, UnitTypes.Zerg_Egg) or
            isinstance(type, UnitTypes.Zerg_Lurker_Egg) or
            isinstance(type, UnitTypes.Zerg_Cocoon) or
            isinstance(type, UnitTypes.Zerg_Infested_Terran) or
            isinstance(type, UnitTypes.Zerg_Infested_Command_Center) or
            isinstance(type, UnitTypes.Zerg_Broodling) or
            isinstance(type, UnitTypes.Protoss_Interceptor) or
            isinstance(type, UnitTypes.Protoss_Scarab) or
            isinstance(type, UnitTypes.Terran_Civilian) or
            isinstance(type, UnitTypes.Terran_Nuclear_Missile) or
            isinstance(type, UnitTypes.Terran_Vulture_Spider_Mine)):
                continue
            cls.AddAction(type)
        # add techs
        for type in TechTypes.getAll():
            if type.whatResearches is TechTypes.none:
                continue
            cls.AddAction(type)
        for type in UpgradeTypes.getAll():
            if type.whatUpgrades is UpgradeTypes.none:
                continue
            cls.AddAction(type)

    @classmethod
    def AddAction(cls, type):
        protoss = "Protoss"
        terran = "Terran"
        zerg = "Zerg"
        if protoss in type.getName():
            cls.allActionTypeDataVec[protoss].append(
                ActionTypeData(type, len(cls.allActionTypeDataVec[protoss])))
        elif terran in type.getName():
            cls.allActionTypeDataVec[terran].append(
                ActionTypeData(type, len(cls.allActionTypeDataVec[terran])))
        elif zerg in type.getName():
            cls.allActionTypeDataVec[zerg].append(
                ActionTypeData(type, len(cls.allActionTypeDataVec[zerg])))


# mapping of actionID to ActionType
class ActionTypes:
    none = ActionType(None, 9999)
    all = {
        "9999": none
    }
    _byType = {}

    @classmethod
    def getByType(cls, typeObj):
        return cls._byType.get(typeObj, ActionTypes.none)


def createActionTypes():
    """ create from UnitTypes, UpgradeTypes, TechTypes a set of ActionTypes
    that have a unique id """
    id = 0
    all_unit_t = UnitTypes.getAll()
    all_upg_t = UpgradeTypes.getAll()
    all_tech_t = TechTypes.getAll()
    for t in itertools.chain(all_unit_t, all_upg_t, all_tech_t):
        action = ActionType(t, id)
        ActionTypes.all[id] = action
        id += 1
        ActionTypes._byType[t] = action
        setattr(ActionTypes, action.type.name, action)
    # create ActionType.none
    #ActionTypes.all[id] = ActionType(Races.none, id)
    #ActionTypes.none = ActionTypes.all[id]


class Action:

    def __init__(self, source, current_frame=0, complete=False):
        self.complete = complete
        # frame when completed, add to current time make time later
        self.time = current_frame
        self.upgradeLevel = 1

        if isinstance(source, int):
            self.id = source
            self.type = ActionTypes.all[self.id]
        elif isinstance(source, ActionType):
            self.id = source.getID()
            self.type = source
        elif(isinstance(source, UnitType) or
             isinstance(source, TechType) or
             isinstance(source, UpgradeType)):
            self.type = ActionTypes.getByType(source)
            self.id = self.type.getID()
            self.time += self.type.make_time
            if self.type.is_structure and not self.type.is_morphed:
                self.time += constants.BUILDING_PLACEMENT
        else:
            raise TypeError()

    def __repr__(self):
        return "<Action:{}>".format(self.type.name)

    # sorted in time order for ActionsInProgress
    def __lt__(self, other):
        return self.time < other.time

    def isNamed(self, name):
        return self.type.name == name


def calculatePrerequisites(action, recursive=False):
    """ takes either single action/type or a list of them.
    Returns a dictionary of ActionType to int """
    actiontypes = []
    prereqs = defaultdict(int)

    def handle_single(action):
        if isinstance(action, Action):
            actiontypes.append(action.type)
            if action.type.isUpgrade():
                level = action.upgradeLevel
                req = action.type.type.whatsRequired(level)
                if req is not UnitTypes.none:
                    at = ActionTypes.getByType(req)
                    actiontypes.append(at)
                    prereqs[at] = 1
        elif isinstance(action, ActionType):
            actiontypes.append(action)

    if isinstance(action, list):
        for an_action in action:
            handle_single(an_action)
    else:
        handle_single(action)

    def getPrereqs(actype):
        assert isinstance(actype, ActionType)
        reqs = defaultdict(int)
        if actype.isUnit():
            utype = actype.type
            unit_reqs = defaultdict(int, utype.requiredUnits)
            for ut, num in unit_reqs.items():
                reqs[ActionTypes.getByType(ut)] = num
            # protoss buildings that need power
            if(utype.race == Races.Protoss and
            utype.isBuilding and not utype.isResourceDepot and
            utype is not UnitTypes.Protoss_Pylon and
            utype is not UnitTypes.Protoss_Assimilator):
                reqs[ActionTypes.getByType(UnitTypes.Protoss_Pylon)] += 1
            if utype.requiredTech is not TechTypes.none:
                req_act = ActionTypes.getByType(utype.requiredTech)
                reqs[req_act] = 1
        else:
            build = actype.whatBuilds()
            if build is not UnitTypes.none:
                reqs[ActionTypes.getByType(build)] = 1
        return reqs

    def mergePreqs(to_merge, reqs=defaultdict(int)):
        newreq = defaultdict(int, reqs)
        for rr in to_merge:
            for key, value in rr.items():  # merge dicts
                newreq[key] = max(newreq[key], value)
        return newreq

    def getPrereqsMulti(actiontypes):
        to_merge = []
        for action in actiontypes:
            to_merge.append(getPrereqs(action))
        return mergePreqs(to_merge)

    def recursivePrereqs(actiontypes, checked=defaultdict(int)):
        reqs = None
        if not isinstance(actiontypes, list):
            reqs = getPrereqs(actiontypes)
        else:
            reqs = getPrereqsMulti(actiontypes)
        assert reqs is not None
        to_merge = []
        for req, num in reqs.items():
            prev = checked[req]
            checked[req] = max(checked[req], num)
            if prev > 0:
                continue
            to_merge.append(recursivePrereqs(req, checked))
        reqs = mergePreqs(to_merge, reqs)
        return reqs

    if not recursive:
        return mergePreqs([getPrereqsMulti(actiontypes), prereqs])
    else:
        return mergePreqs([recursivePrereqs(actiontypes), prereqs])


createActionTypes()
