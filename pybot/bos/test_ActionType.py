from pybot.bos.ActionType import (Action, createActionTypes, ActionTypes,
    UpgradeTypes, calculatePrerequisites)
from pybot.primitive import bwapitype
from pybot.primitive.bwapitype import UnitTypes

bwapitype.init()
createActionTypes()

# Action tests

def test_Action_type():
    a = Action(0)
    assert a.id == 0
    assert a.type == ActionTypes.all[0]

def test_Action_whatBuilds():
    a = Action(0)
    assert a.type.whatBuilds() == UnitTypes.Terran_Barracks

def test_Action_isNamed():
    action = Action(
        ActionTypes.getByType(UpgradeTypes.Terran_Infantry_Armor).getID())
    assert action.isNamed("Terran_Infantry_Armor")


# ActionType tests

def test_ActionType_canBuild():
    actype = ActionTypes.Terran_Barracks
    assert actype.canBuild(ActionTypes.Terran_Marine)
    assert actype.canBuild(ActionTypes.Terran_Firebat)
    assert actype.canBuild(ActionTypes.Terran_Medic)
    assert actype.canBuild(ActionTypes.Terran_Ghost)
    assert not actype.canBuild(ActionTypes.Terran_SCV)
    assert not actype.canBuild(ActionTypes.Protoss_Zealot)

def test_ActionType_whatBuilds():
    assert ActionTypes.Protoss_Zealot.whatBuilds() is UnitTypes.Protoss_Gateway
    assert ActionTypes.Irradiate.whatBuilds() is UnitTypes.Terran_Science_Facility
    assert ActionTypes.Zerg_Carapace.whatBuilds() is UnitTypes.Zerg_Evolution_Chamber


# ActionTypes

def test_ActionTypes_getByType():
    u = UnitTypes.Terran_Barracks
    f = ActionTypes.getByType(u)
    assert u == f.type

def test_calculatePrerequisites():
    action = Action(
        ActionTypes.getByType(UpgradeTypes.Terran_Infantry_Armor).getID())
    action.upgradeLevel = 3
    prereqs = calculatePrerequisites(action, True)
    assert prereqs[ActionTypes.getByType(
        UnitTypes.Terran_Science_Facility)] == 1
    assert prereqs[ActionTypes.getByType(UnitTypes.Terran_Starport)] == 1
    assert prereqs[ActionTypes.getByType(UnitTypes.Terran_Factory)] == 1
    assert prereqs[ActionTypes.getByType(
        UnitTypes.Terran_Engineering_Bay)] == 1
    assert prereqs[ActionTypes.getByType(UnitTypes.Terran_Barracks)] == 1
    assert prereqs[ActionTypes.getByType(UnitTypes.Terran_Command_Center)] == 1
    assert prereqs[ActionTypes.getByType(UnitTypes.Terran_SCV)] == 1

