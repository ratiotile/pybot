import itertools, copy
from pybot.bos.ActionType import (ActionType, Action, calculatePrerequisites, ActionTypes)
from operator import attrgetter
from collections import defaultdict


class DFBB_BOS:

    def __init__(self, parameters=None):
        self.stack = []
        self.depth = 0
        self.param = parameters
        self.first_search = True
        self.was_interrupted = False
        self.results = {
            "solved": False,
            "timed_out": False,
            "solution_found": False,
            "upper_bound": 0,
            "nodes_expanded": 0,
            "time_elapsed": 0
        }

    def isTimeOut(self):
        return (self.params.search_time_limit and
                (self.results.nodes_expanded % 200 == 0) and
                (self.SearchTimer.getTime() > self.params.search_time_limit))  #_TODO

    def updateResults(self, state):
        finish_time = state.getLastActionFinishTime()  # TODO]
        if finish_time < self.results.upper_bound:
            self.results.time_elapsed = self.SearchTimer.getTime()
            self.results.upper_bound = finish_time
            self.results.solution_found = True
            self.results.final_state = state
            self.results.build_order = self.build_order
            self.results.printResults(True)

    def generateLegalActions(self, state, rLegalActions):
        rLegalActions.clear()
        # include recursive prereqs with goals
        goal_set = combineWithPrereqs(self.params.goal.actions)
        # add all the prerequisites in the goal
        for a in itertools.chain(goal_set, self.params.relevant_actions):  #_TODO
            if state.isLegal(a):
                rLegalActions.append(a)

    def sortActionsByLowerBound(self, actions):
        # assume actions all have .lower_bound set
        actions.sort(key=attrgetter('lower_bound'))

    def getRepetitions(self):
        pass # TODO

    def addToBuildOrder(self, action):
        pass # TODO

    @property
    def ACTION_TYPE(self):
        return self.stack[self.depth].current_action_type

    @ACTION_TYPE.setter
    def ACTION_TYPE(self, value):
        self.stack[self.depth].current_action_type = value

    @property
    def STATE(self):
        return self.self.stack[self.depth].state

    @STATE.setter
    def STATE(self, value):
        self.self.stack[self.depth].state = value

    @property
    def LEGAL_ACTIONS(self):
        return self.stack[self.depth].legalActions

    @LEGAL_ACTIONS.setter
    def LEGAL_ACTIONS(self, value):
        self.stack[self.depth].legalActions = value

    @property
    def CHILD_STATE(self):
        return self.stack[self.depth+1].state

    @CHILD_STATE.setter
    def CHILD_STATE(self, value):
        self.stack[self.depth+1].state = value

    @property
    def REPETITIONS(self):
        return self.stack[self.depth].repetition_value

    @REPETITIONS.setter
    def REPETITIONS(self, value):
        self.stack[self.depth].repetition_value = value

    @property
    def COMPLETED_REPS(self):
        return self.stack[self.depth].completed_repetitions

    @COMPLETED_REPS.setter
    def COMPLETED_REPS(self, value):
        self.stack[self.depth].completed_repetitions = value

    def cleanupReps(self):
        for r in range(0, self.COMPLETED_REPS):
            self.build_order.popBack()

    def DFBB(self):
        while True:
            if self.depth < 0:  # end the search
                return
            self.results.nodes_expanded += 1
            if self.isTimeOut():
                raise Exception("DFBB_TIMEOUT_EXCEPTION")
            # load state info
            state = self.STATE
            # trivial solution
            if self.params.goal.isAchievedBy(state):
                self.updateResults(state)
                # backtrack
                self.depth -= 1
                continue
            self.generateLegalActions(state, self.LEGAL_ACTIONS)
            actions = self.LEGAL_ACTIONS
            # evaluate each child state
            for action in actions:
                # landmark time bound
                action_finish_time = (state.whenCanPerform(action) +
                                      action.buildTime() )
                # resource bound
                heuristic_time = (state.current_frame +
                                self.getLowerBound(state, self.params.goal) )
                lower_bound = max(action_finish_time, heuristic_time)
                action.lower_bound = lower_bound
            # sort on lower_bounds
            actions = self.sortActionsByLowerBound(self.LEGAL_ACTIONS)
            # increment and load the last child index checked
            state.last_child += 1
            ci = state.last_child
            if (ci >= len(actions) or
              actions[ci].lower_bound >= self.results.upper_bound):
                # we can prune the rest of these branches and backtrack
                self.depth -= 1
            else:  # follow search deeper
                action = actions[ci]
                self.CHILD_STATE = state  # TODO: check initialized
                # try repeat actions
                self.REPETITIONS = self.getRepetitions(state, action)
                self.COMPLETED_REPS = 0
                while self.COMPLETED_REPS < self.REPETITIONS:
                    self.COMPLETED_REPS += 1
                    if self.CHILD_STATE.isLegal(action):
                        # self.build_order.add(action)
                        self.addToBuildOrder(action)
                        self.CHILD_STATE.doAction(action)
                    else:
                        break
                self.cleanupReps()
                self.depth += 1
                continue  # recurse and check for goal

        # end while True
    # end func DFBB


class BuildOrderSearchGoal:

    def __init__(self, race):
        # {ActionType: int} number of units in goal
        self.actions = defaultdict(int)
        # {ActionType: int} max number of units in goal
        self.max = defaultdict(int)
        self.race = race

    def setGoal(self, actiontype, num):
        self.actions[actiontype] = num

    def setGoalMax(self, actype, num):
        self.max[actype] = num

    def isAchievedBy(self, state):
        for actiontype in self.actions:
            if (state.getActionsCompleted(actiontype) <
              self.actions[actiontype]):
                return False
        return True

def combineWithPrereqs(goal):
    """ get recursive prereqs for all actions in goal and return union """
    merged = defaultdict(int)
    for actype, num_goal in goal.actions.items():
        prereqs = calculatePrerequisites(actype, True)
        for a2 in prereqs:
            if isPrereqConsumed(a2):  # make multiples of consumed prereqs
                prereqs[a2] *= num_goal
        merged = mergePrerequisites(merged, prereqs)
    merged = merged(goal.actions, merged)
    return merged

def mergePrerequisites(a, b):
    """ from two sets a and b, return combined requirement set taking into
    account that tech buildings and research need only 1 copy, while those
    units consumed to make another would add together in number. Expects
    a and b in format of {ActionType: num} """
    merged = defaultdict(int)
    for actype, num in itertools.chain(a.items(), b.items()):
        if isPrereqConsumed(actype):
            merged[actype] += num
        else:
            merged[actype] = max(merged[actype] + num)
    return merged

def isPrereqConsumed(p):
    return (True if actype is
            ActionTypes.Zerg_Drone or
            ActionTypes.Protoss_High_Templar or
            ActionTypes.Protoss_Dark_Templar or
            ActionTypes.Zerg_Hydralisk or
            ActionTypes.Zerg_Mutalisk else
            False)

def landmarkLowerBound(state, action, goal):
    """ get time necessary to perform each action while waiting on
    prereqs, assuming infinite resources """
    # first remove items already acheived by state.
    wanted = combineWithPrereqs(goal)
    for a in state.actions:
        wanted[a] = max(wanted[a] - state.actions[a], 0)
    lower_bound = calcPrereqLowerBound(state,
        wanted, state.time_elapsed, 0)
    return lower_bound

def resourceLowerBound(state, action, goal):
    """ get time necessary to gather the required resources of goal combined
    with prerequisites """
    gas = 0
    minerals = 0

def calcPrereqLowerBound(state, needed, time_so_far, depth):
    max = 0
    for n in needed:
        this_action_time = 0
        # if we have the needed type skil
        if state.getActionsCompleted(n) > 0:
            this_action_time = time_so_far
        elif state.getActionsInProgress(n) > 0:
            this_action_time = (time_so_far + state.getFinishTime(n) -
                state.getCurrentFrame())
        else:
            this_action_time = calcPrereqLowerBound(state,
                n.getPrerequisites(), time_so_far + n.buildTime(),
                depth + 1) #_TODO: check if max is lost
        if this_action_time > max:
            max = this_action_time
    return max
