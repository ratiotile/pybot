from collections import defaultdict
from .ActionType import Action, calculatePrerequisites, ActionType, ActionTypes
from . import constants
from ..primitive.bwapitype import Races
import sys
import math
import bisect
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

class State:

    """ Search state """

    def __init__(self, config=None):
        self.current_frame = 0
        self.last_action_frame = 0
        self.last_child = 0  # index of last child iterated over
        self.units = UnitStateHandler()
        self.res = ResourceState()
        self.actions = ActionState()
        if config is not None:
            self.race = config["race"]

    def addAction(self, action):
        logger.debug("addAction:{}, time:{}, complete:{}".format(
            action, action.time, action.complete))
        if action.complete is False:
            action.time = self.current_frame + action.type.make_time
            logger.debug("action will finish on: {}".format(
                action.time))
            self.actions.addAction(action)
            if action.type.whatBuilds().isBuilding:
                self.units.queueAction(action)
        else:
            self.addCompletedAction(action)


    def addCompletedAction(self, action):  # TODO: trace addAction
        assert action.complete is True
        action.time = self.current_frame
        self.actions.addCompletedAction(action)
        self.res.completedAction(action)
        if action.type.is_structure:
            self.units.addBuilding(action)

    def doAction(self, action):  #_TODO: add the make time + wait time for action
        """ assumes that action is legal """
        assert action.type.race == self.race
        assert self.isLegal(action)

        self.action_performed = action
        self.action_performed_k = 1
        ff_time = self.whenCanPerform(action)
        assert ff_time >= 0 and ff_time < 1000000

        action.time += ff_time
        logger.debug("doAction: {}, time: {}".format(action, action.time))
        self.fastForward(ff_time)

        # elapsed = self.current_frame - self.last_action_frame
        self.last_action_frame = self.current_frame
        # if not self.canAffordMinerals(action):
        #   import ipdb;ipdb.set_trace()
        assert self.canAffordMinerals(action)
        assert self.canAffordGas(action)

        # perform action
        logger.debug("doAction, about to pay, time: {}, min:{}".format(
            action.time, self.res.minerals))
        self.res.minerals -= action.type.mineral_price
        self.res.gas -= action.type.gas_price
        action.time -= ff_time

        if self.race == Races.Protoss:
            self.addAction(action)  # TODO account for maketime in action
        elif self.race == Races.Terran:
            if action.type.is_structure and not action.type.is_addon:
                if self.res.mineral_workers == 0:
                    print(self.toString())
                assert self.res.mineral_workers > 0
                self.res.setBuildingWorker()
            self.addAction(action)  # TODO account for maketime in action
        elif self.race == Races.Zerg:
            if action.type.isUnit() and not action.type.is_structure:
                if action.type.isUnit() and not action.type.is_structure:
                    if action.type.is_morphed:
                        self.actions.morphUnit(action.type.whatBuilds(),  # TODO: move into addaction?
                            action, self.current_frame + action.type.make_time)
                    else:
                        assert self.units.hatcheries.numLarva() > 0
                        self.units.hatcheries.useLarva()
                        self.addAction(action)
                elif action.type.is_structure:
                    self.actions.morphUnit(action.type.whatBuilds(),
                        action, self.current_frame + action.type.make_time)
                else:
                    self.addAction(action)

    def fastForward(self, to_frame):
        """ advance state to to_frame """
        self.units.fastForwardBuildings(self.current_frame, to_frame)
        from_frame = self.current_frame
        last_action_finished = self.current_frame
        total_time = 0
        more_gas = 0
        more_minerals = 0
        time_elapsed = to_frame - self.current_frame

        while(self.actions.numInProgress() > 0 and
              self.actions.getNextActionFinishTime() <= to_frame):
            next_action_finish_time = self.actions.getNextActionFinishTime()
            time_elapsed = (next_action_finish_time -
                            last_action_finished)
            total_time += time_elapsed
            more_minerals += time_elapsed * self.res.getMineralsPerFrame()
            more_gas += time_elapsed * self.res.getGasPerFrame()
            last_action_finished = next_action_finish_time
            self.finishNextAction()
            logger.debug("ff loop, elapsed:{}, gained:{}".format(
                time_elapsed, more_minerals))
        # update from last item finished to to_frame
        time_elapsed = to_frame - last_action_finished
        total_time += time_elapsed
        more_minerals += time_elapsed * self.res.getMineralsPerFrame()
        more_gas += time_elapsed * self.res.getGasPerFrame()
        self.res.minerals += more_minerals
        self.res.gas += more_gas
        logger.debug("ff end, from:{}, to:{}, total_time:{}, gained:{}".format(
            from_frame, to_frame, total_time, more_minerals))
        self.current_frame = to_frame

    def finishNextAction(self):
        action = self.actions.getNextAction()
        logger.debug("finishNextAction: {}".format(action))
        self.actions.finishNextActionInProgress()
        # do resources, workers etc.
        self.res.completedAction(action)
        # add buildings to handler
        if action.type.is_structure:
            if not action.type.is_morphed and not action.type.isSupplyProvider():
                self.units.addBuilding(action, ActionTypes.none)
            if action.type is ActionTypes.Zerg_Hatchery:
                self.units.hatcheries.addHatchery(1)

    def getActionsCompleted(self, action_type):
        """ Takes: UnitType, Return: int how many of type are complete """
        assert isinstance(action_type, ActionType)
        return self.actions.numCompleted(action_type)

    def getNumTotal(self, action_type):
        """ Returns how many total actions of type we have """
        assert isinstance(action_type, ActionType)
        return self.actions.numTotal(action_type)

    def getSupplyInProgress(self):
        return self.actions.getSupplyInProgress()

    def hasPrereqs(self, prereq_action_state):
        return self.actions.satisfies(prereq_action_state)

    def hasGasIncome(self):
        return (self.res.gas_workers > 0 or
            self.actions.numTotal(ActionType(self.race.refinery)) > 0)

    def hasMineralIncome(self):
        return (self.res.mineral_workers > 0 or
            self.res.building_workers > 0 or
            self.actions.numInProgress(ActionType(self.race.worker)) > 0)

    def whenCanPerform(self, action):
        prereq_t = self.whenPrereqsReady(action)
        mineral_t = self.whenMineralsReady(action.type)
        gas_t = self.whenGasReady(action)
        race_t = self.raceSpecificWhenReady(action)  # larva timing
        # when will we have enough supply for the unit
        supply_t = self.whenSupplyReady(action)
        # when will a worker be ready to build it?
        worker_t = self.whenWorkerReady(action)
        logger.debug("whenCanPerform pr:{}, mt:{}, gt:{}, rt:{}, st:{}, wt:{}".format(
               prereq_t, mineral_t, gas_t, race_t, supply_t, worker_t))
        max_t = max(prereq_t,
                    mineral_t,
                    gas_t,
                    race_t,
                    supply_t,
                    worker_t,
                    )
        return max_t

    def isLegal(self, action):
        assert isinstance(action, Action)
        num_refineries = self.actions.numTotal(
            ActionType(self.race.refinery))
        num_depots = self.actions.numTotal(
            ActionType(self.race.depot))
        # refineries_in_progress = self.actions.numInProgress(
        #    ActionType(self.race.refinery))

        if (action.isNamed("Zerg_Larva") or
           (not self.hasPrereqs(calculatePrerequisites(action))) or
           # if its a unit and we are out of supply and its not an overlord
           (not action.type.is_morphed and
                (self.getCurrentSupply() + action.type.supply_required >
                 self.getMaxSupply() + self.getSupplyInProgress())) or
           # can't get gas
           (not self.canAffordGas(action) and not self.hasGasIncome()) or
           # can't get minerals
           (not self.canAffordMinerals(action) and
            not self.hasMineralIncome()) or
           # don't build more refineries than depots
           (action.type.is_refinery and (num_refineries >= num_depots)) or
           # don't go over max supply limit
           (action.type.is_supply_provider and
            self.getActionsCompleted(action) > 0) or
           # only build one of a tech type
           (action.type.isTech() and self.getNumTotal(action.type) > 0) or
           # check to see if an addon can ever be built
           (action.type.is_addon and not
                self.actions.buildings.canBuildEventually(action) and
           self.actions.numInProgress(action.type.whatBuilds()) == 0)):
                return False

        if (action.type.is_structure and not action.type.is_morphed and
          not action.type.is_addon):
            # make sure we have enough workers to go in refinery
            refineriesInProgress = self.actions.numInProgress(
                ActionType.getRefinery(self.race))
            if(action.type.isRefinery() and
               self.res.mineral_workers <=
               (4 + 3 * refineriesInProgress)):
                    return False
            workersPerRefinery = 3
            workersRequiredToBuild = (
                0 if action.type.race == Races.Protoss else 1)
            #buildingIsRefinery = 1 if action.type.isRefinery() else 0
            candidateWorkers = (self.res.mineral_workers +
                self.actions.numInProgress(
                    ActionType.getWorker(self.race)
                ) + self.res.building_workers )
            workersToBeUsed = (workersRequiredToBuild +
                workersPerRefinery * refineriesInProgress)
            if candidateWorkers < workersToBeUsed:
                return False
        return True

    def getCurrentSupply(self):
        return self.res.getCurSupply()

    def getMaxSupply(self):
        return self.res.getMaxSupply()

    def canAffordGas(self, action):
        return self.res.gas >= action.type.gas_price

    def canAffordMinerals(self, action):
        return self.res.minerals >= action.type.mineral_price

    def raceSpecificWhenReady(self, action):
        return self.current_frame  # TODO

    def whenWorkerReady(self, action):
        if not action.type.whatBuilds().isWorker:
            return self.current_frame
        refineries_in_progress = self.actions.numInProgress(
            ActionType(self.race.refinery) )
        if self.race is Races.Protoss and self.mineral_workers > 0:
            return self.current_frame

    def whenPrereqsReady(self, action):
        ready_time = self.current_frame
        if action.type.whatBuilds().isBuilding:
            ready_time = self.whenBuildingPrereqReady(action)
        else:
            reqInProgress = self.actions.getPrereqsInProgress(action)
            if not len(reqInProgress) == 0:
                ready_time = self.actions.getFinishTime(reqInProgress)
        return ready_time

    def whenBuildingPrereqReady(self, action):
        building_available_time = 0
        builder = ActionTypes.getByType(action.type.whatBuilds())
        assert builder.is_structure   # thing that builds must be a building

        building_is_constructed = (self.units.canBuildEventually(action.type))
        building_in_progress = self.actions.numInProgress(builder) > 0
        constructed_building_free_time = sys.maxsize - 10
        building_in_progress_finish_time = sys.maxsize - 10
        assert building_is_constructed or not action.type.requires_addon
        if building_is_constructed:
            constructed_building_free_time = (self.current_frame +
                self.units.getTimeUntilCanBuild(action))
        if not action.type.requires_addon and building_in_progress:
            building_in_progress_finish_time = self.actions.getFinishTime(
                builder)
        building_available_time = min(constructed_building_free_time,
            building_in_progress_finish_time)
        # get all prereqs currently in progress but do not have any completed
        prereq_in_progress = self.actions.getPrereqsInProgress(action)
        # remove the specific builder from this list we calculated that earlier

        if prereq_in_progress.get(builder, None) is not None:
            del prereq_in_progress[builder]
        if len(prereq_in_progress) > 0:
            c = self.actions.get_finish_time(prereq_in_progress)
            building_available_time = (c if c > building_available_time
                else building_available_time)
        return building_available_time

    def whenGasReady(self, actype):
        # TODO
        return self.current_frame

    def whenMineralsReady(self, action_t):
        assert isinstance(action_t, ActionType)
        if self.res.minerals >= action_t.mineral_price:  # note scaled price
            return self.current_frame
        logger.debug("whenMineralsReady, min:{}, frame:{}".format(
            self.res.minerals, self.current_frame))
        current_mineral_workers = self.res.mineral_workers
        current_gas_workers = self.res.gas_workers
        minerals_per_frame = (current_mineral_workers * constants.MPWPF)
        last_action_finish_frame = self.current_frame
        added_time = 0
        added_minerals = 0
        difference = action_t.mineral_price - self.res.minerals
        # loop through all actions in progress and add integrate mineral income
        actions_in_progress = self.actions.numInProgress()
        logger.debug("before loop, mw:{}, gw:{}, dif:{}, acts:{}".format(
            current_mineral_workers, current_gas_workers, difference,
            actions_in_progress))
        for i in range(0, actions_in_progress):
            progress_index = actions_in_progress - i - 1
            # time elapse and current minerals per frame
            elapsed = self.actions.getFinishTimeByIndex(
                progress_index) - last_action_finish_frame

            # amount of mienrals added this time step
            temp_add = elapsed * minerals_per_frame
            # if this isn't enough, update amount added for this interval
            logger.debug("in loop, mpf:{}, elapsed:{}, t_add:{}".format(
                minerals_per_frame, elapsed, temp_add))
            if added_minerals + temp_add < difference:
                added_minerals += temp_add
                added_time += elapsed
            else:  # break out and update at end
                break
            # if it was a drone or extractor update the temp variables.
            action_performed = self.actions.getActionInProgressByIndex(
                progress_index)
            # finishing a building as terran gives mineral worker back
            if((action_performed.type.is_structure and
               not action_performed.type.is_addon and
               self.race == Races.Terran) or
               action_performed.type.is_worker):
                current_mineral_workers += 1
                logger.debug("added mineral worker")

            elif action_performed.type.is_refinery:
                assert current_mineral_workers > 3
                current_mineral_workers -= 3
                current_gas_workers += 3
            # update the last action
            last_action_finish_frame = self.actions.getFinishTimeByIndex(
                progress_index)
        # if we still haven't added enogh minerals, add more time
        logger.debug("after loop, mpf:{}, added_minerals:{}".format(
                minerals_per_frame, added_minerals))
        if added_minerals < difference:
            assert current_mineral_workers > 0
            final_time_to_add = ((difference - added_minerals) /
                (current_mineral_workers * constants.MPWPF))
            added_minerals += (final_time_to_add *
                current_mineral_workers * constants.MPWPF)
            added_time += final_time_to_add
            if added_minerals < difference:
                added_time == 1
                added_minerals += current_mineral_workers * constants.MPWPF
        assert added_minerals >= difference  # mineral prediction error
        logger.debug("return, added_time: {}".format(added_time))
        return self.current_frame + math.ceil(added_time)

    def whenSupplyReady(self, action):
        # TODO
        return self.current_frame


class ActionsInProgress:

    """ handles actions that are not finished yet """

    def __init__(self):
        # a queue of actions in progress, sort by time remaining.
        # The next soonest finishing ones will be in front
        self.in_progress = []
        # actiontype to int number in progress
        self.num_progress = defaultdict(int)

    def addAction(self, action):  # time not needed, represented in action
        bisect.insort_left(self.in_progress, action)

        self.num_progress[action.type] += 1

    def getAction(self, index):
        return self.in_progress[index]

    def getTime(self, index):
        return self.in_progress[index].time

    def nextActionFinishTime(self, actype=ActionTypes.none):
        if actype is ActionTypes.none:  # todo:_handle out of range
            if len(self.in_progress) == 0:
                import ipdb; ipdb.set_trace()
            return self.in_progress[0].time
        else:
            assert self.numInProgress(actype) > 0
            for action in self.in_progress:
                if action.type == actype:
                    return action.time
        assert False and "should have found something"
        return -1

    def nextAction(self):
        """ returns the next action to finish """
        assert len(self.in_progress) > 0, "Tried to nextAction() on empty list"
        return self.in_progress[-1]

    def numInProgress(self, actype=None):
        if actype is None:
            total = 0
            for num in self.num_progress.values():
                total += num
        else:
            total = self.num_progress[actype]

        #logger.debug("numInProgress: type:{}, num:{}".format(actype, total))
        return total

    def numInProgressByIndex(self, index):
        actype = self.in_progress[index].type
        assert isinstance(actype, ActionType)
        return self.num_progress[actype]

    def popNextAction(self):
        popped = self.in_progress.pop()
        self.num_progress[popped.type] -= 1
        assert self.num_progress[popped.type] >= 0

    def whenActionsFinished(self, actions):
        assert len(actions) > 0, "Action set is empty!"
        total_max = 0
        for actype, num in actions.items():
            action_min = sys.maxsize
            for aip in self.in_progress:
                if aip.type == actype and aip.time < action_min:
                    action_min = aip.time
            if action_min < sys.maxsize and action_min > total_max:
                total_max = action_min
        return total_max


class ActionState:

    """ actions in progress or completed """

    def __init__(self, source=None):
        # actions that are completed
        self.actions = defaultdict(list)  # {ActionType: [Actions]}
        self.progress = ActionsInProgress()
        if isinstance(source, dict):
            self._constructFromDict(source)
        self.race = Races.none

    def _constructFromDict(self, input):
        """ assume that input is a dict """
        assert isinstance(input, dict)
        for a_type, num in input.items():
            for i in range(0, num):
                self.addAction(Action(a_type))

    def addAction(self, action):
        self.actions[action.type].append(action)
        self.progress.addAction(action)  # TODO represent time in action

    def addCompletedAction(self, action):
        assert isinstance(action, Action)
        assert action.complete is True
        self.actions[action.type].append(action)

    def completeAction(self, action):
        """ things that need to be taken care of when an action completes """
        self.progress.popNextAction()
        action.complete = True

    def getActionInProgressByIndex(self, index):
        return self.progress.getAction(index)

    def getFinishTime(self, prereq_set):
        if isinstance(prereq_set, ActionType):
            return self.progress.nextActionFinishTime(prereq_set)
        else:  # is actually a dictionary
            return self.progress.whenActionsFinished(prereq_set)

    def getFinishTimeByIndex(self, index):
        return self.progress.getTime(index)

    def getPrereqsInProgress(self, action):
        in_progress = defaultdict(int)  # ActionType to num
        for at in action.type.getPrerequisites():
            if self.numCompleted(at) == 0:
                in_progress[at] = self.numInProgress(at)
        return in_progress

    def numCompleted(self, actiontype):
        return len([a for a in self.actions[actiontype] if a.complete])

    def numTotal(self, actiontype):
        return len([a for a in self.actions[actiontype]])

    def numInProgress(self, actiontype=None):
        if actiontype is None:
            return self.progress.numInProgress()
        return self.progress.numInProgress(actiontype)

    def satisfies(self, other):
        """ expects another ActionState or dictionary.
        Returns True if self has at aleast
        as many actions of each type as other does """
        if isinstance(other, ActionState):
            for at, other_actions in other.actions.items():
                my_actions = self.actions[at]
                if len(my_actions) < len(other_actions):
                    return False
            return True
        else:  # assume that other is a dict of numbers
            for at, num in other.items():
                my_actions = self.actions[at]
                if len(my_actions) < num:
                    return False
            return True

    def getSupplyInProgress(self):
        provider = ActionType(self.race.supplyUnit)
        depot = ActionType(self.race.depot)
        return (self.numInProgress(provider) * provider.supply_provided +
            self.numInProgress(depot) * depot.supply_provided)

    def getNextActionFinishTime(self):
        time = self.progress.nextActionFinishTime()
        logger.debug("getNextActionFinishTime: {}".format(time))
        return time

    def getNextAction(self):
        """ return the next action that will finish """
        return self.progress.nextAction()

    def finishNextActionInProgress(self):
        action = self.progress.nextAction()
        logger.debug("finishNextActionInProgress: {} fin_t: {}".format(
            action, action.time))
        self.completeAction(action)
        if(self.race is Races.Terran and action.type.is_structure
           and not action.type.isAddon()):
            # release worker from building
            self.releaseBuildingWorker()



class UnitState:

    """ state of a single unit, mostly buildings and managed by
    UnitStateHandler """

    def __init__(self, type=ActionTypes.none,
                 addon_type=ActionTypes.none, time_remaining=0,
                 train_type=ActionTypes.none):
        self.type = type  # type of this structure
        self.time_remaining = 0  # time until unit is finished constructing
        self.constructing = ActionTypes.none  # type of unit building is making
        # the type of addon the building has once completed
        self.addon = addon_type
        self.num_larva = 1 if self.isHatchery() else 0
        self.current_frame = 0

    def canBuildNow(self, actype):
        """ expects ActionType actype """
        assert isinstance(actype, ActionType)
        if(self.time_remaining > 0 or
           not self.type.canBuild(actype) or
           (actype.is_addon and self.addon != ActionTypes.none) or
           (actype.requires_addon and
           self.addon != actype.requiredAddonType())):
            return False
        return True

    def canBuildEventually(self, actype):
        """ expects ActionType actype """
        assert isinstance(actype, ActionType)
        if not self.type.canBuild(actype):
            return False
        if actype.is_addon:
            if self.addon is not ActionTypes.none:
                return False
            if self.time_remaining > 0 and self.constructing.is_addon:
                return False
        if(actype.requires_addon and
           self.addon is not actype.requiredAddonType() and
           self.constructing is not actype.requiredAddonType()):
            return False
        # if built type is morphed and we are morphing something
        if(actype.is_morphed and self.time_remaining > 0 and
           self.constructing.is_morphed):
            return False
        return True

    def isHatchery(self):
        hatch = ActionTypes.Zerg_Hatchery
        lair = ActionTypes.Zerg_Lair
        hive = ActionTypes.Zerg_Hive
        return self.type == hatch or lair or hive

    def fastForward(self, frames):
        will_complete = self.time_remaining <= frames
        if self.time_remaining > 0 and will_complete:
            self.time_remaining = 0
            if self.constructing.is_addon:
                self.addon = self.constructing
            if self.constructing.is_morphed:
                self.type = self.constructing
            self.constructing = ActionTypes.none
        elif self.time_remaining > 0:
            self.time_remaining -= frames

    def queueActionType(self, actype):
        """ more like build action now """
        assert isinstance(actype, ActionType)
        self.time_remaining = actype.make_time
        self.constructing = actype


class HatcheryState:

    """ specifically handles larva spawning """

    def __init__(self):
        self.num_larva = 0

    def fastForward(self, current_frame, frames):
        if self.num_larva == 3:
            return
        larva_to_add = (
            (frames + current_frame) / constants.ZERG_LARVA_TIMER -
            current_frame / constants.ZERG_LARVA_TIMER)
        self.num_larva = min(larva_to_add + self.num_larva, 3)

    def useLarva(self):
        self.num_larva -= 1


class HatcheryHandler:

    """ handles all HatcheryStates """

    def __init__(self):
        self.hatcheries = []

    def __len__(self):
        return len(self.hatcheries)

    def useLarva(self):
        """ pick from hatch with most larva """
        most = None
        for h in self.hatcheries:
            if most is None or most.num_larva < h.num_larva:
                most = h
        if most is not None:
            most.useLarva()
        logger.warning("should have found larva to use")

    def fastForward(self, current_frame, to_frame):
        for h in self.hatcheries:
            h.fastForward(current_frame, to_frame)

    def numLarva(self):
        total = 0
        for h in self.hatcheries:
            total += h.num_larva
        return total


class UnitStateHandler:

    """ Handles state for structure units """

    def __init__(self):
        self.units = []  # list of UnitState
        self.hatcheries = HatcheryHandler()

    def addBuilding(self, action, time_until_free=0,
            constructing=ActionTypes.none, addon=ActionTypes.none):
        self.units.append(
            UnitState(action.type, time_until_free, constructing, addon))

    def canBuildEventually(self, actype):
        assert isinstance(actype, ActionType)
        for unit in self.units:
            if unit.canBuildEventually(actype):
                return True
        return False

    def getBuilding(self, index):
        return self.units[index]

    def getTimeUntilCanBuild(self, action):
        """ How long from now until we can build action.
        Must be able to build the action """
        can_build = False
        min_time = sys.maxsize
        for building in self.units:
            if(building.canBuildEventually(action.type) and
               building.time_remaining < min_time):
                can_build = True
                min_time = building.time_remaining
        assert can_build is True
        return min_time

    def queueAction(self, action):
        for building in self.units:
            if building.canBuildNow(action.type):
                building.queueActionType(action.type)
                return
        assert False and "should find a building to queue unit in"

    def fastForwardBuildings(self, now, frames):
        """ expects int current frame, frames to move time forward """
        for b in self.units:
            b.fastForward(frames)


class ResourceState:

    """ handles the minerals/gas for GameState """

    def __init__(self):
        self.minerals = 0
        self.gas = 0
        self.mineral_workers = 0
        self.gas_workers = 0
        self.building_workers = 0
        self.supply_used = 0
        self.max_supply = 0

    def getMinerals(self, frame=None):
        if frame is None:
            return self.minerals
        assert frame >= self.current_frame  # must be in future
        return self.minerals + self.getMineralsPerFrame() * (
            frame - self.current_frame)

    def getGas(self, frame=None):
        if frame is None:
            return self.gas
        assert frame >= self.current_frame  # must be in future
        return self.gas + self.getGasPerFrame() * (
            frame - self.current_frame)

    def getCurSupply(self):
        return self.supply_used

    def getMaxSupply(self):
        return self.max_supply

    def setMinerals(self, minerals):
        self.minerals = minerals * constants.RESOURCE_SCALE

    def setGas(self, gas):
        self.gas = gas * constants.RESOURCE_SCALE

    def getMineralsPerFrame(self):
        return constants.MPWPF * self.mineral_workers

    def getGasPerFrame(self):
        return constants.GPWPF * self.gas_workers

    def numBuildingWorkers(self):
        return self.building_workers

    def setSupply(self, new_supply):
        self.supply_used = new_supply

    def completedAction(self, action):
        # add supply if action is not building or not Lair or Hive
        # no additional supply for morphing lair or hive
        if((action.type is not ActionTypes.Zerg_Lair or
                               ActionTypes.Zerg_Hive)
        ):
            self.max_supply += action.type.supply_provided

        if not action.type.is_morphed:
            self.supply_used += (action.type.supply_required *
                                    action.type.num_produced)
        if action.type.is_worker:
            self.mineral_workers += 1
        if action.type.is_refinery:
            self.mineral_workers -= 3
            self.gas_workers += 3

    def setBuildingWorker(self):
        assert self.mineral_workers > 0
        self.mineral_workers -= 1
        self.building_workers += 1
