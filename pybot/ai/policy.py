from collections import defaultdict
import cybw
import logging
from cybw import UnitTypes
from cybw import Races
from pybot.interface.utility import isProduced, isFactory, whatProduces
from collections import deque

logger = logging.getLogger(__name__)


class BuildPolicy:
    def __init__(self, harvester, unitmanager, bank, builder, unitstate):
        self.harvester = harvester
        self.unitmanager = unitmanager
        self.bank = bank
        self.builder = builder
        self.unitstate = unitstate

    def update(self):
        # train worker if possible
        wtype = cybw.Broodwar.self().getRace().getWorker()
        wmax = self.harvester.workerCapacity()
        saturation = self.harvester.numWorkersToSaturate()
        wkrsinprod = self.builder.getPlanned(wtype)
        totalworkers = self.unitstate.count(wtype) + wkrsinprod
        haveenough = self.bank.haveResourcesFor(wtype)

        if (wmax > totalworkers and haveenough and
                self.builder.canMake(wtype) and
                wkrsinprod == 0):
            logger.info("build worker. saturation is {}".format(saturation))
            self.builder.push(wtype)

        # if supply is running out, and have enough money
        supply_total = cybw.Broodwar.self().supplyTotal()
        supply_type = cybw.Broodwar.self().getRace().getSupplyProvider()
        if(supply_total < 400 and
           self.bank.supplyAvailable() + self.builder.supply(True) < 4 and
           self.unitstate.havePrereqs(supply_type)):
            logger.info("Push supply into queue")
            self.builder.push(supply_type)

        # Build Barracks
        barracks = cybw.UnitTypes.Terran_Barracks
        min_income = self.bank.mineralIncome()
        min_expense = self.bank.mineralExpense()
        min_ex_rax = self.bank.mineralExpense(barracks)
        num_rax = self.unitstate.count(barracks, planned=True)
        single_rax_ex = min_ex_rax / max(1, num_rax)
        if(cybw.Broodwar.canMake(barracks) and
           self.builder.getPlanned(barracks) == 0 and
           num_rax < 8 and
           min_income > min_expense + single_rax_ex and
           supply_total > 20 and
           self.bank.haveResourcesFor(barracks)):
            logger.info("Build Barracks")
            self.builder.push(barracks)

        # Train Marines
        marine = cybw.UnitTypes.Terran_Marine
        cc = cybw.UnitTypes.Terran_Command_Center
        ccs_idle = self.builder.trainer.numIdle(cc)
        if(cybw.Broodwar.canMake(marine) and
           self.builder.getQueued(marine) == 0 and
           self.bank.haveResourcesFor(marine) and
           self.builder.trainer.canTrain(marine)):
            self.builder.push(marine)

        # Build Ebay
        ebay = cybw.UnitTypes.Terran_Engineering_Bay
        if(self.unitstate.count(ebay, planned=True) < 1 and
           self.builder.getPlanned(ebay) == 0 and
           cybw.Broodwar.canMake(ebay) and
           self.unitstate.count(marine) > 4):
            self.builder.push(ebay)

        # build Gas
        refinery = cybw.UnitTypes.Terran_Refinery
        if(self.unitstate.count(ebay) > 0 and
           self.builder.getPlanned(refinery) == 0 and
           self.unitstate.count(refinery, True) < 1 and
           cybw.Broodwar.canMake(refinery)):
            self.builder.push(refinery)


class UnitMix:
    """ responsible for selecting production targets for factory type so that
    the unit mix maintains the preset ratio.
    """
    def __init__(self, unitstate, race):
        # fact type to FactoryRatio
        self.unitratios = self._defaultUnitRatios()
        self.unitstate = unitstate
        self.factorytypes = self._initFactoryTypes(race)

    def getExtantUnits(self, factorytype):
        """ returns counts of all units that factory type can produce

        :param factorytype: a production unit with queue
        :type factorytype: UnitType
        :return: dictionary mapping UnitTypes to counts
        :rtype: {UnitType: int}
        """
        factoryproduces = self.factorytypes[factorytype]
        counts = {}
        for utype in factoryproduces:
            counts[utype] = self.unitstate.count(utype, True)
        return counts

    def next(self, factorytype):
        """ get next unit that this factory type should produce.

        :param factorytype: a production unit with queue
        :type factorytype: UnitType
        :return: production target to maintain ratio
        :rtype: UnitType
        """
        facunits = self.getExtantUnits(factorytype)
        return self.unitratios[factorytype].next(facunits)

    def setUnitRatios(self, unitratio_tuples):
        fac_unitprops = defaultdict(dict)
        for unit, prop in unitratio_tuples:
            builder = unit.whatBuilds()[0]
            if builder is not None and isFactory(builder):
                fac_unitprops[builder][unit] = prop
        for fact, proportions in fac_unitprops.items():
            self.unitratios[fact] = FactoryRatio(proportions)


    def _initFactoryTypes(self, race):
        factorytypes = defaultdict(set)
        for utype in cybw.UnitTypes.allUnitTypes():
            if utype.getRace() == race:
                builder = utype.whatBuilds()[0]
                if builder is not None and isFactory(builder):
                    factorytypes[builder].add(utype)
        return factorytypes

    def _defaultUnitRatios(self):
        return {
            UnitTypes.Terran_Command_Center:
                FactoryRatio({UnitTypes.Terran_SCV: 1}),
            UnitTypes.Terran_Barracks:
                FactoryRatio({UnitTypes.Terran_Marine: 1}),
            UnitTypes.Terran_Factory:
                FactoryRatio({UnitTypes.Terran_Vulture: 1}),
            UnitTypes.Terran_Starport:
                FactoryRatio({UnitTypes.Terran_Wraith: 1})
        }

class FactoryRatio:
    """ Represents the proportion of units a type of factory should produce

    Assumes that we can produce all units in self.proportion, which can not be
    set to 0 for any unit.
    """
    def __init__(self, proportion):
        self.proportion = proportion  # {unit, proportion}

    def next(self, extantunits):
        fact_unitcounts = {}
        smallest = None
        typeneeded = None
        for utype, prop in self.proportion.items():
            ratio = extantunits.get(utype, 0) / prop
            if smallest is None or ratio < smallest:
                smallest = ratio
                typeneeded = utype
        return typeneeded


class GreedyProduction:
    """ 1. Train goal units if possible
        2. Build supply if at risk of supply block
        3. Train unit mix to fill all factories
        4. Build Goal structures
    """
    def __init__(self, eventfinder, unitstate, builder, bank):
        self.unitmix = UnitMix(unitstate, Races.Terran)  # factory type to unit
        # queue
        self.supplymargin = 50  # frames extra look-ahead compensation
        self.supplywindow = 600  # frames to build supply depot
        # stuff that is made by buildings, we want to keep busy
        self.goalprod = defaultdict(deque)  # factory to list of items
        # stuff that has to be made by workers, keep them mining!
        self.goalbuildings = deque()
        # add from onFactoryIdle event
        self.idlefactories = set()
        self.ee_builditem = eventfinder.get("QueueBuildItem")
        self.unregFactIdle = eventfinder.get("FactoryIdle").register(
            self._onFactoryIdle
        )
        self.builder =builder
        self.bank = bank

    def getProduction(self, factype):
        # return the goal production first (greedy)
        prodlist = self.goalprod[factype]
        if len(prodlist) > 0:
            return prodlist.popLeft()
        else:
            return self.unitmix.next(factype)

    def update(self):
        # check for supply block risk first and put the supply into production
        self._checkSupplyBlock()
        # assign production to idle factories
        self._procIdleFactories()
        # finally build goal structures if possible.
        self._procGoalStructures()

    def setGoals(self, goals):
        # process goals into prod and buildings
        # assume all prerequisites are in order and included in goals
        self.goalprod = defaultdict(deque)
        self.goalbuildings = []
        for g in goals:
            if isProduced(g):
                factory = whatProduces(g)
                self.goalprod[factory].append(g)
            else:
                self.goalbuildings.append(g)

    def _checkSupplyBlock(self):
        # check if we need to queue a supply now to avoid supply block
        supplylimit = self.builder.supply(True)
        supplyused = self.builder.queuedResources()[2]
        supplydelta = self.bank.rate.supply_ex / 23.81  # per frame
        supplydelta *= (self.supplywindow + self.supplymargin)
        if supplyused + supplydelta > supplylimit:
            self._enqueueItem(UnitTypes.Terran_Supply_Depot)

    def _enqueueItem(self, utype):
        logger.info("greedy enqueue {}")
        self.ee_builditem.emit(type)

    def _onFactoryIdle(self, event):
        """ Register this with the FactoryIdle event.
        """
        self.idlefactories.add(event.unit)

    def _procGoalStructures(self):
        """ Put first goal struct into build queue if we can afford it
        """
        top = self.goalbuildings[0]
        if self.bank.haveResourcesFor(top):
            self._enqueueItem(top)
            self.goalbuildings.popleft()

    def _procIdleFactories(self):
        """ Give build items to all idle factories
        """
        for fact in self.idlefactories:
            # may need to check if we have the resources
            self._enqueueItem(self.getProduction(fact.getType()))
        self.idlefactories.clear()
