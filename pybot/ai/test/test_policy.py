from cybw import Races
import pytest
from pybot.ai.policy import FactoryRatio, UnitMix
from cybw import UnitTypes
from pybot.engine.builder import Builder
from pybot.engine.state import UnitState
from pybot.engine.test.test_builder import BP
from pybot.engine.test.test_harvester import harvester
from pybot.interface.unit import UnitManager
from pybot.state.resource import Bank
from pybot.interface.test.test_territory import mapInfo

marine = UnitTypes.Terran_Marine
firebat = UnitTypes.Terran_Firebat
barracks = UnitTypes.Terran_Barracks
scv = UnitTypes.Terran_SCV

class TestFactoryRatio:
    def test_allMarine(self):
        proportion = {marine: 1}
        fr = FactoryRatio(proportion)
        extantunits = {marine: 12,
                       firebat: 2}
        assert fr.next(extantunits) == marine

    def test_evenSplit(self):
        proportion = {marine: 1,
                      firebat: 1}
        fr = FactoryRatio(proportion)
        extantunits = {marine: 7,
                       firebat: 6}
        assert fr.next(extantunits) == firebat


@pytest.fixture
def unitmanager():
    return UnitManager()

@pytest.fixture
def builder(BP, unitmanager, harvester):
    tm = None
    mybuilder = Builder(BP, tm, Bank, unitmanager, harvester)
    return mybuilder

@pytest.fixture
def unitstate(unitmanager, builder):
    us = UnitState(unitmanager, builder)
    return us

@pytest.fixture
def unitmix_fix(unitstate):
    um = UnitMix(unitstate, Races.Terran)
    return um

def stripzeroes(d):
    for k in list(d.keys()):
        if d[k] == 0:
            del d[k]

class TestUnitMix:
    def test_getExtantUnits(self, unitstate, unitmix_fix):
        unitmix = unitmix_fix
        unitmix.unitstate.typecount[marine] = 11
        ext = unitmix.getExtantUnits(barracks)
        stripzeroes(ext)
        assert ext == {marine: 11}

    def test_next(self, unitmix_fix):
        unitmix = unitmix_fix
        assert unitmix.next(barracks) == marine

    def test_setUnitRatios(self, unitmix_fix):
        unitmix = unitmix_fix
        unitmix.setUnitRatios((
            (firebat, 1),
        ))
        assert unitmix.next(barracks) == firebat