"""
Predict how quickly units will carry out orders:
- turn time to get on right heading
- acceleration time, deceleration time at end of travel
- cruising time, at max speed
Also learn best way to control unit, ex. whether repeating commands causes
acceleration to speed up.
"""