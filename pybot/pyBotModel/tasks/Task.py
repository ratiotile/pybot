from enum import Enum


class TaskStatus(Enum):
    inactive = 0
    active = 1
    complete = 2
    failed = 3


class Task:

    def __init__(self, name, priority=1, parentTask=None):
        self.name = name
        self.priority = priority
        self.parent = parentTask
        self.status = TaskStatus.inactive

    def activate(self):
        """ Plan for the task """

    def terminate(self):
        """ Cleanup, call when ending task """

    def isInactive(self):
        return self.status == TaskStatus.inactive

    def isActive(self):
        return self.status == TaskStatus.active

    def isComplete(self):
        return self.status == TaskStatus.complete

    def isFailed(self):
        return self.status == TaskStatus.failed


class CompositeTask(Task):

    def __init__(self, name, priority=1, parentTask=None):
        super(CompositeTask, self).__init__(name, priority, parentTask)

    def addSubTask(self, subtask):
        """ Adds a child task to end of queue """
