""" Not for buildings or Archons
1. Check for enough resources
2. Acquire control of builder units (factory, larva)
3. Give train order
4. Monitor until done.
5. Complete successfully.
"""
from .Task import Task, TaskStatus
from pybot.interface.ResourceManager import ResourceManager


class TrainUnitTask(Task):

    def __init__(self, trainType, trainer=None):
        """
        trainType:  a BWAPI UnitType
        trainer:    a UnitModel
        """
        self.trainType = trainType
        self.trainer = trainer

    def activate(self):
        """
        check for sufficient resources and builder unit
        """
        if not ResourceManager.canBuild(self.trainType):
            return False
        elif not self.hasTrainer():
            print("todo: get a trainer")
        else:
            self.state = TaskStatus.active

    def hasTrainer(self):
        """
        check that current self.trainer exists and can build the target unit
        """
        if self.trainer is None:
            return False
        if not self.trainer.getType().canBuild(self.trainType):
            return False
        return True
