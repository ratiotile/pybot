import cybw

class fakeUnit:

    id_counter = 0

    def __init__(self, id=None, type=cybw.UnitTypes.Terran_SCV,
                 iscompleted=True):
        if id is None:
            self.id = self.id_counter
            self.__class__.id_counter += 1
        else:
            self.id = id
            self.__class__.id_counter = id + 1
        self.type = type
        self.iscompleted = iscompleted

    def getID(self):
        return self.id

    def getType(self):
        return self.type

    def isCompleted(self):
        return self.iscompleted