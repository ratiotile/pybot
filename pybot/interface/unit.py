import logging
from pybot.engine.task import PriorityTaskManager
from pybot.engine.event import EventEmitter, UnitEvent
from pybot.interface.prototype import InstanceCounter
import cybw

logger = logging.getLogger(__name__)

class UnitMapper:
    """ Interfaces between UnitModel and BWAPI. Allows for a fake unit to be
        used for testing. """
    def __init__(self):
        # id of unit to the cybw.Unit
        self.BWAPI_units = {}
        # id to unit wrapper
        self.id_unit = {}
        # initialize Unit to point to this instance
        Unit.unitmapper = self

    def add(self, bwunit):
        uid = bwunit.getID()
        if not uid in self.id_unit:
            um = Unit(bwunit)
            self.BWAPI_units[uid] = bwunit
            self.id_unit[uid] = um

    def dispatch(self, uid, methodname, *args, **kwargs):
        """ given unit ID and method name, dispatch to the actual unit and
        return the result.
        """
        # print("dispatch: unit<{}>.{}({})".format(uid, methodname,
        #     ", ".join(args)))
        bwunit = self.BWAPI_units[uid]
        # convert Unit args into Bwapi_unit
        args = list(args)
        for i in range(len(args)):
            if isinstance(args[i], Unit):
                args[i] = self.BWAPI_units[args[i].id]
        ret = getattr(bwunit, methodname)(*args, **kwargs)
        # translate BWAPI unit back
        if isinstance(ret, cybw.Unit):
            ret = self.get(ret)
        return ret

    def get(self, bwunit):
        if isinstance(bwunit, cybw.Unit):
            bwunit = bwunit.getID()
        return self.id_unit[bwunit]

    def getBW(self, unit):
        assert isinstance(unit, Unit)
        return self.BWAPI_units[unit.id]

    def getUnitsInRadius(self, radius, position):
        bwset = cybw.Broodwar.getUnitsInRadius(radius, position)
        units = set()
        for unit in bwset:
            units.add(self.get(unit))
        return units

    def remove(self, uid):
        self.id_unit.pop(uid, None)
        self.BWAPI_units.pop(uid, None)

class UnitManager:

    def __init__(self):
        self.all_units = set()
        # Conpleted unit wrappers only.
        self.units = set()
        # units not being controlled. Newly finished, or selected units
        self.free_units = set()
        # incomplete units
        self.incomplete_units = set()
        # unit up for grabs
        self.ee_unitready = EventEmitter(UnitEvent)
        self.ee_unitselected = EventEmitter(UnitEvent)
        self.ee_unitcreated = EventEmitter(UnitEvent)
        self.ee_unitlost = EventEmitter(UnitEvent)
        # unit just finished, emit only once per unit!
        self.ee_unitfinished = EventEmitter(UnitEvent)
        self.ee_unitmorphed = EventEmitter(UnitEvent)
        # onDestroy event gets called when building refinery
        # add refinery when morphed
        self.morphed_refineries = set()
        # prevent multiple tasks from unregistering on single unit
        self.claimed_production = set()

    def assignFreeUnits(self):
        """ send out events that a unit is ready. Other managers will have to
        respond and acquire the unit. Skip units which are player selected."""
        for unit in set(self.free_units):
            if unit.isSelected():
                continue  # allow human control when selected
            if unit.isIdle():
                self.ee_unitready.emit(unit)

    def addUnit(self, unit):
        """ takes a wrapped unit """
        self.all_units.add(unit)
        if unit.isCompleted():
            self.free_units.add(unit)
            logger.info("emit ee_unitfinished {}".format(unit))
            self.ee_unitfinished.emit(unit)
        else:  # unit was just created
            self.incomplete_units.add(unit)
            logger.info("emit unitcreated {}".format(unit))
            self.ee_unitcreated.emit(unit)

    def acquireUnit(self, unit):
        """ returns True on success. Moves unit from free pool into unit set. """
        if unit in self.free_units:
            self.units.add(unit)
            self.free_units.remove(unit)
            return True
        else:
            return False

    def checkIncompleteUnits(self):
        """ check if units are done, and move them to free_units """
        remaining_units = set()
        for unit in self.incomplete_units:
            if unit.isCompleted():
                self.free_units.add(unit)
                # BWAPI doesn't emit onComplete for morphs
                self.ee_unitfinished.emit(unit)
            else:
                remaining_units.add(unit)
        self.incomplete_units = remaining_units

    def checkSelectedUnits(self):
        """ check if any assigned units are selected by player, and issue an
        event to make everything give up control over them """
        for unit in self.units.copy():
            if unit.getType().canMove() and unit.isSelected():
                logger.info("emit selected: {}".format(unit))
                self.ee_unitselected.emit(unit)

    def claimProduction(self, newunit):
        # production tasks should attempt to claim unit before completing.
        logger.debug("claim production {}, {}".format(newunit,
                newunit not in self.claimed_production))
        if newunit not in self.claimed_production:
            self.claimed_production.add(newunit)
            return True
        return False

    def numIncomplete(self, utype):
        count = 0
        for unit in self.incomplete_units:
            if unit.getType() == utype:
                count += 1
        return count

    def onUnitDestroy(self, unit):
        logger.info("{} was lost.".format(unit))
        # workaround for BWAPI emits this when building a refinery
        if unit in self.morphed_refineries:
            self.morphed_refineries.remove(unit)
            return
        assert unit in self.all_units
        self.ee_unitlost.emit(unit)
        self.units.discard(unit)
        self.incomplete_units.discard(unit)
        self.all_units.discard(unit)
        self.free_units.discard(unit)

    def onUnitMorph(self, unit):
        logger.info("unit {} morphed.".format(unit))
        if unit.getID() not in self.all_units:
            self.addUnit(unit)
        if unit.getType().isRefinery():
            logger.debug("Morphed refinery {}".format(unit))
            self.morphed_refineries.add(unit)
        self.ee_unitmorphed.emit(unit)

    def unitReleased(self, unit):
        """ the caller has given up control of the unit, put it into free pool
        """
        if not isinstance(unit, Unit):
            import ipdb; ipdb.set_trace()
        self.free_units.add(unit)
        self.units.remove(unit)

    def update(self):
        self.checkIncompleteUnits()
        self.assignFreeUnits()
        self.checkSelectedUnits()
        self.claimed_production.clear()


class Unit(InstanceCounter):

    """ Wrapper around a Unit ID """

    unitmapper = None  # initialized in UnitMapper.__init__

    def __init__(self, cybwunit=None, uid=0):
        super().__init__()
        self.name = "Unit"
        if cybwunit is not None:
            self.id = cybwunit.getID()
            self.name = str(cybwunit.getType())
        else:
            self.id = uid
        self.tasks = PriorityTaskManager()

    def __getattribute__(self, name):
        """ we can override specific methods to say, look up unit state instead
        of directly calling the cybwUnit through UnitManager.
        """
        # print("getattr {}".format(name))
        return object.__getattribute__(self, name)

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        if isinstance(other, Unit):
            return self.id == other.id
        elif isinstance(other, cybw.Unit):
            return self.id == other.getID()

    def __str__(self):
        return "{}:{}".format(self.name, self.id)

    def runtask(self):
        pass

    def getUnit(self):
        return self.__class__.unitmapper.getBW(self)

    @classmethod
    def method_generator(cls, name):
        """ generate wrapper methods areound all Unit methods """

        def f(self, *args, **kwargs):
            # print("called {}({})".format(name, ','.join(args)))
            return cls.unitmapper.dispatch(self.id, name, *args, **kwargs)
        return f

    @classmethod
    def initialize(cls, funcnames):
        for name in funcnames:
            setattr(cls, name, cls.method_generator(name))

Unit.initialize([   # just pasted the method names here
  'attack',
  'build',
  'buildAddon',
  'burrow',
  'canAttack',
  'canAttackGrouped',
  'canAttackMove',
  'canAttackMoveGrouped',
  'canAttackUnit',
  'canAttackUnitGrouped',
  'canBuild',
  'canBuildAddon',
  'canBurrow',
  'canCancelAddon',
  'canCancelConstruction',
  'canCancelMorph',
  'canCancelResearch',
  'canCancelTrain',
  'canCancelTrainSlot',
  'canCancelUpgrade',
  'canCloak',
  'canCommand',
  'canCommandGrouped',
  'canDecloak',
  'canFollow',
  'canGather',
  'canHaltConstruction',
  'canHoldPosition',
  'canIssueCommand',
  'canIssueCommandGrouped',
  'canIssueCommandType',
  'canIssueCommandTypeGrouped',
  'canLand',
  'canLift',
  'canLoad',
  'canMorph',
  'canMove',
  'canMoveGrouped',
  'canPatrol',
  'canPatrolGrouped',
  'canPlaceCOP',
  'canRepair',
  'canResearch',
  'canReturnCargo',
  'canRightClick',
  'canRightClickGrouped',
  'canRightClickPosition',
  'canRightClickPositionGrouped',
  'canRightClickUnit',
  'canRightClickUnitGrouped',
  'canSetRallyPoint',
  'canSetRallyPosition',
  'canSetRallyUnit',
  'canSiege',
  'canStop',
  'canTargetUnit',
  'canTrain',
  'canUnburrow',
  'canUnload',
  'canUnloadAll',
  'canUnloadAllPosition',
  'canUnloadAtPosition',
  'canUnloadWithOrWithoutTarget',
  'canUnsiege',
  'canUpgrade',
  'canUseTech',
  'canUseTechPosition',
  'canUseTechUnit',
  'canUseTechWithOrWithoutTarget',
  'cancelAddon',
  'cancelConstruction',
  'cancelMorph',
  'cancelResearch',
  'cancelTrain',
  'cancelUpgrade',
  'cloak',
  'decloak',
  'exists',
  'follow',
  'gather',
  'getAcidSporeCount',
  'getAddon',
  'getAirWeaponCooldown',
  'getAngle',
  'getBottom',
  'getBuildType',
  'getBuildUnit',
  'getCarrier',
  'getClosestUnit',
  'getDefenseMatrixPoints',
  'getDefenseMatrixTimer',
  'getDistance',
  'getEnergy',
  'getEnsnareTimer',
  'getGroundWeaponCooldown',
  'getHatchery',
  'getHitPoints',
  'getID',
  'getInitialHitPoints',
  'getInitialPosition',
  'getInitialResources',
  'getInitialTilePosition',
  'getInitialType',
  'getInterceptorCount',
  'getInterceptors',
  'getIrradiateTimer',
  'getKillCount',
  'getLarva',
  'getLastAttackingPlayer',
  'getLastCommand',
  'getLastCommandFrame',
  'getLeft',
  'getLockdownTimer',
  'getMaelstromTimer',
  'getNydusExit',
  'getOrder',
  'getOrderTarget',
  'getOrderTargetPosition',
  'getOrderTimer',
  'getPlagueTimer',
  'getPlayer',
  'getPosition',
  'getPowerUp',
  'getRallyPosition',
  'getRallyUnit',
  'getRegion',
  'getRemainingBuildTime',
  'getRemainingResearchTime',
  'getRemainingTrainTime',
  'getRemainingUpgradeTime',
  'getRemoveTimer',
  'getReplayID',
  'getResourceGroup',
  'getResources',
  'getRight',
  'getScarabCount',
  'getSecondaryOrder',
  'getShields',
  'getSpaceRemaining',
  'getSpellCooldown',
  'getSpiderMineCount',
  'getStasisTimer',
  'getStimTimer',
  'getTarget',
  'getTargetPosition',
  'getTech',
  'getTilePosition',
  'getTop',
  'getTransport',
  'getType',
  'getUnitsInRadius',
  'getUnitsInWeaponRange',
  'getUpgrade',
  'getVelocityX',
  'getVelocityY',
  'haltConstruction',
  'hasNuke',
  'hasPath',
  'holdPosition',
  'isAccelerating',
  'isAttackFrame',
  'isAttacking',
  'isBeingConstructed',
  'isBeingGathered',
  'isBeingHealed',
  'isBlind',
  'isBraking',
  'isBurrowed',
  'isCarryingGas',
  'isCarryingMinerals',
  'isCloaked',
  'isCompleted',
  'isConstructing',
  'isDefenseMatrixed',
  'isDetected',
  'isEnsnared',
  'isFlying',
  'isFollowing',
  'isGatheringGas',
  'isGatheringMinerals',
  'isHallucination',
  'isHoldingPosition',
  'isIdle',
  'isInWeaponRange',
  'isInterruptible',
  'isInvincible',
  'isIrradiated',
  'isLifted',
  'isLoaded',
  'isLockedDown',
  'isMaelstrommed',
  'isMorphing',
  'isMoving',
  'isParasited',
  'isPatrolling',
  'isPlagued',
  'isPowered',
  'isRepairing',
  'isResearching',
  'isSelected',
  'isSieged',
  'isStartingAttack',
  'isStasised',
  'isStimmed',
  'isStuck',
  'isTargetable',
  'isTraining',
  'isUnderAttack',
  'isUnderDarkSwarm',
  'isUnderDisruptionWeb',
  'isUnderStorm',
  'isUpgrading',
  'isVisible',
  'issueCommand',
  'land',
  'lift',
  'load',
  'morph',
  'move',
  'patrol',
  'placeCOP',
  'repair',
  'research',
  'returnCargo',
  'rightClick',
  'setRallyPoint',
  'siege',
  'stop',
  'train',
  'unburrow',
  'unload',
  'unloadAll',
  'unsiege',
  'upgrade',
  'useTech'])
