from pybot.interface.utility import center, isFactory, isProduced
from cybw import TilePosition
from cybw import UnitTypes, UpgradeTypes, TechTypes

def test_center():
    tp = TilePosition(10, 10)
    assert tp/5 == TilePosition(2, 2)
    br = TilePosition(4, 4)
    assert center(tp, br) == TilePosition(7, 7)

def test_isFactory():
    assert not isFactory(UnitTypes.Terran_SCV)
    assert not isFactory(UnitTypes.Protoss_Probe)
    assert not isFactory(UnitTypes.Zerg_Drone)
    assert not isFactory(UnitTypes.Protoss_High_Templar)
    assert not isFactory(UnitTypes.Protoss_Dark_Templar)
    assert isFactory(UnitTypes.Protoss_Reaver)
    assert isFactory(UnitTypes.Protoss_Carrier)

def test_isProduced():
    assert isProduced(UnitTypes.Protoss_Dark_Templar)
    assert isProduced(UnitTypes.Protoss_Interceptor)
    assert isProduced(UpgradeTypes.Terran_Infantry_Armor)
    assert isProduced(TechTypes.Tank_Siege_Mode)
    assert isProduced(UnitTypes.Protoss_Scarab)
    assert isProduced(UnitTypes.Terran_Machine_Shop)