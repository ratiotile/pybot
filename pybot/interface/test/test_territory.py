import cybw
from pybot.interface.unit import UnitManager
from pybot.interface.territory import MapInfo
import pytest


@pytest.fixture
def mapInfo():
    mi = MapInfo()
    mi.is_analyzed = True
    return mi

class TestMapInfo:
    def test_isBuildable(self, mapInfo):
        um = UnitManager()
        tile = cybw.TilePosition(10, 10)
        mapInfo.buildability_tracker.reserve(tile)
        assert mapInfo.isBuildable(tile) == False
        opentile = cybw.TilePosition(10, 20)
        buildability = mapInfo.buildability_tracker
        assert buildability.isBuildable(opentile) == True