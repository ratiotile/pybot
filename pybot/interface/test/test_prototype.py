from pybot.interface.prototype import InstanceCounter, InstanceCounterMeta


def init_attributes(name, bases, dict):
    if 'attributes' in dict:
        for attr in dict['attributes']:
            dict[attr] = None

    return type(name, bases, dict)

class Initialised(object):
    __metaclass__ = init_attributes
    attributes = ['foo', 'bar', 'baz']

class AA(InstanceCounter):
    def __init__(self):
        super().__init__()


class BB(InstanceCounter):
    def __init__(self):
        super().__init__()


def test_InstanceCounter():
    a = AA()
    b = BB()
    assert a.id == b.id
