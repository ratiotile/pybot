from cybw import UnitTypes

def indexBy(iterable, findex):
    """ takes iterable and creates tuples out of each element, using findex
    callable to create a comparable value for the first element of the tuple.
    For use in sortIndex.
    """
    for val in iterable:
        yield (findex(val), val)

def center(TL, BR):
    diff = BR - TL
    return TL + (diff / 2)

def isProduced(item):
    """ is the item produced by a factory unit?

    :param item: item to check if is produced by a factory
    :type item: UnitType, TechType, UpgradeType
    :return: True if is produced by a factory
    :rtype: bool
    """
    if hasattr(item, "whatBuilds"):  # is unit
        builder = item.whatBuilds()[0]
        if isFactory(builder):
            return True
    elif hasattr(item, "whatUpgrades"):  # is upgrade
        upgrader = item.whatUpgrades()
        if isFactory(upgrader):
            return True
    elif hasattr(item, "whatResearches"):  # tech
        researcher = item.whatResearches()
        if isFactory(researcher):
            return True
    return False  # made with a

def whatProduces(item):
    # expects factory item, true for isProduced
    if hasattr(item, "whatBuilds"):  # is unit
        builder = item.whatBuilds()[0]
        if isFactory(builder):
            return builder
    elif hasattr(item, "whatUpgrades"):  # is upgrade
        upgrader = item.whatUpgrades()
        if isFactory(upgrader):
            return upgrader
    elif hasattr(item, "whatResearches"):  # tech
        researcher = item.whatResearches()
        if isFactory(researcher):
            return researcher
    raise TypeError("item {} is not produced in a factory!".format(item))

def isFactory(utype):
    # assumes utype actually produces or researches something
    if utype.isWorker():
        return False
    if(utype == UnitTypes.Protoss_Dark_Templar or
       utype == UnitTypes.Protoss_High_Templar):
        return False
    return True