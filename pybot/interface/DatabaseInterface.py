"""
Interfaces with SQLite to store data.
All static BWAPI data like UnitType stats will be stored to reduce dependence
on loading BWAPI.
"""
import cybw
import sqlite3 as sqlite
import time

import pprint
pp = pprint.PrettyPrinter(indent=2)

DB_NAME = 'pybot.db'
UNITTYPE_TABLE = "UnitType"
TECHTYPE_TABLE = "TechType"
UPGRADETYPE_TABLE = "UpgradeType"
UPG_REQ_TABLE = "UpgradeRequiredUnit"
UNIT_REQUIREMENTS_TABLE = "UnitReq"
RACE_TABLE = "Race"


class TypeInterface:
    pass


def load_BWAPI_UnitType_Data():
    """ Takes type data and generates a data structure, then stores it
    in sqlite database.
    """

    unittypes = []
    unitreqs = []
    start = time.process_time()
    for ut in cybw.UnitTypes.allUnitTypes():
        reqs = ut.requiredUnits()
        # skip special units
        if (ut.getName() != "None" and
          len(reqs.items()) == 0 and
          ut.getID() != 130  # Zerg_Infested_Command_Center
          ):
            continue
        # handle whatBuilds
        what_builds_unit, what_builds_num = ut.whatBuilds()

        unittypes.append({
            "id": ut.getID(),
            "name": fixname(ut.getName()),
            "race": ut.getRace().getID(),
            "topSpeed": ut.topSpeed(),
            "acceleration": ut.acceleration(),
            "haltDistance": ut.haltDistance(),
            "turnRadius": ut.turnRadius(),
            "mineralPrice": ut.mineralPrice(),
            "gasPrice": ut.gasPrice(),
            "buildTime": ut.buildTime(),
            "supplyRequired": ut.supplyRequired(),
            "supplyProvided": ut.supplyProvided(),
            "isBuilding": ut.isBuilding(),
            "isTwoUnitsInOneEgg": ut.isTwoUnitsInOneEgg(),
            "requiresPsi": ut.requiresPsi(),
            "requiresCreep": ut.requiresCreep(),
            "isWorker": ut.isWorker(),
            "isResourceDepot": ut.isResourceDepot(),
            "isRefinery": ut.isRefinery(),
            "isAddon": ut.isAddon(),
            "canBuildAddon": ut.canBuildAddon(),
            "requiredTech": ut.requiredTech().getID(),
            "canAttack": ut.canAttack(),
            "canProduce": ut.canProduce(),
            "whatBuildsUnit": what_builds_unit.getID(),
            "whatBuildsNum": what_builds_num
            })
        # (Unit ID, Req Unit ID, num needed)
        if len(reqs.items()) == 0:
            unitreqs.append([ut.getID(),
                             cybw.UnitTypes._None.getID(), 0])
        for rType, num in reqs.items():
            unitreqs.append([ut.getID(), rType.getID(), num])

    readin = time.process_time() - start
    con = sqlite.connect(DB_NAME)
    with con:
        cur = con.cursor()
        cur.execute("BEGIN TRANSACTION;")
        cur.execute('DROP TABLE IF EXISTS {};'.format(UNITTYPE_TABLE))
        cur.execute('DROP TABLE IF EXISTS {};'.format(
                                                UNIT_REQUIREMENTS_TABLE))
        cur.execute('''
            CREATE TABLE {}(  id INTEGER PRIMARY KEY,
                              name TEXT,
                              race INT,
                              topSpeed REAL,
                              acceleration INT,
                              haltDistance INT,
                              turnRadius INT,
                              mineralPrice INT,
                              gasPrice INT,
                              buildTime INT,
                              supplyRequired INT,
                              supplyProvided INT,
                              isBuilding INT,
                              isTwoUnitsInOneEgg INT,
                              requiresPsi INT,
                              requiresCreep INT,
                              isWorker INT,
                              isResourceDepot INT,
                              isRefinery INT,
                              isAddon INT,
                              canBuildAddon INT,
                              requiredTech INT,
                              canAttack INT,
                              canProduce INT,
                              whatBuildsUnit INT,
                              whatBuildsNum INT
                                  );
            '''.format(UNITTYPE_TABLE))
        cur.executemany('''
            INSERT INTO {} VALUES(  :id,
                                    :name,
                                    :race,
                                    :topSpeed,
                                    :acceleration,
                                    :haltDistance,
                                    :turnRadius,
                                    :mineralPrice,
                                    :gasPrice,
                                    :buildTime,
                                    :supplyRequired,
                                    :supplyProvided,
                                    :isBuilding,
                                    :isTwoUnitsInOneEgg,
                                    :requiresPsi,
                                    :requiresCreep,
                                    :isWorker,
                                    :isResourceDepot,
                                    :isRefinery,
                                    :isAddon,
                                    :canBuildAddon,
                                    :requiredTech,
                                    :canAttack,
                                    :canProduce,
                                    :whatBuildsUnit,
                                    :whatBuildsNum
                                    );
            '''.format(UNITTYPE_TABLE), unittypes)
        cur.execute('''
          CREATE TABLE {}(
            id INTEGER PRIMARY KEY,
            unit_id INT,
            req_unit_id INT,
            req_num INT,
            FOREIGN KEY(req_unit_id) REFERENCES UnitType(id),
            FOREIGN KEY(unit_id) REFERENCES UnitType(id)
            );'''.format(UNIT_REQUIREMENTS_TABLE))
        cur.executemany('''
          INSERT INTO {} VALUES(NULL,?,?,?);'''.format(
            UNIT_REQUIREMENTS_TABLE), unitreqs)
        con.commit()

        result = cur.fetchall()
        sqldone = time.process_time() - start
        print(str(result) + " time: " + str(sqldone))
    print("read time: " + str(readin))


def load_BWAPI_TechType_Data():
    """ Takes type data and generates a data structure, then stores it
    in sqlite database.
    """
    techs = []
    for tt in cybw.TechTypes.allTechTypes():
        techs.append({
            "id":               tt.getID(),
            "name":             fixname(tt.getName()),
            "race":             tt.getRace().getID(),
            "mineralPrice":     tt.mineralPrice(),
            "gasPrice":         tt.gasPrice(),
            "researchTime":     tt.researchTime(),
            "energyCost":       tt.energyCost(),
            "order":            tt.getOrder().getID(),
            "weapon":           tt.getWeapon().getID(),
            "targetsPosition":  tt.targetsPosition(),
            "targetsUnit":      tt.targetsUnit(),
            "whatResearches":   tt.whatResearches().getID(),
            })

    con = sqlite.connect(DB_NAME)
    with con:
        cur = con.cursor()
        cur.execute("BEGIN TRANSACTION;")
        cur.execute('DROP TABLE IF EXISTS {};'.format(TECHTYPE_TABLE))
        cur.execute('''
            CREATE TABLE {}(id INTEGER PRIMARY KEY,
                                  name TEXT,
                                  race INT,
                                  mineralPrice INT,
                                  gasPrice INT,
                                  researchTime INT,
                                  energyCost INT,
                                  orderID INT,
                                  weapon INT,
                                  targetsPosition INT,
                                  targetsUnit INT,
                                  whatResearches INT
                                  );
            '''.format(TECHTYPE_TABLE))

        cur.executemany('''
            INSERT INTO {} VALUES(:id,
                                        :name,
                                        :race,
                                        :mineralPrice,
                                        :gasPrice,
                                        :researchTime,
                                        :energyCost,
                                        :order,
                                        :weapon,
                                        :targetsPosition,
                                        :targetsUnit,
                                        :whatResearches
                                        );
            '''.format(TECHTYPE_TABLE), techs)
        con.commit()


def load_BWAPI_UpgradeType_Data():
    """ Takes type data and generates a data structure, then stores it
    in sqlite database.
    """
    upgrades = []  # contains the level-invariant data
    upg_req = []
    # pp.pprint([x for x in dir(cybw.UpgradeType) if filter_method_names(x)])
    for ut in cybw.UpgradeTypes.allUpgradeTypes():
        upgrades.append({
            "id":                   ut.getID(),
            "name":                 fixname(ut.getName()),
            "race":                 ut.getRace().getID(),
            "mineralPriceFactor":   ut.mineralPriceFactor(),
            "gasPriceFactor":       ut.gasPriceFactor(),
            "upgradeTimeFactor":    ut.upgradeTimeFactor(),
            "maxRepeats":           ut.maxRepeats(),
            "mineralPrice":         ut.mineralPrice(),
            "gasPrice":             ut.gasPrice(),
            "upgradeTime":          ut.upgradeTime(),
            "whatUpgrades":         ut.whatUpgrades().getID(),
        })
        # [upg_id, level, unit_id]
        for lvl in range(1, 4):   # 1,2,3
            upg_req.append((ut.getID(), lvl, ut.whatsRequired(lvl).getID()))

    con = sqlite.connect(DB_NAME)
    with con:
        cur = con.cursor()
        cur.execute("BEGIN TRANSACTION;")
        cur.execute('DROP TABLE IF EXISTS {};'.format(UPGRADETYPE_TABLE))
        cur.execute('DROP TABLE IF EXISTS {};'.format(UPG_REQ_TABLE))
        cur.execute('''
            CREATE TABLE {}(id INTEGER PRIMARY KEY,
                                  name TEXT,
                                  race INT,
                                  mineralPriceFactor INT,
                                  gasPriceFactor INT,
                                  upgradeTimeFactor INT,
                                  maxRepeats INT,
                                  mineralPrice INT,
                                  gasPrice INT,
                                  upgradeTime INT,
                                  whatUpgrades INT
                                  );
            '''.format(UPGRADETYPE_TABLE))
        cur.executemany('''
            INSERT INTO {} VALUES(:id,
                                        :name,
                                        :race,
                                        :mineralPriceFactor,
                                        :gasPriceFactor,
                                        :upgradeTimeFactor,
                                        :maxRepeats,
                                        :mineralPrice,
                                        :gasPrice,
                                        :upgradeTime,
                                        :whatUpgrades
                                        );
            '''.format(UPGRADETYPE_TABLE), upgrades)
        cur.execute('''
            CREATE TABLE {}(
                upgradeID INT,
                level INT,
                requiredUnitID INT,
                FOREIGN KEY(upgradeID) REFERENCES {}(id),
                FOREIGN KEY(requiredUnitID) REFERENCES {}(id)
            );'''.format(UPG_REQ_TABLE, UPGRADETYPE_TABLE, UNITTYPE_TABLE))
        cur.executemany('''
            INSERT INTO {} VALUES(?,?,?);
          '''.format(UPG_REQ_TABLE), upg_req)
        con.commit()
        result = cur.fetchall()
        print(str(result))


def load_BWAPI_Race_Data():
    """ Takes type data and generates a data structure, then stores it
    in sqlite database.
    """
    races = []  # contains the level-invariant data
    # pp.pprint([x for x in dir(cybw.UpgradeType) if filter_method_names(x)])
    for r in cybw.Races.allRaces():
        races.append({
            "id":           r.getID(),
            "name":         fixname(r.getName()),
            "depot":        r.getCenter().getID(),
            "refinery":     r.getRefinery().getID(),
            "supplyUnit":   r.getSupplyProvider().getID(),
            "transport":    r.getTransport().getID(),
            "worker":       r.getWorker().getID()
        })

    con = sqlite.connect(DB_NAME)
    with con:
        cur = con.cursor()
        cur.execute("BEGIN TRANSACTION;")
        cur.execute('DROP TABLE IF EXISTS {};'.format(RACE_TABLE))
        cur.execute('''
            CREATE TABLE {}(
                id INTEGER PRIMARY KEY,
                name TEXT,
                depot INT,
                refinery INT,
                supplyUnit INT,
                transport INT,
                worker INT
                              );
            '''.format(RACE_TABLE))
        cur.executemany('''
            INSERT INTO {} VALUES(  :id,
                                    :name,
                                    :depot,
                                    :refinery,
                                    :supplyUnit,
                                    :transport,
                                    :worker
                                    );
            '''.format(RACE_TABLE), races)
        con.commit()
        result = cur.fetchall()
        print(str(result))


def getTypeDataFromTable(table_name):
    """ return iterator on sqlite3.Row objects from database.
    Used to build -Type data structures """
    con = sqlite.connect(DB_NAME)
    with con:
        c = con.cursor()
        c.row_factory = sqlite.Row
        c.execute('select * from ' + table_name)
        data = c.fetchall()
    return data


def loadAllTypes():
    load_BWAPI_UpgradeType_Data()
    load_BWAPI_TechType_Data()
    load_BWAPI_UnitType_Data()
    load_BWAPI_Race_Data()


def printTest_Names(table):
    data = getTypeDataFromTable(table)
    for row in data:
        print(row["name"])


def printTest_Units():
    printTest_Names(UNITTYPE_TABLE)


def printTest_Techs():
    printTest_Names(TECHTYPE_TABLE)


def printTest_Upgrades():
    printTest_Names(UPGRADETYPE_TABLE)


def printTest_ReleventUnits():
    for ut in cybw.UnitTypes.allUnitTypes():
        if ut.getName() == "None" or len(ut.requiredUnits().items()) > 0:
            print(ut.getName())


def filter_method_names(name):
    """ use with dir() to get public attributes """
    return "__" not in name


def fixname(name):
    if name == "None":
        return "none"
    return name
