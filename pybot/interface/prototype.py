import itertools

class InstanceCounterMeta(type):
    """ Metaclass to make instance counter not share count with descendants
    """
    def __init__(cls, name, bases, attrs):
        super().__init__(name, bases, attrs)
        cls._ids = itertools.count(1)

class InstanceCounter(object, metaclass=InstanceCounterMeta):
    """ Mixin to add automatic ID generation
    """
    def __init__(self):
        self.id = next(self.__class__._ids)

class TaskOwner:
    """ Mixin to hold tasks and terminate them when they are done
    """
    def __init__(self):
        self.tasks = set()

    def update(self):
        for task in self.tasks.copy():
            if task.isCompleted() or task.isFailed():
                self.tasks.remove(task)