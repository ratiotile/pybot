from cybw import WalkPosition, Position, TilePosition, Positions, Flag, \
    Broodwar, Colors, BtoW
import logging
from enum import Enum
from collections import defaultdict
import math
from pybot.engine.pathfinder import Pathfinder

logger = logging.getLogger(__name__)



class MapInfo:

    """ representation of a map """

    def __init__(self):
        """ expects an instance of TerrainAnalyzer.
        creates local copy of the data """
        self.is_analyzed = False
        # handles buildtile buildability
        self.pathfinder = Pathfinder(self)
        self.buildability_tracker = BuildabilityTracker(self, self.pathfinder)

    def onMapAnalyzed(self, terrainanalyzer):
        self.walkwidth = terrainanalyzer.w
        self.walkheight = terrainanalyzer.h
        self.clearance = terrainanalyzer.clearance
        self.small_obstacles = terrainanalyzer.ignored
        self.wtile_region = terrainanalyzer.tile_to_region
        self.regions = terrainanalyzer.regions
        self.chokes = terrainanalyzer.chokes
        # TODO: don't need the clusters, just the baselocations
        self.resource_clusters = terrainanalyzer.resource_clusters
        self.connectivity = terrainanalyzer.access
        self.conn_id_impassable = terrainanalyzer.access_id_impassable
        self.is_analyzed = True

        logger.info("Map initialized")

    def getMousePosition(self):
        """ returns mouse position in map Position units """
        pos = Broodwar.getMousePosition()
        if pos == Positions.Unknown:
            return pos
        screenpos = Broodwar.getScreenPosition()
        return pos + screenpos

    def getRegion(self, pos):
        """ returns region at position """
        if not isinstance(pos, WalkPosition):
            pos = WalkPosition(pos)
        return self.wtile_region[pos.x][pos.y]

    def _isBuildable(self, tpos):
        return Broodwar.isBuildable(tpos)

    def isBuildable(self, tpos):
        return (self.buildability_tracker.isBuildable(tpos) and
                self._isBuildable(tpos))

    def isAreaWalkable(self, walktop, walkleft, walkright, walkbottom):
        for y in range(walktop, walkbottom):
            for x in range(walkleft, walkright):
                if not Broodwar.isWalkable(x, y):
                    return False
        return True

    def isTileWalkable(self, tilepos):
        return(self.isAreaWalkable(BtoW(tilepos.y), BtoW(tilepos.x),
                                   BtoW(tilepos.x) + 3, BtoW(tilepos.y) + 3)
               and self.buildability_tracker.isWalkable(tilepos)
               )

    def tileWidth(self):
        return self.walkwidth * 4

    def tileHeight(self):
        return self.walkheight * 4

    def update(self):
        self.buildability_tracker.debug_draw()

    def drawRegionLabels(self):
        for region in self.regions:
            center = Position(region.center)
            rid = region.id
            Broodwar.drawTextMap(center, str(rid))

    def drawMouseRegion(self):
        """ draw dots in region under mouse """
        if not Broodwar.isFlagEnabled(Flag.UserInput):
            return
        mpos = self.getMousePosition()
        region = self.getRegion(mpos)
        screenpos = WalkPosition(Broodwar.getScreenPosition())

        for x in range(screenpos.x, screenpos.x + 80):
            for y in range(screenpos.y, screenpos.y + 60):
                if x >= self.walkwidth or y >= self.walkheight:
                    continue
                if self.wtile_region[x][y] == region:
                    dpos = Position(x*8, y*8)
                    Broodwar.drawDotMap(dpos, Colors.Green)

class Buildability(Enum):  # for TilePositions in Reserved Tiles
    yes = 0  # can build here
    walkonly = 1  # can't build but is passable
    nowalk = 2  # can't build or walk
    reserved = 3  # planned to build on
    bib = 4  # space around building for access
    resourcing = 5  # space between resourceDepot and resources

class BuildabilityTracker:

    def __init__(self, mapinfo, pathfinder):
        # TilePosition to buildability, sparse representation
        self.buildable = defaultdict(lambda: Buildability.yes)
        self.map = mapinfo
        self.pathfinder = pathfinder
        if not Broodwar.isNull():  # check that we are connected to BW
            self.addStaticBuildings()

    def addResourceArea(self, resources, depot):
        logger.info("add resource area")
        # draw lines from each resource to depot
        dpos = depot.getTilePosition()
        for res in resources:
            rpos = res.getTilePosition()
            dx = math.fabs(dpos.x - rpos.x)
            dy = math.fabs(dpos.y - rpos.y)
            x, y = rpos.getXY()
            n = 1 + dx + dy
            ix = 1 if rpos.x < dpos.x else -1
            iy = 1 if rpos.y < dpos.y else -1

            error = dx - dy
            dx *= 2
            dy *= 2

            lasttile = TilePosition(x, y)
            while n > 0:
                tile = TilePosition(x, y)
                if tile != lasttile and tile.isValid():
                    lasttile = tile
                    if self.buildable[tile] != Buildability.nowalk:
                        self.buildable[tile] = Buildability.resourcing
                if error > 0:
                    x += ix
                    error -= dy
                else:
                    y += iy
                    error += dx
                n -= 1

        self.reserveSpaceAroundDepot(depot)

    def reserveSpaceAroundDepot(self, depot):
        """ puts a bib around the depot to avoid screwing up worker spawn

        :param depot: ResourceDepot
        :type depot: Unit
        :return:
        :rtype:
        """
        dtl = depot.getTilePosition()
        dbr = dtl + depot.getType().tileSize()
        left = dtl.x - 1
        y = dbr.y
        right = dbr.x + 1
        for x in range(left, right):
            tile = TilePosition(x, y)
            self.buildable[tile] = Buildability.bib


    def addStaticBuildings(self):
        minerals = Broodwar.getStaticMinerals()
        geysers = Broodwar.getStaticGeysers()
        for m in minerals:
            TL = m.getInitialTilePosition()
            BR = TL + m.getType().tileSize()
            self.markTiles(TL, BR, Buildability.nowalk)
        for g in geysers:
            TL = g.getInitialTilePosition()
            BR = TL + g.getType().tileSize()
            self.markTiles(TL, BR, Buildability.nowalk)

    def markTiles(self, TL, BR, b):
        for x in range(TL.x, BR.x):
            for y in range(TL.y, BR.y):
                tile = TilePosition(x, y)
                self.buildable[tile] = b

    def markExitPaths(self, depot):
        dpos = depot.getTilePosition()
        logger.info("mark exit paths from {}".format(dpos))
        dpos.y += 2
        region = self.map.getRegion(dpos)
        for choke in region.chokes:
            exitpath = self.pathfinder.createTilePath(dpos, TilePosition(
                choke.center))
            print("path length " + str(len(exitpath)))
            for tile in exitpath:
                if self.buildable[tile] == Buildability.yes:
                    self.buildable[tile] = Buildability.bib

    def reserve(self, tilepos):
        """ reserves TilePosition for future building """
        self.buildable[tilepos] = Buildability.reserved

    def free(self, tilepos):
        """ clear reservation on a TilePosition """
        self.buildable[tilepos] = Buildability.yes

    def isBuildable(self, tilepos):
        """ returns True if the tile is buildable and has not been reserved.

        Args:
            tilepos: TilePosition of spot to test

        Returns: True if we can build there, False if we cannot.
        """
        return self.buildable[tilepos] == Buildability.yes

    def isWalkable(self, tilepos):
        return self.buildable[tilepos] != Buildability.nowalk

    def debug_draw(self):
        for tile, bb in self.buildable.items():
            x, y = tile.getXY()
            x = x * 32 + 16
            y = y * 32 + 16
            text = "X"
            if bb == Buildability.yes: continue
            if bb == Buildability.resourcing: text = "R"
            if bb == Buildability.bib: text = "B"
            Broodwar.drawTextMap(Position(x, y), text)