def test_fakeUnit():
    from ..pybot.interface.fakes import fakeUnit
    a = fakeUnit()
    b = fakeUnit()
    c = fakeUnit(99)
    d = fakeUnit()
    assert a.getID() == 0
    assert b.getID() == 1
    assert c.getID() == 99
    assert d.getID() == 100

def test_UnitManager_addUnit():
    from ..pybot.interface.unit import UnitManager
    from ..pybot.interface.fakes import fakeUnit
    a = fakeUnit()
    UM = UnitManager()
    UM.addUnit(a)
    assert len(UM.free_units) == 1
