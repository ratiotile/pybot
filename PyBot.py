import logging
from threading import Thread
import time
import cybw
from cybw import Broodwar
from tabw.TerrainAnalyzer import TerrainAnalyzer
from pybot import settings
from pybot.engine.harvester import Harvester
from pybot.interface.unit import UnitManager, UnitMapper
from pybot.engine.task import TaskMaster
from pybot.state.resource import Bank
from pybot.interface.territory import MapInfo
from pybot.engine.builder import BuildingPlacer, Builder
from pybot.ai.policy import BuildPolicy
from pybot.engine.state import UnitState
from pybot.state import player as PS

# note: __name__ here makes logging in other modules not work!
logger = logging.getLogger("pybot")

class MapAnalysisWorker(Thread):

    def __init__(self):
        super(MapAnalysisWorker, self).__init__()
        self.TA = None

    def run(self):
        global analysis_done
        logger.info("starting analysis!")
        self.TA = TerrainAnalyzer(Broodwar)
        self.TA.analyze()
        analysis_done = True

class Pybot:

    def __init__(self):
        # interface
        self.unitmapper = UnitMapper()
        self.unitmanager = UnitManager()
        self.map = MapInfo()
        # state
        self.bank = Bank()
        self.taskmaster = TaskMaster()
        # engine
        self.harvester = Harvester(unitmanager=self.unitmanager,
            taskmaster=self.taskmaster, mapinfo=self.map, bank=self.bank,
            unitmapper=self.unitmapper)
        self.placer = BuildingPlacer(self.map, self.unitmanager, self.harvester)
        self.builder = Builder(self.placer, taskmaster=self.taskmaster,
                               bank=self.bank, unitmanager=self.unitmanager,
                               harvester=self.harvester)
        # engine state
        self.unitstate = UnitState(unitmanager=self.unitmanager,
                                   builder=self.builder)
        # mid-level AI
        self.buildpolicy = BuildPolicy(self.harvester, self.unitmanager,
                                       self.bank, self.builder,
                                       unitstate=self.unitstate)


    def onMapAnalyzed(self, analyzer):
        self.map.onMapAnalyzed(analyzer)

        # create paths out of base
        for depot in [n.depot for n in self.harvester.harvest_nodes]:
            self.map.buildability_tracker.markExitPaths(depot)



    def onUnitComplete(self, unit):
        # useless BWAPI implementation doesn't work on morphs
        pass

    def onUnitCreate(self, unit):
        # need to sequester the new units until they are completed!
        # logger.info("{} created".format(unit))
        self.unitmapper.add(unit)
        if unit.getPlayer() == Broodwar.self():
            # print("one of ours")
            uw = self.unitmapper.get(unit.getID())
            self.unitmanager.addUnit(uw)

    def onUnitDiscover(self, unit):
        self.unitmapper.add(unit)
        #print("A {} has been discovered at {}".format(
        #    unit, unit.getPosition()))

    def onUnitShow(self, unit):
        pass
        #print("A {} has appeared at {}".format(
        #    unit, unit.getPosition()))

    def onUnitEvade(self, unit):
        pass

    def onUnitHide(self, unit):
        pass

    def onUnitMorph(self, unit):
        if unit.getPlayer() == Broodwar.self():
            uw = self.unitmapper.get(unit.getID())
            self.unitmanager.onUnitMorph(uw)

    def onUnitRenegade(self, unit):
        if unit.getPlayer() == Broodwar.self():
            uw = self.unitmapper.get(unit.getID())
            self.unitmanager.onUnitDestroy(uw)

    def onUnitDestroy(self, unit):
        if unit.getPlayer() == Broodwar.self():
            uw = self.unitmapper.get(unit.getID())
            self.unitmanager.onUnitDestroy(uw)
        self.unitmapper.remove(unit.getID())

    def onStart(self):
        PS.ownplayer = Broodwar.self()

    def onFrame(self):
        if Broodwar.isPaused():
            return  # prevents worker control from spazzing out
        tframebegin = time.clock()
        self.bank.update(Broodwar.getFrameCount(),
            mineral=Broodwar.self().minerals(),
            gas=Broodwar.self().gas(),
            supply=(Broodwar.self().supplyUsed(),
                    Broodwar.self().supplyTotal()))
        self.buildpolicy.update() # update after bank!
        self.harvester.update()
        self.unitmanager.update()

        self.placer.update()
        self.map.update()
        self.builder.update()
        self.taskmaster.update()  # need to update after builder

        self.unitstate.debugDraw()
        # map debugging
        if self.map.is_analyzed and settings.debug_draw.regions:
            self.map.drawMouseRegion()
            self.map.drawRegionLabels()

        frametime = time.clock() - tframebegin
        if frametime > 0.055:
            logger.warn("Frame {} time limit exceeded: {} > 0.055s".format(
                Broodwar.getFrameCount(), frametime))
    def onEnd(self, event):
        pass

    def onNukeDetect(self, event):
        pass

    def onPlayerLeft(self, event):
        pass

    def onReceiveText(self, event):
        pass

    def onSendText(self, event):
        Broodwar.sendText(event.getText())

    def onSaveGame(self, event):
        pass


analysis_done = False

def main():
    global analysis_done

    def reconnect():
        while not client.connect():
            time.sleep(0.5)

    client = cybw.BWAPIClient
    check_analysis = True

    print("Connecting...")
    reconnect()
    while True:
        print("waiting to enter match")
        while not Broodwar.isInGame():
            client.update()
            if not client.isConnected():
                print("Reconnecting...")
                reconnect()
        print("starting match!")
        Broodwar.enableFlag(cybw.Flag.UserInput)
        # message the analysis process to start
        TA_worker = MapAnalysisWorker()
        TA_worker.start()
        pybot = Pybot()

        Broodwar.sendText("black sheep wall")
        Broodwar.sendText("operation cwal")
        Broodwar.sendText("whats mine is mine")
        print(Broodwar.mapFileName())

        while Broodwar.isInGame():
            if check_analysis:
                TA_worker.join(0.055)
                if analysis_done:
                    logger.info("analysis done!")
                    pybot.onMapAnalyzed(TA_worker.TA)
                    check_analysis = False

            events = Broodwar.getEvents()
            for e in events:
                eventtype = e.getType()
                if eventtype == cybw.EventType.MatchStart:
                    pybot.onStart()

                elif eventtype == cybw.EventType.MatchFrame:
                    pybot.onFrame()

                elif eventtype == cybw.EventType.MatchEnd:
                    pybot.onEnd(e)

                elif eventtype == cybw.EventType.SendText:
                    pybot.onSendText(e)

                elif eventtype == cybw.EventType.ReceiveText:
                    pybot.onReceiveText(e)

                elif eventtype == cybw.EventType.PlayerLeft:
                    pybot.onPlayerLeft(e)

                elif eventtype == cybw.EventType.NukeDetect:
                    pybot.onNukeDetect(e)

                elif eventtype == cybw.EventType.UnitCreate:
                    pybot.onUnitCreate(e.getUnit())

                elif eventtype == cybw.EventType.UnitComplete:
                    pybot.onUnitComplete(e.getUnit())

                elif eventtype == cybw.EventType.UnitDiscover:
                    pybot.onUnitDiscover(e.getUnit())

                elif eventtype == cybw.EventType.UnitDestroy:
                    pybot.onUnitDestroy(e.getUnit())

                elif eventtype == cybw.EventType.UnitMorph:
                    pybot.onUnitMorph(e.getUnit())

                elif eventtype == cybw.EventType.UnitRenegade:
                    pybot.onUnitRenegade(e.getUnit())

                elif eventtype == cybw.EventType.UnitShow:
                    pybot.onUnitShow(e.getUnit())

                elif eventtype == cybw.EventType.UnitHide:
                    pybot.onUnitHide(e.getUnit())

                elif eventtype == cybw.EventType.SaveGame:
                    pybot.onSaveGame(e.getUnit())

            client.update()
            while not Broodwar.isInGame():
                client.update()
                if not client.isConnected():
                    print("Reconnecting...")
                    reconnect()

# necessary on Windows to protect entry point
if __name__ == "__main__":
    logger.setLevel(logging.NOTSET)
    fh = logging.FileHandler("pybot.log", mode='w')
    ch = logging.StreamHandler()
    ch.setLevel(logging.NOTSET)
    formatter = logging.Formatter(
        '%(asctime)s.%(msecs).03d - %(name)s - %(levelname)s - %(message)s',
        '%H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(ch)
    logger.info("begin")
    main()

