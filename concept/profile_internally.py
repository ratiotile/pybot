import logging
from threading import Thread
import time
import cybw
from cybw import Broodwar
from tabw.TerrainAnalyzer import TerrainAnalyzer
from pybot import clientevents
import cProfile
# note: __name__ here makes logging in other modules not work!
logger = logging.getLogger("pybot")

class MapAnalysisWorker(Thread):

    def __init__(self):
        super(MapAnalysisWorker, self).__init__()

    def run(self):
        global analysis_done
        logger.info("starting analysis!")
        TA = TerrainAnalyzer(Broodwar)
        TA.analyze()
        analysis_done = True

analysis_done = False

def main():
    global analysis_done
    profile = cProfile.Profile()

    def reconnect():
        while not client.connect():
            time.sleep(0.5)

    client = cybw.BWAPIClient
    check_analysis = True

    print("Connecting...")
    reconnect()
    while True:
        print("waiting to enter match")
        while not Broodwar.isInGame():
            client.update()
            if not client.isConnected():
                print("Reconnecting...")
                reconnect()
        print("starting match!")
        Broodwar.enableFlag(cybw.Flag.UserInput)
        # message the analysis process to start
        profile.enable()
        TA_worker = MapAnalysisWorker()
        TA_worker.start()

        Broodwar.sendText("black sheep wall")
        print(Broodwar.mapFileName())
        #
        #
        #Broodwar.leaveGame()
        clientevents.onStart()  # doesn't seem to trigger on events

        while Broodwar.isInGame():
            if check_analysis:
                TA_worker.join(0.055)
                if analysis_done:
                    logger.info("analysis done!")
                    check_analysis = False
                    profile.disable()
                    profile.dump_stats("pybot_profile.log")

            events = Broodwar.getEvents()
            for e in events:
                eventtype = e.getType()
                if eventtype == cybw.EventType.MatchStart:
                    clientevents.onStart()

                elif eventtype == cybw.EventType.MatchFrame:
                    clientevents.onFrame()

                elif eventtype == cybw.EventType.MatchEnd:
                    clientevents.onEnd(e)

                elif eventtype == cybw.EventType.SendText:
                    clientevents.onSendText(e)

                elif eventtype == cybw.EventType.ReceiveText:
                    clientevents.onReceiveText(e)

                elif eventtype == cybw.EventType.PlayerLeft:
                    clientevents.onPlayerLeft(e)

                elif eventtype == cybw.EventType.NukeDetect:
                    clientevents.onNukeDetect(e)

                elif eventtype == cybw.EventType.UnitCreate:
                    clientevents.onUnitCreate(e.getUnit())

                elif eventtype == cybw.EventType.UnitDestroy:
                    clientevents.onUnitDestroy(e.getUnit())

                elif eventtype == cybw.EventType.UnitMorph:
                    clientevents.onUnitMorph(e.getUnit())

                elif eventtype == cybw.EventType.UnitRenegade:
                    clientevents.onUnitRenegade(e.getUnit())

                elif eventtype == cybw.EventType.UnitShow:
                    clientevents.onUnitShow(e.getUnit())

                elif eventtype == cybw.EventType.UnitHide:
                    clientevents.onUnitHide(e.getUnit())

                elif eventtype == cybw.EventType.SaveGame:
                    clientevents.onSaveGame(e.getUnit())

            client.update()
            while not Broodwar.isInGame():
                client.update()
                if not client.isConnected():
                    print("Reconnecting...")
                    reconnect()

# necessary on Windows to protect entry point
if __name__ == "__main__":
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler("pybot.log", mode='w')
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter(
        '%(asctime)s.%(msecs).03d - %(name)s - %(levelname)s - %(message)s',
        '%H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(ch)
    logger.info("begin")
    main()
