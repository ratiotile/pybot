from greenlet import greenlet
from time import sleep

class GreenletTaskScheduler:

    def __init__(self):
        self.tasks = []

    def createtask(self, task):
        gtask = greenlet(task)
        self.tasks.append(gtask)

    def update(self):
        for task in self.tasks:
            task.switch()


def foo():
    while True:
        sleep(1)
        print("foo")
        greenlet.getcurrent().parent.switch()


def bar():
    while True:
        sleep(1)
        print("bar")
        greenlet.getcurrent().parent.switch()


mgr = GreenletTaskScheduler()
mgr.createtask(foo)
mgr.createtask(bar)

for i in range(10):
    mgr.update()
