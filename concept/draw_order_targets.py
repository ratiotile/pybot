from pybot.interface.unit import UnitManager
import cybw
from time import sleep
from cybw import Position

client = cybw.BWAPIClient
Broodwar = cybw.Broodwar

def drawOrderTargets():
    units = Broodwar.self().getUnits()
    for unit in units:
        if not unit.getType().isWorker() or not unit.isCompleted():
            continue
        try
:            target = unit.getTarget()
            ordertarget = unit.getOrderTarget()
            Broodwar.drawLineMap(target.getPosition(), unit.getPosition(), cybw.Colors.White)
            Broodwar.drawLineMap(
                Position(ordertarget.getPosition().x,ordertarget.getPosition().y+1),
                Position(unit.getPosition().x,unit.getPosition().y+1), cybw.Colors.Yellow)
        except cybw.NullPointerException:
            print("order is {}, {}".format(unit.getOrder(), unit.getSecondaryOrder()))
            continue

def reconnect()
:    while not client.connect():
        sleep(0.5)

print("Connecting...")
reconnect()
while True:
    print("waiting to enter match")
    while not Broodwar.isInGame():
        client.update()
        if not client.isConnected():
            print("Reconnecting...")
            reconnect()
    print("starting match!")
    # Enable some cheat flags
    Broodwar.enableFlag(cybw.Flag.UserInput);

    if Broodwar.isReplay():
        Broodwar << "The following players are in this replay:\n"
        players = Broodwar.getPlayers()
        #TODO add rest of replay actions

    else:
        units    = Broodwar.self().getUnits();
        minerals  = Broodwar.getMinerals();

        for unit in units:
            if unit.getType().isWorker():
                closestMineral = None
                for mineral in minerals:
                    if closestMineral is None or unit.getDistance(mineral) < unit.getDistance(closestMineral):
                        closestMineral = mineral
                if closestMineral:
                    unit.rightClick(closestMineral)

            elif unit.getType().isResourceDepot():
                unit.train(Broodwar.self().getRace().getWorker())

    while Broodwar.isInGame():
        drawOrderTargets()

        client.update()

