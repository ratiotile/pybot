from greenlet import greenlet
from time import sleep, clock
import random

#now try to interrupt the event loop

class GreenletTaskScheduler:

    def __init__(self):
        self.tasks = {}  # Task to greenlet
        self.timeout = 2
        self.gloop = None
        self.first_task = None  # prevent from repeating task in a cycle

    def createtask(self, task):
        self.tasks[task] = greenlet(task.run)

    def update(self, n):
        self.first_task = None  # so taskloop will update it this cycle
        self.begin_time = clock()
        for task in self.tasks.keys():
            task.update(n)
        if self.gloop is None or self.gloop.dead:
            self.gloop = greenlet(self.taskloop)
        self.gloop.switch()
        # eat up remaining time
        if self.gloop.dead:
            self.gloop = greenlet(self.taskloop)
            self.gloop.switch()

    def taskloop(self):
        for tglet in self.tasks.values():
            if self.first_task == tglet:
                print("reached start of task cycle again!")
                return
            if self.first_task is None:
                self.first_task = tglet
            # must rewrite parent here, since greenlets were created outside
            tglet.parent = greenlet.getcurrent()
            tglet.switch()
            if clock() - self.begin_time >= self.timeout:
                print("timeout in taskloop!")
                greenlet.getcurrent().parent.switch(True)

class Task:

    def __init__(self, name):
        self.name = name
        self.num = 0

    def run(self):
        while True:
            sleep(random.uniform(0.1, 0.5))
            self.say()
            greenlet.getcurrent().parent.switch()

    def say(self):
        print(self.name + " " + str(self.num))

    def update(self, n):
        self.num = n

mgr = GreenletTaskScheduler()
t1 = Task("foo")
t2 = Task("bar")
t3 = Task("spam")
t4 = Task("eggs")
t5 = Task("monty")
t6 = Task("python")
mgr.createtask(t1)
mgr.createtask(t2)
mgr.createtask(t3)
mgr.createtask(t4)
mgr.createtask(t5)
mgr.createtask(t6)

for i in range(10):
    mgr.update(i)
