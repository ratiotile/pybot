""" Run the bot with "python -m cProfile -o pybot.prof PyBot.py" first to
generate the profile file to analyze """

import pstats

p = pstats.Stats("pybot.prof")
p.strip_dirs().sort_stats("tottime", "cumtime").print_stats(50)