from pybot.state.resource import Resource, Reserve

def coroutine(func):
    def start(*args, **kwargs):
        g = func(*args, **kwargs)
        next(g)
        return g
    return start

class task:

    def __init__(self, state):
        self.state = state
        self.gen = self.run()

    @coroutine
    def run(self):
        with Reserve(50):
            while self.state != "finished":
                print("wait for input")
                newstate = yield  # A
                print("input: " + newstate)
                self.state = newstate
                print("wait for next, state: " + self.state)
                yield self.state  # B

        return "finished"

    def update(self, state="running"):
        try:
            r = self.gen.send(state)  # A
            next(self.gen)  # B
            return r
        except StopIteration as e:
            if len(e.args) == 0:
                raise e
                return 'finished!'
            retval = e.args[0]
            return retval


def runTask(t):
    try:
        return next(t)
    except StopIteration as e:
        if len(e.args) == 0:
            raise e
            return 'finished!'
        retval = e.args[0]
        return retval

Resource.update(100)

print(Resource.mineralsAvailable())
t = task("start")
print(Resource.mineralsAvailable())
print("got: " + t.update())
print(Resource.mineralsAvailable())
print("got: " + t.update())
print(Resource.mineralsAvailable())
print("got: " + t.update('finished'))
print(Resource.mineralsAvailable())
