from greenlet import greenlet
from time import sleep, clock
import random, itertools
from collections import deque

# Only cycle through each task once in a frame, and each frame has limited time
# Want to restart the cycle where we left off, thus use coroutine.
# Add handling of empty dictionary

class GreenletTaskScheduler:

    def __init__(self):
        self.tasks = {}  # Task to greenlet
        self.timeout = 0.5
        self.gloop = greenlet(self.taskloop)
        self.first_task = None  # prevent from repeating task in a cycle
        self.last_task = None  # fastforward when recreating gloop
        self.stopitercount = 0

    def createtask(self, task):
        self.tasks[task] = greenlet(task.run)
        self.gloop = greenlet(self.taskloop)

    def cleartasks(self):
        print("delete all tasks")
        self.tasks = {}
        self.gloop = greenlet(self.taskloop)

    def update(self, n):
        print(str(n) + "th frame")
        self.stopitercount = 0
        self.first_task = None  # so taskloop will update it this cycle
        self.begin_time = clock()
        for task in self.tasks.keys():
            task.update(n)
        self.gloop.switch()

    def taskloop(self):
        # loop forever over cycles
        t_iter = itertools.cycle(self.tasks.items())
        if(self.last_task in self.tasks.keys()):
            print("last task was " + self.last_task.name)
            task, tglet = next(t_iter)
            while(task != self.last_task):
                print("advance past " + str(task.name))  # advance to last
                task, tglet = next(t_iter)
        for task, tglet in t_iter:
            if self.first_task == tglet:
                print("reached start of task cycle again!")
                greenlet.getcurrent().parent.switch(True)
            if self.first_task is None:
                self.first_task = tglet
            self.last_task = task
            # must rewrite parent here, since greenlets were created outside
            tglet.parent = greenlet.getcurrent()
            tglet.switch()
            if clock() - self.begin_time >= self.timeout:
                print("timeout in taskloop!")
                greenlet.getcurrent().parent.switch(True)

class Task:

    def __init__(self, name):
        self.name = name
        self.num = deque()

    def run(self):
        while True:
            sleep(random.uniform(0.001, 0.20))
            self.say()
            greenlet.getcurrent().parent.switch()

    def say(self):
        print(self.name + " " + str(self.num.popleft()))

    def update(self, n):
        self.num.append(n)

mgr = GreenletTaskScheduler()
t1 = Task("foo")
t2 = Task("bar")
t3 = Task("spam")
t4 = Task("eggs")
t5 = Task("monty")
t6 = Task("python")
mgr.createtask(t1)
mgr.createtask(t2)
mgr.createtask(t3)
mgr.createtask(t4)
mgr.createtask(t5)


for i in range(6):
    mgr.update(i)

mgr.createtask(t6)

for i in range(6, 12):
    mgr.update(i)


mgr.cleartasks()

mgr.update(13)
