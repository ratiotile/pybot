"""
Demonstration of threading to analze map concurrently.
must have tabw-data/ and mapimg/ directories created, or will error out.
"""

import logging
from threading import Thread
import time
import cybw
from cybw import Broodwar
from tabw.TerrainAnalyzer import TerrainAnalyzer
# note: __name__ here makes logging in other modules not work!
logger = logging.getLogger("pybot")

class MapAnalysisWorker(Thread):

    def __init__(self):
        super(MapAnalysisWorker, self).__init__()

    def run(self):
        global analysis_done, start_time
        start_time = time.clock()
        logger.info("starting analysis!")
        TA = TerrainAnalyzer(Broodwar)
        TA.analyze()
        analysis_done = True


analysis_done = False
start_time = 0

def main():
    global analysis_done, start_time

    def reconnect():
        while not client.connect():
            time.sleep(0.5)

    client = cybw.BWAPIClient
    check_analysis = True

    print("Connecting...")
    reconnect()
    while True:
        print("waiting to enter match")
        while not Broodwar.isInGame():
            client.update()
            if not client.isConnected():
                print("Reconnecting...")
                reconnect()
        print("starting match!")
        Broodwar.enableFlag(cybw.Flag.UserInput)
        # message the analysis process to start
        TA_worker = MapAnalysisWorker()


        Broodwar.sendText("black sheep wall")
        print(Broodwar.mapFileName())

        while Broodwar.isInGame():
            if Broodwar.getFrameCount() == 200:
                TA_worker.start()

            events = Broodwar.getEvents()

            client.update()
            if check_analysis and TA_worker.isAlive():
                TA_worker.join(0.03)
                if analysis_done:
                    logger.info("analysis done in {}s!".format(time.clock()-start_time))
                    check_analysis = False
            Broodwar.drawTextScreen(cybw.Position(300, 0), "FPS: " +
            str(Broodwar.getAverageFPS()))

# necessary on Windows to protect entry point
if __name__ == "__main__":
    logger.setLevel(logging.NOTSET)
    fh = logging.FileHandler("pybot.log", mode='w')
    ch = logging.StreamHandler()
    ch.setLevel(logging.NOTSET)
    formatter = logging.Formatter(
        '%(asctime)s.%(msecs).03d - %(name)s - %(levelname)s - %(message)s',
        '%H:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(ch)
    logger.info("begin")
    main()
