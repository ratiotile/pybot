from greenlet import greenlet
from time import sleep, clock
import random, itertools
from collections import deque

# Only cycle through each task once in a frame, and each frame has limited time
# Want to restart the cycle where we left off, thus use coroutine.

class GreenletTaskScheduler:

    def __init__(self):
        self.tasks = {}  # Task to greenlet
        self.timeout = 1
        self.gloop = greenlet(self.taskloop)
        self.first_task = None  # prevent from repeating task in a cycle
        print("back in init")

    def createtask(self, task):
        self.tasks[task] = greenlet(task.run)
        # need to update the gloop greenlet to reflect new task
        self.gloop = greenlet(self.taskloop)

    def update(self, n):
        print(str(n) + "th frame")
        self.first_task = None  # so taskloop will update it this cycle
        self.begin_time = clock()
        for task in self.tasks.keys():
            task.update(n)
        self.gloop.switch()
        print("back in update")

    def taskloop(self):
        # loop forever over cycles
        for tglet in itertools.cycle(self.tasks.values()):
            if self.first_task == tglet:
                print("reached start of task cycle again!")
                greenlet.getcurrent().parent.switch(True)
            if self.first_task is None:
                self.first_task = tglet
            # must rewrite parent here, since greenlets were created outside
            tglet.parent = greenlet.getcurrent()
            tglet.switch()
            if clock() - self.begin_time >= self.timeout:
                print("timeout in taskloop!")
                greenlet.getcurrent().parent.switch(True)

class Task:

    def __init__(self, name):
        self.name = name
        self.num = deque()

    def run(self):
        while True:
            sleep(random.uniform(0.01, 0.40))
            self.say()
            greenlet.getcurrent().parent.switch()

    def say(self):
        print(self.name + " " + str(self.num.popleft()))

    def update(self, n):
        self.num.append(n)

mgr = GreenletTaskScheduler()
t1 = Task("foo")
t2 = Task("bar")
t3 = Task("spam")
t4 = Task("eggs")
t5 = Task("monty")
t6 = Task("python")
mgr.createtask(t1)
mgr.createtask(t2)
mgr.createtask(t3)
mgr.createtask(t4)
mgr.createtask(t5)


for i in range(6):
    mgr.update(i)

mgr.createtask(t6)

for i in range(6,12):
    mgr.update(i)
