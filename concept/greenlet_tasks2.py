from greenlet import greenlet
from time import sleep

class GreenletTaskScheduler:

    def __init__(self):
        self.tasks = []

    def createtask(self, task):
        gtask = greenlet(task)
        self.tasks.append(gtask)

    def update(self):
        for task in self.tasks:
            task.switch()

class Task:

    def __init__(self, name):
        self.name = name

    def run(self):
        while True:
            sleep(0.5)
            print(self.name)
            greenlet.getcurrent().parent.switch()

mgr = GreenletTaskScheduler()
t1 = Task("foo")
t2 = Task("bar")
mgr.createtask(t1.run)
mgr.createtask(t2.run)

for i in range(10):
    mgr.update()
