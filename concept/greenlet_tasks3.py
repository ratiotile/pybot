from greenlet import greenlet
from time import sleep

class GreenletTaskScheduler:

    def __init__(self):
        self.tasks = []

    def createtask(self, task):
        gtask = greenlet(task)
        self.tasks.append(gtask)

    def update(self):
        for task in self.tasks:
            task.switch()

class Task:

    def __init__(self, name):
        self.name = name
        self.num = 0

    def run(self):
        while True:
            sleep(0.5)
            self.say()
            greenlet.getcurrent().parent.switch()

    def say(self):
        print(self.name + " " + str(self.num))

    def update(self, n):
        self.num = n

mgr = GreenletTaskScheduler()
t1 = Task("foo")
t2 = Task("bar")
mgr.createtask(t1.run)
mgr.createtask(t2.run)

for i in range(10):
    t1.update(i)
    t2.update(i)
    mgr.update()
