from pybot.primitive.dynamic import Unit
import cybw, inspect

def getPublicMethodNames(obj):
    """ used to get cybw extension type method names """
    def is_public_method(obj):
        if inspect.isroutine(obj) and not obj.__name__.startswith("_"):
            return True
        return False

    return [x[0] for x in inspect.getmembers(
        obj, predicate=is_public_method)]

import pprint

ppr = pprint.PrettyPrinter()

ppr.pprint(getPublicMethodNames(cybw.BroodwarClass))
print(cybw.Broodwar.isNull())