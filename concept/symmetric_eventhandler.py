import weakref

class SymmetricEvent:

    """ Works by giving references to both emitter and receiver. The emitter
    will call emit, and the receiver can register/unregister.
    Its perfectly safe, just don't do stupid things.
    """

    def __init__(self, eventcls):
        self.listener_func = weakref.WeakKeyDictionary()
        self.eventcls = eventcls

    def emit(self, *args, **kwargs):
        """ push event to all its listeners via registered callbacks """

        event = self.eventcls(*args, **kwargs)
        for obj, func in list(self.listener_func.items()):
            func(obj, event)

    def register(self,  callback):
        """ saves weak ref to callback's self, so it shouldn't keep the object
        alive if we delete all other references to it. """

        obj = callback.__self__
        func = callback.__func__
        self.listener_func[obj] = func

    def unregister(self, callback):
        self.listener_func.pop(callback.__self__, None)

class TestClass:
    def __init__(self, id=1):
        self.eventhandler = None
        self.id = id

    def inc(self, event):
        print(str(self.id) + "_" + event.id)

class TestEvent:
    def __init__(self, id):
        self.id = id

se = SymmetricEvent(TestEvent)

test1 = TestClass()
test1.eventhandler = se
se.register(test1.inc)

test2 = TestClass(2)
test2.eventhandler = se
se.register(test2.inc)

se.emit("1")
se.emit("2")

test1 = None

se.emit("3")

test2.eventhandler.unregister(test2.inc)

se.emit("4")