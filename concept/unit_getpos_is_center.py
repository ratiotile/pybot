#from multiprocessing import Process, Pipe
from threading import Thread
import time
import cybw
from tabw.TerrainAnalyzer import TerrainAnalyzer
from pybot import clientevents

client = cybw.BWAPIClient
Broodwar = cybw.Broodwar

im = None
draw = None

TA = None
check_analysis = True
analysis_done = False

class MapAnalysisWorker(Thread):

    def __init__(self):
        super(MapAnalysisWorker, self).__init__()

    def run(self):
        global analysis_done
        print("starting analysis!")
        TA = TerrainAnalyzer(Broodwar)
        TA.analyze()
        analysis_done = True

def reconnect():
    while not client.connect():
        time.sleep(0.5)

# necessary on Windows to protect entry point
if __name__ == "__main__":
    #parent_conn, child_conn = Pipe()



    print("Connecting...")
    reconnect()
    while True:
        print("waiting to enter match")
        while not Broodwar.isInGame():
            client.update()
            if not client.isConnected():
                print("Reconnecting...")
                reconnect()
        print("starting match!")
        Broodwar.enableFlag(cybw.Flag.UserInput)
        # message the analysis process to start
        TA_worker = MapAnalysisWorker()
        TA_worker.start()

        Broodwar.sendText("black sheep wall")
        print(Broodwar.mapFileName())
        #
        #
        #Broodwar.leaveGame()
        clientevents.onStart()  # doesn't seem to trigger on events

        while Broodwar.isInGame():
            if check_analysis:
                TA_worker.join(0.055)
                if analysis_done:
                    print("analysis done!")
                    check_analysis = False

            units = Broodwar.self().getUnits()
            for unit in units:
                pos = unit.getPosition()
                Broodwar.drawCircleMap(pos, 5, cybw.Colors.Green, True)


            client.update()
            while not Broodwar.isInGame():
                client.update()
                if not client.isConnected():
                    print("Reconnecting...")
                    reconnect()
